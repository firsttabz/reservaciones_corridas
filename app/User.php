<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Hash;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $role
 * @property string $remember_token
 * @property string $sucursal
*/
class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = ['name', 'email', 'password', 'remember_token', 'role_id', 'sucursal_id'];
    protected $hidden = ['password', 'remember_token'];


    public static function boot()
    {
        parent::boot();

        User::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }


    /**
     * Set to null if empty
     * @param $input
     */
    public function setRoleIdAttribute($input)
    {
        $this->attributes['role_id'] = $input ? $input : null;
    }
    public function setSucursalIdAttribute($input)
  {
      $this->attributes['sucursal_id'] = $input ? $input : null;
  }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
    public function sucursal()
    {
        return $this->belongsTo(Sucursale::class, 'sucursal_id')->withTrashed();
    }



    public function sendPasswordResetNotification($token)
    {
       $this->notify(new ResetPassword($token));
    }
}
