<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Milon\Barcode\DNS1D;
use App\Configuracion;
use DateTime;
class ticket extends Model
{
    public static function generarTickets($data){
 /*$data=[
          'nombre_cliente'=>'Miguel Angel Cruz',
          'nombre_corrida'=>'México',
          'fecha_corrida'=>'01/12/18',
          'id_asiento'=>'1',
          'clave'=>'375LÑ',
          'codigo_reservacion'=>'375LÑ',
          'status_reservacion'=>'Pagado',
          'fecha_reservacion'=>'31/11/18',
          'costo_boleto'=>'250.00',
          'fecha_boleto'=>'31/11/18',
          'nombre_empresa'=>'Linea dorada',
          'ubicacion_empresa'=>'Oaxaca centro',
          'telefono_empresa'=>'515832468',
          'pagina_empresa'=>'www.lineadorada.com'
        ];*/
        $configuracion = Configuracion::first();
        $img = Image::make('quickadmin/images/ticket23.png');
        $watermark = Image::make('quickadmin/images/autobus.jpg')->opacity(20);
        $img->insert($watermark, 'top-center', 10, 5);
        $img->text($data['nombre_empresa'],175,20, function($font) {
        $font->file(public_path('font/RobotoCondensed-Bold.ttf'));
        $font->size(22);
        $font->color('#0E0E0D');
        $font->align('center');
        $font->valign('top');
        });
      /* TERMINAL MÉXICO */
      $img->text(/*$data['nombre_empresa']*/"Terminal México: Privada de Vallarta #13",175,20+28, function($font) {
        $font->file(public_path('font/Roboto-BoldItalic.ttf'));
        $font->size(17.6);
        $font->color('#0E0E0D');
        $font->align('center');
        $font->valign('top');
        });
$img->text(/*$data['ubicacion_empresa']*/"Entre Ignacio Vallarta y Ignacio Ramírez", 175, 47+28, function($font) {
        $font->file(public_path('font/Roboto-BoldItalic.ttf'));
        $font->size(17.6);
        $font->color('#0E0E0D');
        $font->align('center');
        $font->valign('top');
        });
$img->text("tel: "./*$data['telefono_empresa']*/"55-570-333-06", 175, 74+28, function($font) {
        $font->file(public_path('font/Roboto-BoldItalic.ttf'));
        $font->size(17.6);
        $font->color('#0E0E0D');
        $font->align('center');
        $font->valign('top');
        });
/* END TERMINAL OAXACA */
      /* TERMINAL OAXACA */
      $img->text(/*$data['nombre_empresa']*/"Terminal Oaxaca: Periférico sur #317",175,20+110, function($font) {
                $font->file(public_path('font/Roboto-BoldItalic.ttf'));
                $font->size(20);
                $font->color('#0E0E0D');
                $font->align('center');
                $font->valign('top');
                });
      $img->text(/*$data['ubicacion_empresa']*/"Entre huzares y Guadalupe victoria", 175, 47+110, function($font) {
                $font->file(public_path('font/Roboto-BoldItalic.ttf'));
                $font->size(20);
                $font->color('#0E0E0D');
                $font->align('center');
                $font->valign('top');
                });


                setlocale(LC_ALL,"es_ES");
                $string = date("d/m/Y", strtotime($data['fecha_reservacion']));
                $date = DateTime::createFromFormat("d/m/Y", $string);
                //echo strftime("%A",$date->getTimestamp());
              //  "*".strftime("%A %e de %B del %G ",$date->getTimestamp())."*   "
      $img->text("tel: "./*$data['telefono_empresa']*/"951-351-81-23", 175, 74+110, function($font) {
                $font->file(public_path('font/Roboto-BoldItalic.ttf'));
                $font->size(20);
                $font->color('#0E0E0D');
                $font->align('center');
                $font->valign('top');
                });
      /* END TERMINAL OAXACA */
      /* DATOS DE LA VENTA */
     $img->line(30, 100+110, 315, 100+110, function ($draw) {
         $draw->color('#0E0E0D');
         });
            $img->text($data['nombre_corrida'], 175, 110+110, function($font) {
                $font->file(public_path('font/Roboto-Bold.ttf'));
                $font->size(25);
                $font->color('#0E0E0D');
                $font->align('center');
                $font->valign('top');
                });
            $img->text(strtoupper(strftime("%A, %e",$date->getTimestamp())), 175, 139+110, function($font) {
                $font->file(public_path('font/Roboto-Bold.ttf'));
                $font->size(22);
                $font->color('#0E0E0D');
                $font->align('right');
                $font->valign('top');
                });
            $img->text(strtoupper(strftime("%B %G",$date->getTimestamp())), 100, 167+110, function($font) {
                $font->file(public_path('font/Roboto-Bold.ttf'));
                $font->size(22);
                $font->color('#0E0E0D');
                $font->align('left');
                $font->valign('top');
                });
            $img->text($data['hora_corrida']." hrs.", 175, 190+110, function($font) {
                $font->file(public_path('font/Roboto-Bold.ttf'));
                $font->size(24);
                $font->color('#0E0E0D');
                $font->align('center');
                $font->valign('top');
                });
            $img->text("(horario en formato de 24 horas)", 175, 215+110, function($font) {
                $font->file(public_path('font/OpenSans-SemiBoldItalic.ttf'));
                $font->size(14);
                $font->color('#0E0E0D');
                $font->align('center');
                $font->valign('top');
                });
  /*          $img->text("Hora de salida: ".$data['fecha_corrida'], 175, 212+110, function($font) {
                      $font->file(public_path('font/Roboto-Bold.ttf'));
                      $font->size(20);
                      $font->color('#0E0E0D');
                      $font->align('center');
                      $font->valign('top');
                      });*/
            $img->text("ASIENTO ".$data['id_asiento'], 175, 245+105, function($font) {
                      $font->file(public_path('font/Roboto-Bold.ttf'));
                      $font->size(24);
                      $font->color('#0E0E0D');
                      $font->align('center');
                      $font->valign('top');
                      });
            $img->text("CLIENTE: ".$data['nombre_cliente'], 175, 275+102, function($font) {
                      $font->file(public_path('font/Roboto-Bold.ttf'));
                      $font->size(19);
                      $font->color('#0E0E0D');
                      $font->align('center');
                      $font->valign('top');
                      });
            $img->text("COSTO: "."$".$data['costo_boleto']."MXN", 175, 300+100, function($font) {
                      $font->file(public_path('font/Roboto-Bold.ttf'));
                      $font->size(22);
                      $font->color('#0E0E0D');
                      $font->align('center');
                      $font->valign('top');
                      });
 /*           $img->line(30, 278+110, 315, 278+110, function ($draw) {
                          $draw->color('#0E0E0D');
                          });*/
                          // write text at position
            $img->text("RESERVACIÓN N°: ".$data['codigo_reservacion'], 175, 330+97, function($font) {
                      $font->file(public_path('font/Roboto-Bold.ttf'));
                      $font->size(19);
                      $font->color('#0E0E0D');
                      $font->align('center');
                      $font->valign('top');
                      }); /*
            $img->text("N°: ".$data['codigo_reservacion'], 175, 311+110, function($font) {
                      $font->file(public_path('font/Roboto-Bold.ttf'));
                      $font->size(20);
                      $font->color('#0E0E0D');
                      $font->align('center');
                      $font->valign('top');
                      });*/
            $img->text("ESTADO: ".strtoupper($data['status_reservacion']), 175, 357+96, function($font) {
                      $font->file(public_path('font/Roboto-Bold.ttf'));
                      $font->size(19);
                      $font->color('#0E0E0D');
                      $font->align('center');
                      $font->valign('top');
                      });
   /*         $img->text("Fecha: ".$data['fecha_reservacion'], 175, 357+110, function($font) {
                      $font->file(public_path('font/Roboto-Bold.ttf'));
                      $font->size(20);
                      $font->color('#0E0E0D');
                      $font->align('center');
                      $font->valign('top');
                      });*/
              $img->text("(ticket impreso el ".date("d/m/Y", strtotime($data['fecha_boleto'])).")", 175, 357+116, function($font) {
                        $font->file(public_path('font/OpenSans-SemiBoldItalic.ttf'));
                        $font->size(14);
                        $font->color('#0E0E0D');
                        $font->align('center');
                        $font->valign('top');
                        });
            $img->line(30, 380+110, 315, 380+110, function ($draw) {
                        $draw->color('#0E0E0D');
                        });
            /* START TERMINOS */
              // La cancelación esta en horas
              $img->text("*Cambios o cancelaciones 4 horas", 175, 386+110, function($font) {
                            $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                            $font->size(18);
                            $font->color('#0E0E0D');
                            $font->align('center');
                            $font->valign('top');
                            });
            $img->text("antes de la salida. ", 175, 386+17+110, function($font) {
                            $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                            $font->size(18);
                            $font->color('#0E0E0D');
                            $font->align('center');
                            $font->valign('top');
                            });
            $img->text("*Maximo 1 cambio de fecha de su viaje,", 175, 386+110+17+17, function($font) {
                          $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                          $font->size(18);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
          $img->text("los demás cambios se cobrara $15.00 c/u. ", 175, 386+17+110+17+17, function($font) {
                          $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                          $font->size(18);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
          $img->text("*No hay cambios de destinos.", 175, 386+17+110+17+17+20, function($font) {
                          $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                          $font->size(18);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
            $img->text("*Niños Mayores de 3 años pagan boleto. ", 175, 386+17+17+110+17+17+20, function($font) {
                          $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                          $font->size(18);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
            $img->text("*En viajes redondos 1 mes de vigencia", 175, 386+17+17+17+110+17+17+20, function($font) {
                          $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                          $font->size(18);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
            $img->text("sin devolución.  ", 175, 386+17+17+17+17+110+17+17+20, function($font) {
                            $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                            $font->size(18);
                            $font->color('#0E0E0D');
                            $font->align('center');
                            $font->valign('top');
                            });
            $img->text("*La reservación del boleto se respeta", 175, 386+17+17+17+17+17+110+17+17+20, function($font) {
                          $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                          $font->size(18);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
             $img->text("1 hora antes del abordaje.  ", 175, 386+17+17+17+17+17+17+110+17+17+20, function($font) {
                              $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                              $font->size(18);
                              $font->color('#0E0E0D');
                              $font->align('center');
                              $font->valign('top');
                              });
            $img->text("*El equipaje se etiquetará 30 min", 175, 386+17+17+17+17+17+17+17+110+17+17+20, function($font) {
                          $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                          $font->size(18);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
            $img->text("antes del abordaje.  ", 175, 386+17+17+17+17+17+17+17+17+110+17+17+20, function($font) {
                            $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                            $font->size(18);
                            $font->color('#0E0E0D');
                            $font->align('center');
                            $font->valign('top');
                            });
            $img->text("*Debe presetarse 30 min antes del abordaje  ", 175, 386+17+17+17+17+17+17+17+17+17+110+17+17+20, function($font) {
                          $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                          $font->size(18);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
            $img->text("*** No pierda su boleto ***", 175, 386+17+17+17+17+17+17+17+17+17+17+110+17+17+20, function($font) {
                            $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                            $font->size(18);
                            $font->color('#0E0E0D');
                            $font->align('center');
                            $font->valign('top');
                            });
           $img->text("$20 adicionales por reimpresión", 175, 386+17+17+17+17+17+17+17+17+17+17+17+110+17+17+20, function($font) {
                              $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                              $font->size(18);
                              $font->color('#0E0E0D');
                              $font->align('center');
                              $font->valign('top');
                              });
            $img->line(30, 386+110+17+17+17+17+17+17+17+17+17+17+17+17+17+5+17+20, 315, 386+20+17+17+17+17+17+17+17+17+17+17+17+5+110+17+17+17, function ($draw) {
                                $draw->color('#0E0E0D');
                                });
            /* END TERMINOS */
           $img->text("Gracias por usar nuestro servicio", 175, 480+120+110+17+17+17, function($font) {
                     $font->file(public_path('font/Roboto-Bold.ttf'));
                     $font->size(15);
                     $font->color('#0E0E0D');
                     $font->align('center');
                     $font->valign('top');
                     });
          $img->text("Le atendio ".$data['vendedor'], 175, 494+120+110+17+17+17, function($font) {
                    $font->file(public_path('font/Roboto-Bold.ttf'));
                    $font->size(15);
                    $font->color('#0E0E0D');
                    $font->align('center');
                    $font->valign('top');
                    });
         $img->text("Visitenos en ".$data['pagina_empresa'], 175, 508+120+110+17+17+17, function($font) {
                   $font->file(public_path('font/Roboto-Bold.ttf'));
                   $font->size(15);
                   $font->color('#0E0E0D');
                   $font->align('center');
                   $font->valign('top');
                   });
          $barcode = Image::make(DNS1D::getBarcodePNG($data['codigo_reservacion'],"C128A"))->resize(300, 55);
          $img->insert($barcode, 'top-center', 1200, 524+120+115+50);
            return $img->response('jpg', 100);
    }
}
