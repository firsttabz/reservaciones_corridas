<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Asiento
 *
 * @package App
 * @property integer $numero
 * @property string $corrida
*/
class Asiento extends Model
{
    use SoftDeletes;

    protected $fillable = ['numero', 'corrida_id'];
    protected $hidden = [];


    public static function boot()
    {
        parent::boot();

        Asiento::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setNumeroAttribute($input)
    {
        $this->attributes['numero'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setAutobusIdAttribute($input)
    {
        $this->attributes['corrida_id'] = $input ? $input : null;
    }

    public function corrida()
    {
        return $this->belongsTo(Corrida::class, 'corrida_id')->withTrashed();
    }

    public function reservacion(){
      return $this->hasOne(Reservacione::class,'asiento_id')->withTrashed();
    }

}
