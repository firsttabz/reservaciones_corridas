<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Autobus
 *
 * @package App
 * @property string $marca
 * @property string $modelo
 * @property string $placas
 * @property string $propietario
 * @property integer $numero_asientos
 * @property text $descripcion
*/
class Autobus extends Model
{
    use SoftDeletes;

    protected $fillable = ['marca', 'modelo', 'placas', 'propietario', 'numero_asientos', 'descripcion', 'costo_renta', 'costo_boleto_redondo', 'costo_boleto_sencillo'];
    protected $hidden = [];


    public static function boot()
    {
        parent::boot();

        Autobus::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setNumeroAsientosAttribute($input)
    {
        $this->attributes['numero_asientos'] = $input ? $input : null;
    }

}
