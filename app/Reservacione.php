<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Reservacione
 *
 * @package App
 * @property string $cliente
 * @property decimal $costo_boleto
 * @property string $vendedor
 * @property string $asiento
 * @property string $corrida
 * @property string $clave
 * @property tinyInteger $descuento
 * @property text $observaciones
 * @property string $comprobante_pago
 * @property string $estatus
*/
class Reservacione extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'cliente', 'costo_boleto',
      'costo_boleto_original',
      'clave', 'descuento',
      'observaciones',
      'comprobante_pago',
      'estatus',
      'vendedor_id',
      'vendedor_liquida_id',
      'vendedor_liquida_fecha',
      'vendedor_anticipa_id',
      'vendedor_anticipa_fecha',
      'vendedor_cancela_id',
      'vendedor_cancela_fecha',
      'vendedor_pagado_id',
      'vendedor_pagado_fecha',
      'vendedor_cambia_id',
      'vendedor_cambia_fecha',
      'asiento_id',
      'corrida_id',
      'anticipo',
      'redondo',
      'sucursal_id',
      'clave_global',
      'email'
    ];
    protected $hidden = [];


    public static function boot()
    {
        parent::boot();

        Reservacione::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setCostoBoletoAttribute($input)
    {
        $this->attributes['costo_boleto'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setVendedorIdAttribute($input)
    {
        $this->attributes['vendedor_id'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setAsientoIdAttribute($input)
    {
        $this->attributes['asiento_id'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setCorridaIdAttribute($input)
    {
        $this->attributes['corrida_id'] = $input ? $input : null;
    }

    public function vendedor()
    {
        return $this->belongsTo(User::class, 'vendedor_id');
    }

    public function asiento()
    {
        return $this->belongsTo(Asiento::class, 'asiento_id')->withTrashed();
    }

    public function corrida()
    {
        return $this->belongsTo(Corrida::class, 'corrida_id')->withTrashed();
    }

}
