<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
  protected $fillable = ['razon_social', 'rfc', 'regimen_fiscal', 'uso_cfdi', 'celular', 'domicilio', 'email'];
}
