<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

/**
 * Class Chofer
 *
 * @package App
 * @property string $nombre
 * @property string $edad
 * @property string $domicilio
 * @property string $domicili_gmaps
 * @property string $telefono
 * @property string $celular
 * @property text $nota
 * @property string $correo
*/
class Chofer extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $fillable = ['nombre', 'edad', 'domicilio', 'telefono', 'celular', 'nota', 'correo', 'domicili_gmaps_address', 'domicili_gmaps_latitude', 'domicili_gmaps_longitude'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Chofer::observe(new \App\Observers\UserActionsObserver);
    }
    
}
