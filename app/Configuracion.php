<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Configuracion
 *
 * @package App
 * @property decimal $costo_boleto_sencillo
 * @property decimal $costo_boleto_redondo
 * @property integer $tiempo_espera_reservacion
 * @property tinyInteger $editar_precios
 * @property tinyInteger $aplicar_descuentos
 * @property tinyInteger $forzar_chofer
 * @property tinyInteger $forzar_autobus
*/
class Configuracion extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'costo_boleto_sencillo',
      'costo_boleto_redondo',
      'tiempo_espera_reservacion',
      'tiempo_espera_reservacion_plataforma',
      'editar_precios',
      'aplicar_descuentos',
      'forzar_chofer',
      'forzar_autobus',
      'total_asientos',
      'ganancia_boleto_sencillo',
      'ganancia_boleto_redondo'
    ];
    protected $hidden = [];


    public static function boot()
    {
        parent::boot();

        Configuracion::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setCostoBoletoSencilloAttribute($input)
    {
        $this->attributes['costo_boleto_sencillo'] = $input ? $input : null;
    }

    public function setTotalAsientosAttribute($input)
    {
        $this->attributes['total_asientos'] = $input ? $input : null;
    }
    /**
     * Set attribute to money format
     * @param $input
     */
    public function setCostoBoletoRedondoAttribute($input)
    {
        $this->attributes['costo_boleto_redondo'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setTiempoEsperaReservacionAttribute($input)
    {
        $this->attributes['tiempo_espera_reservacion'] = $input ? $input : null;
    }

}
