<?php

namespace App\Providers;

use App\Role;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $user = \Auth::user();


        // Auth gates for: User management
        Gate::define('user_management_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Users
        Gate::define('user_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Role access
        Gate::define('role_access', function ($user) {
          return in_array($user->role_id, [1]);
        });
        Gate::define('role_delete', function ($user) {
          return in_array($user->role_id, [1]);
        });
        Gate::define('role_edit', function ($user) {
          return in_array($user->role_id, [1]);
        });
        Gate::define('role_create', function ($user) {
          return in_array($user->role_id, [1]);
        });
        Gate::define('role_view', function ($user) {
          return in_array($user->role_id, [1]);
        });
        // Auth gates for: User actions
        Gate::define('user_action_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Contact management
        Gate::define('contact_management_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: lista_liquidaciones_access
        Gate::define('lista_liquidaciones_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Contact companies
        Gate::define('contact_company_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_company_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_company_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_company_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_company_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Contacts
        Gate::define('contact_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Planeación de corridas
        Gate::define('planeación_de_corrida_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Autobus
        Gate::define('autobus_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('autobus_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('autobus_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('autobus_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('autobus_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Chofer
        Gate::define('chofer_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('chofer_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('chofer_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('chofer_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('chofer_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Corridas
        Gate::define('corrida_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('corrida_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('corrida_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('corrida_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('corrida_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Configuracion
        Gate::define('configuracion_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('configuracion_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('configuracion_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('configuracion_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('configuracion_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Asiento
        Gate::define('asiento_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('asiento_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('asiento_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('asiento_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('asiento_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('reporte_corrida_access', function ($user) {
          return in_array($user->role_id, [1,2]);
        });
        // Auth gates for: Ventas
        Gate::define('venta_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Reservaciones
        Gate::define('reservacione_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('reservacione_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('reservacione_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('reservacione_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('reservacione_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('reservar_cambios', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Reservar
        Gate::define('reservar_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

         // Auth gates for: Planeación de corridas
         Gate::define('contabilidad_access', function ($user) {
            return in_array($user->role_id, [1,2]);
        });

        // Auth gates for: Corte de caja
        Gate::define('corte_de_caja_access', function ($user) {
            return in_array($user->role_id, [1,2]);
        });


        // Auth gates for: Contabilidad de boletos
        Gate::define('contabilidad_de_boletos', function ($user) {
          return in_array($user->role_id,[1]);
        });

        // Auth gates for: paypal
        Gate::define('paypal_access', function ($user) {
          return in_array($user->role_id,[1,2]);
        });

        // Auth gates for: Envios
        Gate::define('envios_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Envios
        Gate::define('envio_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Envio
        Gate::define('buscar_envio_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Clientes
        Gate::define('client_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        Gate::define('client_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        Gate::define('client_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        Gate::define('client_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Sucursales
        Gate::define('sucursale_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('sucursale_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('sucursale_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('sucursale_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('sucursale_delete', function ($user) {
            return in_array($user->role_id, [1]);});

    }
}
