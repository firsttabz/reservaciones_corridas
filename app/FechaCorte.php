<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class FechaCorte extends Model
{
      protected $fillable = ['fecha_corte', 'activado'];

      public static function dias() {
        $fecha_corte = FechaCorte::first();
        $now = Carbon::now();
        $corte = Carbon::parse($fecha_corte->fecha_corte);
        $dias = env('DIAS_PARA_CORTE',30);
        $diffDays =  $now->diffInDays($corte, false);
        return $diffDays;
      }
}
