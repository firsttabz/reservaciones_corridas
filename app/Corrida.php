<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Corrida
 *
 * @package App
 * @property string $nombre
 * @property time $hora_salida
 * @property decimal $costo_boleto_sencillo
 * @property string $costo_boleto_redondo
 * @property integer $total_asientos
 * @property text $observaciones
 * @property tinyInteger $generar_lunes
 * @property tinyInteger $generar_martes
 * @property tinyInteger $generar_miercoles
 * @property tinyInteger $generar_jueves
 * @property tinyInteger $generar_viernes
 * @property tinyInteger $generar_sabado
 * @property tinyInteger $generar_domingo
 * @property string $chofer
 * @property string $autobus
  *@property string $lat_a
  *@property string $long_a
  *@property string $lat_b
  *@property string $long_b
*/
class Corrida extends Model
{
    use SoftDeletes;

    protected $fillable = ['activo','nombre', 'hora_salida', 'costo_boleto_sencillo', 'costo_boleto_redondo', 'total_asientos', 'observaciones', 'generar_lunes', 'generar_martes', 'generar_miercoles', 'generar_jueves', 'generar_viernes', 'generar_sabado', 'generar_domingo', 'chofer_id', 'autobus_id', 'camioneta', 'extraordinaria', 'fecha_extraordinaria','lat_a','lng_a','lat_b','lng_b'];
    protected $hidden = [];


    public static function boot()
    {
        parent::boot();

        Corrida::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setHoraSalidaAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['hora_salida'] = Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            $this->attributes['hora_salida'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getHoraSalidaAttribute($input)
    {
        if ($input != null && $input != '') {
            return Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setCostoBoletoSencilloAttribute($input)
    {
        $this->attributes['costo_boleto_sencillo'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setTotalAsientosAttribute($input)
    {
        $this->attributes['total_asientos'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setChoferIdAttribute($input)
    {
        $this->attributes['chofer_id'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setAutobusIdAttribute($input)
    {
        $this->attributes['autobus_id'] = $input ? $input : null;
    }

    public function chofer()
    {
        return $this->belongsTo(Chofer::class, 'chofer_id')->withTrashed();
    }

    public function autobus()
    {
        return $this->belongsTo(Autobus::class, 'autobus_id')->withTrashed();
    }

    public static function obtieneLimiteDeAsientos(Carbon $from, $corrida){
        $limite = $corrida->total_asientos;
        $from_gt = $from->gt(Carbon::now());
        if($from_gt == true && $corrida->camion == false){
          $limite = Configuracion::first()->total_asientos;
        }
        return $limite;
      }
}
