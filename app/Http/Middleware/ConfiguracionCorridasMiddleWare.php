<?php

namespace App\Http\Middleware;

use Closure;
use App\Configuracion;
use Session;
use Auth;
class ConfiguracionCorridasMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $configuracion = Configuracion::where('deleted_at',null)->first();
      if($configuracion == null){
        $configuracion = Configuracion::create([
          'costo_boleto_sencillo' => '250.00',
          'costo_boleto_redondo' => '480.00',
          'tiempo_espera_reservacion' => 60,
          'tiempo_espera_envio' => 60,
          'editar_precios' => 1,
          'total_asientos' => 46,
        ]);
      }
      //Session::forget('configuracion');
      Session::put('configuracion', $configuracion);
      //return dd(Session::get('configuracion'));
        return $next($request);
    }
}
