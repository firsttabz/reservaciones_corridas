<?php

namespace App\Http\Middleware;

use Closure;
use App\Corrida;
use Carbon\Carbon;
class CorridasExtraordinariasMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $corridas = Corrida::where('activo',1)
      ->where('extraordinaria',1)
      ->whereNull('deleted_at')
      ->whereDate('fecha_extraordinaria','<', Carbon::now()->startOfDay())
      ->update(['activo' => 0]);

        return $next($request);
    }
}
