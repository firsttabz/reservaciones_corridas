<?php

namespace App\Http\Middleware;

use Closure;
use App\Asiento;
use App\Configuracion;
use App\Reservacione;
use Carbon\Carbon;

class CancelacionesPlataformaAutomatica
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $hoy = Carbon::now()->format('Y-m-d');
        $time_off = Configuracion::first()->tiempo_espera_reservacion_plataforma;
        // eliminando reservaciones no pgadas  de la plataforma
        $reservaciones = Reservacione::where('reservaciones.estatus','apartado')
        ->select([
          'reservaciones.id as reservacion_id',
          'corridas.hora_salida',
          'reservaciones.created_at'
        ])
        ->join('corridas','reservaciones.corrida_id','=','corridas.id')
        //->where('fecha_reservacion', $hoy)
        ->where('reservaciones.is_plt_reservacion',true)
        ->whereNull('reservaciones.deleted_at')
        ->get();

        foreach($reservaciones as $a){
          $a->justo_ahora = Carbon::now();
          $a->diff = Carbon::parse($a->updated_at)->diffInMinutes($a->justo_ahora);
          if($a->diff >= $time_off){
            Reservacione::where('id',$a->reservacion_id)->update([
              'estatus' => 'cancelado'
            ]);
          }
        }

        $reservaciones = Reservacione::where('reservaciones.estatus','apartado')
        ->select([
          'reservaciones.id as reservacion_id',
          'corridas.hora_salida',
          'reservaciones.created_at',
          'reservaciones.updated_at',
        ])
        ->join('corridas','reservaciones.corrida_id','=','corridas.id')
        //->where('fecha_reservacion', $hoy)
        ->where('reservaciones.is_plt_reservacion',true)
        ->where('reservaciones.cliente','N/A')
        ->whereNull('reservaciones.deleted_at')
        ->get();
        // eliminando reservaciones seleccionadas de la plataforma  sin concluir reservacion
        foreach($reservaciones as $a){
          $a->justo_ahora = Carbon::now();
          $a->diff = Carbon::parse($a->updated_at)->diffInMinutes($a->justo_ahora, false);
          if($a->diff >= 10){
            Reservacione::where('id',$a->reservacion_id)->update([
              'estatus' => 'cancelado'
            ]);
          }
        }
        return $next($request);
    }
}
