<?php

namespace App\Http\Middleware;
use App\Asiento;
use App\Configuracion;
use App\Reservacione;
use Carbon\Carbon;
use Closure;

class CancelacionesAutomaticasMiddlWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $hoy = Carbon::now()->format('Y-m-d');
        $time_off = Configuracion::first()->tiempo_espera_reservacion;
        
        $reservaciones = Reservacione::where('reservaciones.estatus','apartado')
        ->select([
          'reservaciones.id as reservacion_id',
          'corridas.hora_salida',
        ])
        ->join('corridas','reservaciones.corrida_id','=','corridas.id')
        ->where('fecha_reservacion', $hoy)
        ->whereNull('reservaciones.deleted_at')
        ->get();
  
        foreach($reservaciones as $a){
          $a->justo_ahora = Carbon::now();
          $a->hora_salida =  Carbon::createFromFormat('Y-m-d H:i:s',Carbon::now()->format('Y-m-d').' '. $a->hora_salida);
          $a->diff = $a->justo_ahora->diffInMinutes($a->hora_salida);
          if($a->diff <= $time_off){
            Reservacione::where('id',$a->reservacion_id)->update([
              'estatus' => 'cancelado'
            ]);
          }
        }

        return $next($request);
    }
}
