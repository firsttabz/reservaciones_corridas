<?php

namespace App\Http\Middleware;

use Closure;
use App\FechaCorte;
use Carbon\Carbon;
use Session;
class ActivacionSistemaMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $corte = FechaCorte::first();
        if(!$corte){
          FechaCorte::create([
            'fecha_corte' => Carbon::now()->addDays(30)->toDateString(),
            'activado' => true,
          ]);
        }
        $fecha_corte = FechaCorte::first();
        $now = Carbon::now();
        $corte = Carbon::parse($fecha_corte->fecha_corte);
        $dias = env('DIAS_PARA_CORTE',30);
        $diffDays =  $now->diffInDays($corte, false);

        if($diffDays <= 0) {
          $fecha_corte->activado = false;
          $fecha_corte->save();
          die("Sistema inactivo. contáctenos en https://jasbit.com");
        }

        return $next($request);
    }
}
