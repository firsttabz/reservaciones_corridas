<?php

namespace App\Http\Controllers\Api\V1;

use App\Sucursale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreSucursalesRequest;
use App\Http\Requests\Admin\UpdateSucursalesRequest;

class SucursalesController extends Controller
{
    public function index()
    {
        return Sucursale::all();
    }

    public function show($id)
    {
        return Sucursale::findOrFail($id);
    }

    public function update(UpdateSucursalesRequest $request, $id)
    {
        $sucursale = Sucursale::findOrFail($id);
        $sucursale->update($request->all());
        

        return $sucursale;
    }

    public function store(StoreSucursalesRequest $request)
    {
        $sucursale = Sucursale::create($request->all());
        

        return $sucursale;
    }

    public function destroy($id)
    {
        $sucursale = Sucursale::findOrFail($id);
        $sucursale->delete();
        return '';
    }
}
