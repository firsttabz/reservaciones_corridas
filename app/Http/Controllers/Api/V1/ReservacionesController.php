<?php

namespace App\Http\Controllers\Api\V1;

use App\Reservacione;
use App\Asiento;
use App\Corrida;
use App\Autobus;
use App\User;
use App\Configuracion;
use App\Mensaje;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreReservacionesRequest;
use App\Http\Requests\Admin\UpdateReservacionesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Http\Requests\ReservacionesEncuentraRequest;
use App\Http\Requests\ReservacionesCambiosEncuentraRequest;
use App\Http\Requests\ReservarCambiosRequest;
use App\Http\Requests\ReservarRequest;
use Validator;
use Carbon\Carbon;
use Session;
use Auth;
use Crypt;
use Curl;
use Log;
use App\Http\Requests\Plataforma\ReservacionPlataforma;

class ReservacionesController extends Controller
{
    use FileUploadTrait;

    public function index()
    {
        return Reservacione::all();
    }

    public function show($id)
    {
        return Reservacione::findOrFail($id);
    }

    public function update(UpdateReservacionesRequest $request, $id)
    {
        $request = $this->saveFiles($request);
        $reservacione = Reservacione::findOrFail($id);
        $reservacione->update($request->all());


        return $reservacione;
    }

    public function store(StoreReservacionesRequest $request)
    {
        $request = $this->saveFiles($request);
        $reservacione = Reservacione::create($request->all());


        return $reservacione;
    }

    public function destroy($id)
    {
        $reservacione = Reservacione::findOrFail($id);
        $reservacione->delete();
        return '';
    }

    private function obtieneLimiteDeAsientos($from, $corrida){
      $limite = $corrida->total_asientos;
      $from_gt = Carbon::createFromFormat('Y-m-d', $from)->gt(Carbon::now());
      if($from_gt == true && $corrida->camion == false){
        $limite = Configuracion::first()->total_asientos;
      }
      return $limite;
    }

    public function cambiosEncuentra(ReservacionesCambiosEncuentraRequest $request) {
      $config = Configuracion::first();
      $corrida = Corrida::findOrFail($request->corrida_cambio);

      // validando la fechas
      $fecha_cambio = Carbon::parse($request->fecha_cambio);
      $diff_days = Carbon::now()->diffInDays($fecha_cambio, false);
      // validando si la fecha es menor al día de hoy
      if($diff_days < 0){
        return response()->json(['message' => 'Tu fecha de cambio es menor al día de hoy']);
        exit;
      }
      // validando si el nuevo cambio no esta a 15 min de salir
      $fecha_cambio = Carbon::parse($fecha_cambio->format('Y-m-d'). ' ' . $corrida->hora_salida);
      $diff_mins = Carbon::now()->diffInMinutes($fecha_cambio, false);
      if ($diff_mins <= $config->tiempo_espera_reservacion) {
        return response()->json(['message' => 'Ya no puedes hacer cambios para esta reservación']);
        exit;
      }
      // Aqui quiere decir que ya podemos buscar los asientos
      $limite_asientos = $this->obtieneLimiteDeAsientos($fecha_cambio->format('Y-m-d'), $corrida);
      $asientos_corrida = Asiento::where('corrida_id',$corrida->id)->limit($limite_asientos)->get();
      $fecha_cambio = $fecha_cambio->format('Y-m-d');
      // revisando las reservaciones de cada asientos
      $asientos_disponibles = [];
      foreach ($asientos_corrida as $asiento) {
        $asiento->reservacion = Reservacione::select('id','estatus')
        ->whereNull('deleted_at')
        ->where('estatus','!=','cancelado')
        ->where('asiento_id',$asiento->id)
        ->where('corrida_id',$corrida->id)
        ->whereDate('fecha_reservacion', $fecha_cambio)->first();
        // $asientos_disponibles[] = $asiento;
        if($asiento->reservacion == null){
          $asientos_disponibles[] = $asiento;
        }
      }
      return ($asientos_disponibles);
    }

    public function encuentra(ReservacionesEncuentraRequest $request)
    {
      //return dd($request->all());exit;
      //Validaciones
      if($request->tipo_viaje == 'redondo') {
        $request->validate([
          'reservaciones.0.corrida_id' => 'required|numeric|different:reservaciones.1.corrida_id',
          'reservaciones.*.fecha_reservacion' => 'required|date_format:Y-m-d',
        ]);
      }
      else {
        $request->validate([
            'reservaciones.0.corrida_id' => 'required|numeric',
            'reservaciones.0.fecha_reservacion' => 'required|date_format:Y-m-d',
        ]);
      }
      // Validación de corrida extraordinaria
      $corrida_salida = Corrida::findOrFail($request->reservaciones[0]['corrida_id']);
      if($corrida_salida->extraordinaria == true){
        $fecha_salida_reservacion = Carbon::parse($request->reservaciones[0]['fecha_reservacion']);
        $fecha_extraordinaria = Carbon::parse($corrida_salida->fecha_extraordinaria);
        $diff_days = $fecha_salida_reservacion->diffInDays($fecha_extraordinaria);
        if($diff_days > 0){
          return response()->json([
            'message' => 'Tu corrida de ida es extraordinaria solo puedes reservar para  '.$fecha_extraordinaria->format('d/m/Y').'. Diferencia de días: '.$diff_days,
          ],422);
        }
      }
      // Validación de fechas de reservación para hoy para fecha de ida
      $fecha_salida_reservacion = Carbon::parse($request->reservaciones[0]['fecha_reservacion'].' '.$corrida_salida->hora_salida);
      $fecha_hoy = Carbon::now();
      $diff = $fecha_hoy->diffInMinutes($fecha_salida_reservacion, false);
      if($diff <= 0) {
        return response()->json([
          // 'diff' => $diff,
          // 'fr' => $request->reservaciones[0]['fecha_reservacion'].' '.$corrida_salida->hora_salida,
          // 'now' =>  $fecha_hoy = Carbon::now()->format('Y-m-d H:i:s')
          'message' => 'No puedes reservar para la corrida de ida debido al horario'
        ],422);
        exit;
      }

      if($request->tipo_viaje == 'redondo') {
        $corrida_regreso = Corrida::findOrFail($request->reservaciones[1]['corrida_id']);
        if($corrida_regreso->extraordinaria == true){
          $fecha_salida_reservacion = Carbon::parse($request->reservaciones[1]['fecha_reservacion']);
          $fecha_extraordinaria = Carbon::parse($corrida_regreso->fecha_extraordinaria);
          $diff_days = $fecha_salida_reservacion->diffInDays($fecha_extraordinaria);
          if($diff_days > 0){
            return response()->json([
              'message' => 'Tu corrida de regreso es extraordinaria solo puedes reservar para  '.$fecha_extraordinaria->format('d/m/Y').'. Diferencia de días: '.$diff_days,
            ],422);
          }
        }
      }
      // fin Validaciones
      // Obteniendo asientos de corrida de salida index:0
      $from = $request->reservaciones[0]['fecha_reservacion'];
      $corrida = Corrida::findOrFail($request->reservaciones[0]['corrida_id']);
      $limite_asientos = $this->obtieneLimiteDeAsientos($from,$corrida);
      $asientos_corrida = Asiento::where('corrida_id',$corrida->id)->limit($limite_asientos)->get();
      $asientos_disponibles_0 = [];
      foreach ($asientos_corrida as $asiento) {
        $asiento->reservacion = Reservacione::select('id','estatus')
        ->whereNull('deleted_at')
        ->where('estatus','!=','cancelado')
        ->where('asiento_id',$asiento->id)
        ->where('corrida_id',$corrida->id)
        ->whereDate('fecha_reservacion',$from)->first();
        $asientos_disponibles_0[] = $asiento;
        /*if($asiento->reservacion == null){
          $asientos_disponibles_0[] = $asiento;
        }*/
      }
      // Obteniendo asientos de corrida de regreso index:1 (si el tipo de viaje es 'redondo')
      if($request->tipo_viaje == 'redondo'){
        $from = $request->reservaciones[1]['fecha_reservacion'];//Carbon::createFromFormat('Y-m-d',$request->reservaciones[0]['fecha_reservacion']);
        $corrida = Corrida::findOrFail($request->reservaciones[1]['corrida_id']);
        $limite_asientos = $this->obtieneLimiteDeAsientos($from,$corrida);
        $asientos_corrida = Asiento::where('corrida_id',$corrida->id)->limit($limite_asientos)->get();
        $asientos_disponibles_1 = [];
        foreach ($asientos_corrida as $asiento) {
          $asiento->reservacion = Reservacione::select('id','estatus')
          ->whereNull('deleted_at')
          ->where('estatus','!=','cancelado')
          ->where('asiento_id',$asiento->id)
          ->where('corrida_id',$corrida->id)
          ->whereDate('fecha_reservacion',$from)->first();
          $asientos_disponibles_1[] = $asiento;
          /*
          if($asiento->reservacion == null){
            $asientos_disponibles_1[] = $asiento;
          }
          */
        }
      }
      //forma en la que responderá el webservice dependiento del tipo de viaje
      if($request->tipo_viaje == 'sencillo'){
        return response()->json([
          'corrida_salida_asientos' => $asientos_disponibles_0,
          'corrida_regreso_asientos' => [],
          'permisos' => Session::get('configuracion'),
          'precio' => [
            'raw' => $corrida->costo_boleto_sencillo,
            'format' => number_format($corrida->costo_boleto_sencillo,2),
          ],
          //'fecha_mayor_a_hoy' => Carbon::createFromFormat('Y-m-d', $from)->gt(Carbon::now()),
          //'$limite_asientos' => $limite_asientos,
          //'$corrida->total_asientos' => $corrida->total_asientos
        ]);
      }
      else {
        return response()->json([
          'corrida_salida_asientos' => $asientos_disponibles_0,
          'corrida_regreso_asientos' => $asientos_disponibles_1,
          'permisos' => Session::get('configuracion'),
          'precio' => [
            'raw' => $corrida->costo_boleto_redondo/2,
            'format' => number_format($corrida->costo_boleto_redondo/2,2),
          ],
        ]);
      }
    }

    public function reservar(ReservarRequest $request)
    {
        //Validaciones
        if($request->tipo_viaje == 'redondo') {
          $request->validate([
            'reservaciones.0.corrida_id' => 'required|numeric|different:reservaciones.1.corrida_id',
            'reservaciones.*.fecha_reservacion' => 'required|date_format:Y-m-d',
            'reservaciones.*.costo_boleto' => 'required|numeric',
            'reservaciones.*.asiento.id' => 'required|integer',
            'reservaciones.*.estatus' => 'required|in:apartado,pagado',
          ]);
        }
        else {
          $request->validate([
              'reservaciones.0.corrida_id' => 'required|numeric',
              'reservaciones.0.fecha_reservacion' => 'required|date_format:Y-m-d',
              'reservaciones.0.costo_boleto' => 'required|numeric',
              'reservaciones.0.asiento.id' => 'required|integer',
              'reservaciones.0.estatus' => 'required|in:apartado,pagado',
          ]);
        }
        //checamos si aun podemos reservar ese asientos
        //Corrida de ida
        $asiento_sencillo_id = $request->reservaciones[0]['asiento']['id'];
        $from_sencillo =  Carbon::createFromFormat('Y-m-d',$request->reservaciones[0]['fecha_reservacion'])->toDateString().'';

        $r1 = Reservacione::select(['id'])
        ->whereNull('deleted_at')
        ->where('estatus','!=','cancelado')
        ->where('corrida_id',$request->reservaciones[0]['corrida_id'])
        ->where('asiento_id',$asiento_sencillo_id)
        ->where('fecha_reservacion',$from_sencillo)
        ->first();
        if($r1){
          //ya existe una reservación
          return response()->json([
            'message' => 'El asiento '.$request->reservaciones[0]['asiento']['numero'].' de la corrida de ida ya no se puede reservar',
          ],422);
          exit;
        }
        //corrida de regreso
        if($request->tipo_viaje == 'redondo') {
          $asiento_regreso_id = $request->reservaciones[1]['asiento']['id'];
          $from_regreso =  Carbon::createFromFormat('Y-m-d',$request->reservaciones[1]['fecha_reservacion'])->toDateString().'';
          $r2 = Reservacione::select(['id'])
          ->whereNull('deleted_at')
          ->where('estatus','!=','cancelado')
          ->where('corrida_id',$request->reservaciones[1]['corrida_id'])
          ->where('asiento_id',$asiento_regreso_id)
          ->whereDate('fecha_reservacion',$from_regreso)->first();
          if($r2){
            //ya existe una reservación
            return response()->json([
              'message' => 'El asiento '.$request->reservaciones[1]['asiento']['numero'].' de la corrida de regreso ya no se puede reservar',
            ],422);
            exit;
          }
        }
        // fin Validaciones

        /*
        *Si llegamos hasta aqui quiere decir que podemos reservar
        */
        //Reservando asiento de ida
        /*return response()->json([
          'fecha_reservacion' => Carbon::createFromFormat('Y-m-d',$request->reservaciones[0]['fecha_reservacion'])->toDateString(),
        ]);exit;*/
        $user = User::find(Crypt::decrypt($request->user_id));
        // interrumpir su el usuario no tiene sucursal asignada
        if($user->sucursal_id == null){
          return response()->json(['message' => 'El vendedor no tiene sucursal asignada'],422);
        }
        $clave = $this->generarClave();
        $sufix_redondo = ($request->tipo_viaje == 'redondo')? '-1':'';
          $reservacion_salida = new Reservacione();
          $reservacion_salida->cliente = $request->cliente;
          $reservacion_salida->phone = $request->phone;
          $reservacion_salida->costo_boleto = $this->getCostoBoleto($request->reservaciones[0]['costo_boleto'], $request->reservaciones[0]['descuento']);
          $reservacion_salida->anticipo = $request->reservaciones[0]['anticipo'];
          $reservacion_salida->resta = $request->reservaciones[0]['estatus'] == 'apartado'? $reservacion_salida->costo_boleto - $request->reservaciones[0]['anticipo']:0;
          $reservacion_salida->clave = $clave. $sufix_redondo;
          $reservacion_salida->descuento = $request->reservaciones[0]['descuento'];
          $reservacion_salida->asiento_id = $request->reservaciones[0]['asiento_id'];
          $reservacion_salida->corrida_id = $request->reservaciones[0]['corrida_id'];
          $reservacion_salida->vendedor_id = $user->id;
          $reservacion_salida->sucursal_id = $user->sucursal_id;
          $reservacion_salida->observaciones = "";
          $reservacion_salida->descuento = ((int)$request->reservaciones[0]['descuento'] > 0)? true:false;
          $reservacion_salida->estatus = $request->reservaciones[0]['estatus'];
          $reservacion_salida->fecha_reservacion = Carbon::createFromFormat('Y-m-d',$request->reservaciones[0]['fecha_reservacion'])->toDateString().'';
          $reservacion_salida->redondo = ($request->tipo_viaje == 'redondo')? true:false;
          $reservacion_salida->vendedor_anticipa_id = ($request->reservaciones[0]['anticipo'] > 0 && $request->reservaciones[0]['estatus'] == 'apartado')? $user->id:null;
          $reservacion_salida->vendedor_anticipa_fecha = ($request->reservaciones[0]['anticipo'] > 0 && $request->reservaciones[0]['estatus'] == 'apartado')? Carbon::now():null;
          $reservacion_salida->vendedor_pagado_id = ($request->reservaciones[0]['anticipo'] == 0 && $request->reservaciones[0]['estatus'] == 'pagado')? $user->id:null;
          $reservacion_salida->vendedor_pagado_fecha = ($request->reservaciones[0]['anticipo'] == 0 && $request->reservaciones[0]['estatus'] == 'pagado')? Carbon::now():null;
          $reservacion_salida->save();
          $reservacion_salida->corrida = Corrida::find($reservacion_salida->corrida_id);
          $reservacion_salida->asiento = Asiento::find($reservacion_salida->asiento_id);
          $reservacion_salida->costo_boleto = number_format($reservacion_salida->costo_boleto,2);
          $reservacion_salida->fecha_reservacion = Carbon::createFromFormat('Y-m-d',$reservacion_salida->fecha_reservacion)->format('d/m/Y').'';
          if(strlen($request->phone) == 10){
            $this->sendMessage([
              'message' => 'Hola '.$this->ajustaNombreCliente($reservacion_salida->cliente).'. tu código de reservación es '.$reservacion_salida->clave.' con estatus '.$reservacion_salida->estatus .' para la fecha '.$reservacion_salida->fecha_reservacion.' en el asiento número '.$reservacion_salida->asiento->numero,
              'phone' => $request->phone
            ]);
          }
        $reservacion_regreso = null;
        if($request->tipo_viaje == 'redondo'){
          $reservacion_regreso = new Reservacione();
          $reservacion_regreso->cliente = $request->cliente;
          $reservacion_regreso->phone = $request->phone;
          $reservacion_regreso->costo_boleto = $this->getCostoBoleto($request->reservaciones[1]['costo_boleto'], $request->reservaciones[1]['descuento']);
          $reservacion_regreso->anticipo = $request->reservaciones[1]['anticipo'];
          $reservacion_regreso->resta = $request->reservaciones[1]['estatus'] == 'apartado' ? $reservacion_regreso->costo_boleto - $request->reservaciones[1]['anticipo'] : 0;
          $reservacion_regreso->clave = $clave.'-2';
          $reservacion_regreso->descuento = $request->reservaciones[1]['descuento'];
          $reservacion_regreso->asiento_id = $request->reservaciones[1]['asiento_id'];
          $reservacion_regreso->corrida_id = $request->reservaciones[1]['corrida_id'];
          $reservacion_regreso->vendedor_id = $user->id;
          $reservacion_regreso->sucursal_id = $user->sucursal_id;
          $reservacion_regreso->observaciones = "";
          $reservacion_regreso->descuento = ((int)$request->reservaciones[1]['descuento'] > 0)? true:false;
          $reservacion_regreso->estatus = $request->reservaciones[1]['estatus'];
          $reservacion_regreso->fecha_reservacion = Carbon::createFromFormat('Y-m-d',$request->reservaciones[1]['fecha_reservacion'])->toDateString().'';
          $reservacion_regreso->redondo = true;
          $reservacion_regreso->vendedor_anticipa_id = ($request->reservaciones[1]['anticipo'] > 0 && $request->reservaciones[1]['estatus'] == 'apartado')? $user->id : null;
          $reservacion_regreso->vendedor_anticipa_fecha = ($request->reservaciones[1]['anticipo'] > 0 && $request->reservaciones[1]['estatus'] == 'apartado')? Carbon::now() : null;
          $reservacion_regreso->vendedor_pagado_id = ($request->reservaciones[1]['anticipo'] == 0 && $request->reservaciones[1]['estatus'] == 'pagado')? $user->id : null;
          $reservacion_regreso->vendedor_pagado_fecha = ($request->reservaciones[1]['anticipo'] == 0 && $request->reservaciones[1]['estatus'] == 'pagado')? Carbon::now() : null;
          $reservacion_regreso->save();
          $reservacion_regreso->corrida = Corrida::find($reservacion_regreso->corrida_id);
          $reservacion_regreso->asiento = Asiento::find($reservacion_regreso->asiento_id);
          $reservacion_regreso->costo_boleto = number_format($reservacion_regreso->costo_boleto,2);
          $reservacion_regreso->fecha_reservacion = Carbon::createFromFormat('Y-m-d',$reservacion_regreso->fecha_reservacion)->format('d/m/Y').'';

          if(strlen($request->phone) == 10){
            $this->sendMessage([
              'message' => 'Hola '.$this->ajustaNombreCliente($reservacion_regreso->cliente).'. tu código de reservación es '.$reservacion_regreso->clave.' con estatus '.$reservacion_regreso->estatus .' para la fecha '.$reservacion_regreso->fecha_reservacion.' en el asiento número '.$reservacion_regreso->asiento->numero,
              'phone' => $request->phone
            ]);
          }

        }

        return response()->json([
          'message' => 'Se ha completado la reservación del cliente '.$request->cliente,
          'reservacion_salida' => $reservacion_salida,
          'reservacion_regreso' => $reservacion_regreso,
        ]);
    }

    public function cambiosReserva(ReservarCambiosRequest $request) {
      $user = User::find(Crypt::decrypt($request->user_id));
      $config = Configuracion::first();
      $corrida = Corrida::findOrFail($request->corrida_cambio);
      // validando la fechas
      $fecha_cambio = Carbon::parse($request->fecha_cambio);
      $diff_days = Carbon::now()->diffInDays($fecha_cambio, false);
      // validando si la fecha es menor al día de hoy
      if($diff_days < 0){
        return response()->json(['message' => 'Tu fecha de cambio es menor al día de hoy']);
        exit;
      }

      // validando si el nuevo cambio no esta a 15 min de salir
      $fecha_cambio = Carbon::parse($fecha_cambio->format('Y-m-d'). ' ' . $corrida->hora_salida);
      $diff_mins = Carbon::now()->diffInMinutes($fecha_cambio, false);
      if ($diff_mins <= $config->tiempo_espera_reservacion) {
        return response()->json(['message' => 'Ya no puedes hacer cambios para esta reservación']);
        exit;
      }
      // validando si el asiento ya esta reservado
      $fecha_cambio = $fecha_cambio->format('Y-m-d');
      $r1 = Reservacione::select(['id'])
      ->whereNull('deleted_at')
      ->where('estatus','!=','cancelado')
      ->where('corrida_id',$request->corrida_cambio)
      ->where('asiento_id',$request->asiento_cambio)
      ->where('fecha_reservacion',$fecha_cambio)
      ->first();
      if($r1){
        //ya existe una reservación
        return response()->json([
          'message' => 'El asiento '.$request->reservaciones[0]['asiento']['numero'].' de la corrida de ida ya no se puede reservar',
        ],422);
        exit;
      }

      $reservacion_anterior = Reservacione::where('clave',$request->clave)->first();
      $reservacion_anterior->fecha_reservacion = Carbon::createFromFormat('Y-m-d', $fecha_cambio)->toDateString().'';
      $reservacion_anterior->asiento_id = $request->asiento_cambio;
      $reservacion_anterior->corrida_id = $request->corrida_cambio;
      $reservacion_anterior->save();

      return response()->json([
        'message' => 'Reservacion del cliente '.$reservacion_anterior->cliente. ' ha sido cambiada',
      ]);
    }

    private function generarClaveGlobal() {
      $clave = strtoupper(str_random(6)).'GL';
      $exitsClave = Reservacione::where('clave_global',$clave)->select('clave_global')->first();
      if($exitsClave){
        return $this->generarClave();
      } else {
        return $clave;
      }
    }

    private function generarClave() {
      $clave = strtoupper(str_random(6));
      $exitsClave = Reservacione::where('clave',$clave)->select('clave')->first();
      if($exitsClave){
        return $this->generarClave();
      } else {
        return $clave;
      }
    }

    private function getCostoBoleto($costo, $descuento){
      if((int)$descuento > 0){
        return (($costo * $descuento)/100) - $costo;
      }
      else {
        return $costo;
      }
    }

    public function reservacion(Request $request) {
      $reservacion = Reservacione::where('clave',$request->clave)->first();
      if(!$reservacion) {
        return response()->json([
          'message' => 'No se encontró la clave'
        ],422);
      } else {
        $reservacion->corrida = Corrida::findOrFail($reservacion->corrida_id);
        $reservacion->asiento = Asiento::findOrFail($reservacion->asiento_id);
        Carbon::createFromFormat('Y-m-d',$reservacion->fecha_reservacion)->format('d-m-Y');
        return $reservacion;
      }
    }

    public function ajustar() {
      $hoy = Carbon::now()->format('Y-m-d');
      $time_off = Configuracion::first()->tiempo_espera_reservacion;

      $reservaciones = Reservacione::where('estatus','apartado')
      ->select([
        'reservaciones.id as reservacion_id',
        'corridas.hora_salida',
      ])
      ->join('corridas','reservaciones.corrida_id','=','corridas.id')
      ->where('fecha_reservacion', $hoy)
      ->whereNull('reservaciones.deleted_at')
      ->get();

      foreach($reservaciones as $a){
        $a->justo_ahora = Carbon::now();
        $a->hora_salida =  Carbon::createFromFormat('Y-m-d H:i:s',Carbon::now()->format('Y-m-d').' '. $a->hora_salida);
        $a->diff = $a->justo_ahora->diffInMinutes($a->hora_salida);
        if($a->diff <= $time_off){
          Reservacione::where('id',$a->reservacion_id)->update([
            'estatus' => 'cancelado'
          ]);
        }
      }
      return dd([
        'reservaciones' => $reservaciones,
        'tiempo_espera_reservacion' => $time_off,
      ]);
    }

    private function ajustaNombreCliente($cadena){

      //Codificamos la cadena en formato utf8 en caso de que nos de errores
      //$cadena = utf8_encode($cadena);

      //Ahora reemplazamos las letras
      $cadena = str_replace(
          array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
          array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
          $cadena
      );

      $cadena = str_replace(
          array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
          array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
          $cadena );

      $cadena = str_replace(
          array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
          array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
          $cadena );

      $cadena = str_replace(
          array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
          array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
          $cadena );

      $cadena = str_replace(
          array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
          array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
          $cadena );

      $cadena = str_replace(
          array('ñ', 'Ñ', 'ç', 'Ç'),
          array('n', 'N', 'c', 'C'),
          $cadena
      );
      $cadena = title_case($cadena);
      $nombre_arr = explode(" ", $cadena);

      if(count($nombre_arr) > 1){
        $cadena = $nombre_arr[0].' '.substr($nombre_arr[1],0,1);
      }
      return $cadena;
    }

    private function quitarAcentos($cadena){

        //Codificamos la cadena en formato utf8 en caso de que nos de errores
        //$cadena = utf8_encode($cadena);

        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena );

        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );

        return $cadena;
    }

    private function sendMessage($sms){
      $response = Curl::to(env('WABOX_URL'))
        ->withData([
          'token' => env('WABOX_TOKEN'),
          'uid' => env('WABOX_UID'),
          'to' => '52'.$sms['phone'],
          'custom_uid' => env('WABOX_CUSTOM_ID_PREFIX').str_random(10),
          'text' => $sms['message'].' - Notificación del SISTEMA de '.env('EMPRESA_NOMBRE').', favor de no responder.',
        ])
        ->asJson()
        ->post();

        if(isset($response->error)){
          Log::alert($response->error);
          Mensaje::create([
            'message' => $this->quitarAcentos($sms['message']),
            'phone' => $sms['phone']
          ]);
        }
    }

    public function verificarDisponibilidad(Request $request) {
      $request->validate([
        'fecha' => 'required|date_format:d/m/Y',
        'corrida_id' => 'required|exists:corridas,id',
        'asiento_id' => 'required|exists:asientos,id'
      ]);
      // validando si el asiento ya esta reservado
      $from = Carbon::createFromFormat('d/m/Y',$request->fecha);
      $re = Reservacione::select(['id'])
      ->whereNull('deleted_at')
      ->where('estatus','!=','cancelado')
      ->where('corrida_id',$request->corrida_id)
      ->where('asiento_id',$request->asiento_id)
      ->where('fecha_reservacion',$from->format('Y-m-d'))
      ->first();
      if($re){
        //ya existe una reservación
        return response()->json([
          'message' => 'El asiento '.$request->reservaciones[0]['asiento']['numero'].' de la corrida de ida ya no se puede reservar',
        ],422);
        exit;
      } else {
        $costo_boleto = Corrida::find($request->corrida_id)->costo_boleto_sencillo;
        $reservacion = new Reservacione();
        $reservacion->clave = $this->generarClave();
        $reservacion->cliente = 'N/A';
        $reservacion->corrida_id = $request->corrida_id;
        $reservacion->asiento_id = $request->asiento_id;
        $reservacion->fecha_reservacion = $from->toDateString().'';
        $reservacion->estatus = 'apartado';
        $reservacion->is_plt_reservacion = true;
        $reservacion->costo_boleto = $costo_boleto;
        $reservacion->save();
        return $reservacion;
      }
    }

    public function cancelarReservacion($clave) {
      $r = Reservacione::where('clave',$clave)->first();
      if($r->is_plt_reservacion == 1) {
        $r->estatus = 'cancelado';
        $r->save();
      }
      return $r;
    }

    public function reservacionPlataforma(ReservacionPlataforma $request) {
      $clave_global = $this->generarClaveGlobal();
      foreach($request->reservaciones['ida'] as $r){
        Reservacione::where('id',$r['id'])->first()->update([
          'cliente' => $request->nombre,
          'email' => $request->email,
          'clave_global' => $clave_global,
        ]);
      }
      if($request->redondo == 'true') {
        foreach($request->reservaciones['regreso'] as $r){
          Reservacione::where('id',$r['id'])->first()->update([
            'cliente' => $request->nombre,
            'email' => $request->email,
            'clave_global' => $clave_global,
          ]);
        }
      }

      return response()->json([
        'message' => 'Tu clave de reservacion es ' . $clave_global,
        'clave' => $clave_global
      ]);
    }
}
