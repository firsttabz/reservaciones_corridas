<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FechaCorte;
use Carbon\Carbon;
use Session;
use Storage;
class ActivacionSistemaController extends Controller
{
    public function desactivacion(){
      $fecha_corte = FechaCorte::first();
      $now = Carbon::now();
      $corte = Carbon::parse($fecha_corte->fecha_corte);
      $dias = env('DIAS_PARA_CORTE',30);
      $diffDays =  $now->diffInDays($corte, false);

      if($diffDays <= 0) {
        $fecha_corte->activado = false;
        $fecha_corte->save();
      }

      return response()->json([
        'now' => $now,
        'corte' => $corte,
        'diffInDays' => Session::get('dias'),
        'activado' => $fecha_corte->activado,
      ]);
    }

    public function activacion() {
      // return Carbon::now()->addDays(30 + FechaCorte::dias())->format('Y-m-d H:i:s');exit;
      if(FechaCorte::dias() <= 5){
        $corte = FechaCorte::first();
        $corte->fecha_corte = Carbon::now()->addDays(30 + FechaCorte::dias());
        $corte->activado = true;
        $corte->save();
        return $corte;
      } else {
        return response()->json([
          'message' => 'Falta '.(FechaCorte::dias()-5).' dias para poder actualizar la fecha de corte'
        ]);
      }
    }

    public function incomes(Request $request) {
      Storage::disk('public')->put('request.json', json_encode($request->all()));
      return $request->all();
    }
}
