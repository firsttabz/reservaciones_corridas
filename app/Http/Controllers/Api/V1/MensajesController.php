<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mensaje;

class MensajesController extends Controller
{
    public function service(Request $request){
       $mensajes = [];
       if(env('SMS_GATEWAY_PASSWORD') == $request->password){
         $mensajes = Mensaje::select(['id','phone','message'])->orderBy('id', 'DESC')->limit(100)->get();
         foreach($mensajes as $m){
           Mensaje::where('id',$m->id)->delete();
         }
       }
       return $mensajes;
    }
}
