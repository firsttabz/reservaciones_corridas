<?php

namespace App\Http\Controllers\Api\V1;

use Carbon\Carbon;
use App\Corrida;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCorridasRequest;
use App\Http\Requests\Admin\UpdateCorridasRequest;

class CorridasController extends Controller
{
    public function index()
    {
      $corridas = Corrida::whereNull('deleted_at')->where('activo',1)->get();
      foreach ($corridas as $value) {
        $value->nombre .= $value->extraordinaria == 1? '(e) '.Carbon::parse($value->fecha_extraordinaria)->format('d/m/Y'):'';
      }
        return $corridas;
    }

    public function show($id)
    {
        return Corrida::findOrFail($id);
    }

    public function update(UpdateCorridasRequest $request, $id)
    {
        $corrida = Corrida::findOrFail($id);
        $corrida->update($request->all());


        return $corrida;
    }

    public function store(StoreCorridasRequest $request)
    {
        $corrida = Corrida::create($request->all());


        return $corrida;
    }

    public function destroy($id)
    {
        $corrida = Corrida::findOrFail($id);
        $corrida->delete();
        return '';
    }
}
