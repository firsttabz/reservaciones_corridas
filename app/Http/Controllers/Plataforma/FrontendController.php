<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Corrida;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
  public function index()
  {
      $corridas = Corrida::whereNull('deleted_at')->where('activo',1)->where('extraordinaria',0)->get();
      return view('frontend/index',compact('corridas'));
  }
}
