<?php

namespace App\Http\Controllers\Plataforma;
use App\Http\Requests\Plataforma\ReservacionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Reservacione;
use App\Corrida;
use App\Asiento;
use App\Autobus;
use App\Configuracion;
class ReservacionController extends Controller
{
    public function index(ReservacionRequest $request) {
      $corrida = Corrida::find($request->ida);
      $configuracion = Configuracion::first();
      return view('frontend.reservaciones.index', compact('corrida', 'configuracion'));
    }

    public function renderBus(Request $request) {
      $request->validate([
        'corrida_id' => 'required|exists:corridas,id',
        'fecha' => 'required|date_format:d/m/Y',
        'function_name' => 'required|string'
      ]);
      $corrida = Corrida::findOrFail($request->corrida_id);
      $config = Configuracion::first();

      // validando la fechas
      $from = Carbon::createFromFormat('d/m/Y',$request->fecha);
      $diff_days = Carbon::now()->diffInDays($from, false);
      // validando si la fecha es menor al día de hoy
      if($diff_days < 0){
        return response()->json(['message' => 'Tu fecha es menor al día de hoy'],422);
        exit;
      }

      $limite_asientos = $this->obtieneLimiteDeAsientos($from, $corrida);
      $asientos_corrida = Asiento::where('corrida_id',$corrida->id)->limit($limite_asientos)->get();
      $asientos_disponibles = [];
      foreach ($asientos_corrida as $asiento) {
        $asiento->reservacion = Reservacione::select('estatus','asiento_id','fecha_reservacion')
        ->whereNull('deleted_at')
        ->where('estatus','!=','cancelado')
        ->where('asiento_id',$asiento->id)
        ->where('corrida_id',$corrida->id)
        ->whereDate('fecha_reservacion', $from->format('Y-m-d'))
        ->first();
        $asientos_disponibles[] = $asiento;
      }
      // para la vista
      $fila[0] = [];
      $fila[1] = [];
      $fila[2] = [];
      $fila[3] = [];

      foreach ($asientos_disponibles as $a) {
        if($a->numero%4==0){
      	   $fila[0][] = $a;
      	}
      }

      $num = 3;
      foreach ($asientos_disponibles as $a) {
        if($a->numero == $num){
      	   $fila[1][] = $a;
           $num += 4;
      	}
      }

      $num = 2;
      foreach ($asientos_disponibles as $a) {
        if($a->numero == $num){
      	   $fila[2][] = $a;
           $num += 4;
      	}
      }

      $num = 1;
      foreach ($asientos_disponibles as $a) {
        if($a->numero == $num){
      	   $fila[3][] = $a;
           $num += 4;
      	}
      }
      $funcion = $request->function_name;
      return view('frontend.reservaciones.bus_maker', compact('limite', 'fila', 'corrida', 'funcion'));
    }

    private function obtieneLimiteDeAsientos(Carbon $from, $corrida){
      $limite = $corrida->total_asientos;
      $from_gt = $from->gt(Carbon::now());
      if($from_gt == true && $corrida->camion == false){
        $limite = Configuracion::first()->total_asientos;
      }
      return $limite;
    }
}
