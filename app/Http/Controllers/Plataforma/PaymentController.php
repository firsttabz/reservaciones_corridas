<?php

namespace App\Http\Controllers\Plataforma;

use Session;
use Redirect;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use App\Mail\SendEmail;
//PayPal details class
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\PaymentExecution;
//DB
use App\Reservacione;
use App\Corrida;
use App\PaypalPayment;

use URL;
use Request as Req;

class PaymentController extends Controller
{
    //
    private $suma_boletos = [];
    private $_api_context;
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }


    public function create($clave){
        $reservaciones = [];
        if($clave){

            $suma_boletos = (int) Reservacione::where('clave_global','=',$clave)->where('estatus','apartado')->sum('costo_boleto');
            $cantidad_boletos = Reservacione::where('clave_global', '=', $clave)->where('estatus','apartado')->count();
            $reservacion = Reservacione::where('clave_global', '=', $clave)->first();
            $reservaciones = Reservacione::where('clave_global', '=', $clave)->where('estatus','apartado')->get();
            foreach ($reservaciones as $r) {
              $r->corrida = Corrida::find($r->corrida_id);
              //$corridas = $r->corrida->nombre;
            }
              $cantidad_boletos_pagados = Reservacione::where('clave_global', '=', $clave)->where('estatus','pagado')->count();
        }

        if($reservacion->estatus == "cancelado"){
            return view('frontend.paypal.canceled_reservation', compact('clave','reservacion'));
        }

        if(count($reservaciones) == 0) {
          return view('frontend.paypal_paid', compact('clave','reservacion','cantidad_boletos_pagados'));
          //echo 'ya pagaste';
        }
        else {
            return view('frontend.paypal', compact('clave','suma_boletos','cantidad_boletos','reservacion','reservaciones'));
        }

    }


    public function payWithpaypal(Request $request){
      // Validando si hay reservaciones que pagar
      $clave = $request->clave_global;
      $reservaciones = Reservacione::where('clave_global', '=', $clave)->where('estatus','apartado')->get();
      if(count($reservaciones) < 1){
        return back()->withErrors([
          'reservaciones' => 'Ya no hay reservaciones pendientes de la clave '.$clave. ' que pagar'
        ]);
      }


      $clave = $request->clave_global;
      $clave = (int) Reservacione::where('clave_global','=',$clave)
      ->where('estatus','apartado')
      ->sum('costo_boleto');

      $payer = new Payer();
       $payer->setPaymentMethod('paypal');
       //->setExternalSelectedFundingInstrumentType('PAY_UPON_INVOICE');

       $item_1 = new Item();

       $item_1->setName('Reservación') /** item name **/
           ->setCurrency('MXN')
           ->setQuantity(1)
           ->setPrice($clave); /** unit price **/

       $item_list = new ItemList();
       $item_list->setItems(array($item_1));

       $amount = new Amount();
       $amount->setCurrency('MXN')
           ->setTotal($clave);

       $transaction = new Transaction();
       $transaction->setAmount($amount)
           ->setItemList($item_list)
           ->setDescription('Your transaction description');

       $redirect_urls = new RedirectUrls();
       $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
           ->setCancelUrl(URL::route('status'));

       $payment = new Payment();
       $payment->setIntent('Sale')
           ->setPayer($payer)
           ->setRedirectUrls($redirect_urls)
           ->setTransactions(array($transaction));
       /** dd($payment->create($this->_api_context));exit; **/
       try {

           $payment->create($this->_api_context);

       } catch (\PayPal\Exception\PPConnectionException $ex) {

           if (\Config::get('app.debug')) {

               \Session::put('error', 'Connection timeout');
               return Redirect::route('paywithpaypal');

           } else {

               \Session::put('error', 'Some error occur, sorry for inconvenient');
               return Redirect::route('paywithpaypal');

           }

           }

           foreach ($payment->getLinks() as $link) {

               if ($link->getRel() == 'approval_url') {

                   $redirect_url = $link->getHref();
                   break;

               }

           }

           /** add payment ID to session **/
           Session::put('paypal_payment_id', $payment->getId());
           //dd($payment->saleId);
            $paypal_payment = new PaypalPayment();
            $paypal_payment->payment_id = $payment->id;
            $paypal_payment->state = $payment->state;
            $paypal_payment->clave_reservacion_global = $request->clave_global;
            $paypal_payment->save();

           if (isset($redirect_url)) {

               /** redirect to paypal **/
               return Redirect::away($redirect_url);

           }

           \Session::put('error', 'Unknown error occurred');
           return Redirect::route('paywithpaypal');

    }

    public function getPaymentStatus(){
         /** Get the payment ID before session clear **/
         $payment_id = Req::input('paymentId');
         $paypal_payment = PaypalPayment::where('payment_id', '=', $payment_id)->first();
         $clave = $paypal_payment->clave_reservacion_global;



         /** clear the session payment ID **/
         Session::forget('paypal_payment_id');
         if (empty(Req::input('PayerID')) || empty(Req::input('token'))) {

             \Session::put('error', 'Payment failed');

             return Redirect::route('status' );

         }

         $payment = Payment::get($payment_id, $this->_api_context);
         $execution = new PaymentExecution();
         $execution->setPayerId(Req::input('PayerID'));

         /**Execute the payment **/
         $result = $payment->execute($execution, $this->_api_context);
         //die('hasta aqui todo bien');
         if ($result->getState() == 'approved') {
           $transactions = $result->getTransactions();
           $resources = $transactions[0]->getRelatedResources();

           $sale = $resources[0]->getSale();
           $salesID = $sale->getId();

           //dd($saleID);exit;

           $approved_payment = PaypalPayment::find(1);
           $paypal_payment->state =  $result->getState();
           $paypal_payment->sales_id = $salesID;
           $paypal_payment->save();

           Reservacione::where('clave_global','=',$clave)
           ->where('estatus', '=', 'apartado')
           ->update([
             'estatus' => 'pagado',
             'is_plt_reservacion' => 1
           ]);

           $reservaciones = Reservacione::where('clave_global','=',$clave)
           ->where('estatus', '=', 'pagado')
           ->get();
           $suma_boletos = (int) Reservacione::where('clave_global','=',$clave)
           ->where('estatus','pagado')
           ->sum('costo_boleto');
           $cantidad_boletos_pagados = Reservacione::where('clave_global', '=', $clave)
           ->where('estatus','pagado')
           ->count();
           $cliente = $reservaciones[0]->cliente;
           $email = $reservaciones[0]->email;
           $tipo_viaje = $reservaciones[0]->redondo;
           $fecha_reservacion = $reservaciones[0]->fecha_reservacion;


           Mail::to($email)->send(new SendEmail($clave, $suma_boletos, $cantidad_boletos_pagados, $reservaciones, $cliente, $email, $tipo_viaje, $fecha_reservacion));

             //\Session::put('success', 'Payment success');
             return Redirect::route('pagoconpaypal',['clave'=>$clave]);
             //return 'Pagado';

         }

         \Session::put('error', 'Payment failed');

         $approved_payment = PaypalPayment::find(1);
         $paypal_payment->state =  $result->getState();
         $paypal_payment->save();
         //return Redirect::route('status');
         return Redirect::route('pagoconpaypal',['clave'=>$clave]);
    }

}
