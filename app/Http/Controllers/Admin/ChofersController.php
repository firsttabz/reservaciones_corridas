<?php

namespace App\Http\Controllers\Admin;

use App\Chofer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreChofersRequest;
use App\Http\Requests\Admin\UpdateChofersRequest;
use App\Http\Controllers\Traits\FileUploadTrait;

class ChofersController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Chofer.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('chofer_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('chofer_delete')) {
                return abort(401);
            }
            $chofers = Chofer::onlyTrashed()->get();
        } else {
            $chofers = Chofer::all();
        }

        return view('admin.chofers.index', compact('chofers'));
    }

    /**
     * Show the form for creating new Chofer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('chofer_create')) {
            return abort(401);
        }
        return view('admin.chofers.create');
    }

    /**
     * Store a newly created Chofer in storage.
     *
     * @param  \App\Http\Requests\StoreChofersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChofersRequest $request)
    {
        if (! Gate::allows('chofer_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $chofer = Chofer::create($request->all());


        foreach ($request->input('documentos_id', []) as $index => $id) {
            $model          = config('laravel-medialibrary.media_model');
            $file           = $model::find($id);
            $file->model_id = $chofer->id;
            $file->save();
        }

        return redirect()->route('admin.chofers.index');
    }


    /**
     * Show the form for editing Chofer.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('chofer_edit')) {
            return abort(401);
        }
        $chofer = Chofer::findOrFail($id);

        return view('admin.chofers.edit', compact('chofer'));
    }

    /**
     * Update Chofer in storage.
     *
     * @param  \App\Http\Requests\UpdateChofersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChofersRequest $request, $id)
    {
        if (! Gate::allows('chofer_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $chofer = Chofer::findOrFail($id);
        $chofer->update($request->all());


        $media = [];
        foreach ($request->input('documentos_id', []) as $index => $id) {
            $model          = config('laravel-medialibrary.media_model');
            $file           = $model::find($id);
            $file->model_id = $chofer->id;
            $file->save();
            $media[] = $file->toArray();
        }
        $chofer->updateMedia($media, 'documentos');

        return redirect()->route('admin.chofers.index');
    }


    /**
     * Display Chofer.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('chofer_view')) {
            return abort(401);
        }
        $corridas = \App\Corrida::where('chofer_id', $id)->get();

        $chofer = Chofer::findOrFail($id);

        return view('admin.chofers.show', compact('chofer', 'corridas'));
    }


    /**
     * Remove Chofer from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('chofer_delete')) {
            return abort(401);
        }
        $chofer = Chofer::findOrFail($id);
        $chofer->deletePreservingMedia();

        return redirect()->route('admin.chofers.index');
    }

    /**
     * Delete all selected Chofer at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('chofer_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Chofer::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->deletePreservingMedia();
            }
        }
    }


    /**
     * Restore Chofer from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('chofer_delete')) {
            return abort(401);
        }
        $chofer = Chofer::onlyTrashed()->findOrFail($id);
        $chofer->restore();

        return redirect()->route('admin.chofers.index');
    }

    /**
     * Permanently delete Chofer from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('chofer_delete')) {
            return abort(401);
        }
        $chofer = Chofer::onlyTrashed()->findOrFail($id);
        $chofer->forceDelete();

        return redirect()->route('admin.chofers.index');
    }
}
