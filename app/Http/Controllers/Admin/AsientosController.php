<?php

namespace App\Http\Controllers\Admin;

use App\Asiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAsientosRequest;
use App\Http\Requests\Admin\UpdateAsientosRequest;

class AsientosController extends Controller
{
    /**
     * Display a listing of Asiento.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('asiento_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('asiento_delete')) {
                return abort(401);
            }
            $asientos = Asiento::onlyTrashed()->get();
        } else {
            $asientos = Asiento::all();
        }

        return view('admin.asientos.index', compact('asientos'));
    }

    /**
     * Show the form for creating new Asiento.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('asiento_create')) {
            return abort(401);
        }

        $corridas = \App\Corrida::get()->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        return view('admin.asientos.create', compact('corridas'));
    }

    /**
     * Store a newly created Asiento in storage.
     *
     * @param  \App\Http\Requests\StoreAsientosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAsientosRequest $request)
    {
        if (! Gate::allows('asiento_create')) {
            return abort(401);
        }
        $asiento = Asiento::create($request->all());



        return redirect()->route('admin.asientos.index');
    }


    /**
     * Show the form for editing Asiento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('asiento_edit')) {
            return abort(401);
        }

        $corridas = \App\Corrida::get()->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        $asiento = Asiento::findOrFail($id);

        return view('admin.asientos.edit', compact('asiento', 'corridas'));
    }

    /**
     * Update Asiento in storage.
     *
     * @param  \App\Http\Requests\UpdateAsientosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAsientosRequest $request, $id)
    {
        if (! Gate::allows('asiento_edit')) {
            return abort(401);
        }
        $asiento = Asiento::findOrFail($id);
        $asiento->update($request->all());



        return redirect()->route('admin.asientos.index');
    }


    /**
     * Display Asiento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('asiento_view')) {
            return abort(401);
        }

        $autobuses = \App\Autobus::get()->pluck('placas', 'id')->prepend(trans('quickadmin.qa_please_select'), '');$reservaciones = \App\Reservacione::where('asiento_id', $id)->get();

        $asiento = Asiento::findOrFail($id);

        return view('admin.asientos.show', compact('asiento', 'reservaciones'));
    }


    /**
     * Remove Asiento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('asiento_delete')) {
            return abort(401);
        }
        $asiento = Asiento::findOrFail($id);
        $asiento->delete();

        return redirect()->route('admin.asientos.index');
    }

    /**
     * Delete all selected Asiento at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('asiento_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Asiento::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Asiento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('asiento_delete')) {
            return abort(401);
        }
        $asiento = Asiento::onlyTrashed()->findOrFail($id);
        $asiento->restore();

        return redirect()->route('admin.asientos.index');
    }

    /**
     * Permanently delete Asiento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('asiento_delete')) {
            return abort(401);
        }
        $asiento = Asiento::onlyTrashed()->findOrFail($id);
        $asiento->forceDelete();

        return redirect()->route('admin.asientos.index');
    }
}
