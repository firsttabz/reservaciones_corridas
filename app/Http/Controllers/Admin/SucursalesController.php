<?php

namespace App\Http\Controllers\Admin;

use App\Sucursale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreSucursalesRequest;
use App\Http\Requests\Admin\UpdateSucursalesRequest;

class SucursalesController extends Controller
{
    /**
     * Display a listing of Sucursale.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('sucursale_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('sucursale_delete')) {
                return abort(401);
            }
            $sucursales = Sucursale::onlyTrashed()->get();
        } else {
            $sucursales = Sucursale::all();
        }

        return view('admin.sucursales.index', compact('sucursales'));
    }

    /**
     * Show the form for creating new Sucursale.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('sucursale_create')) {
            return abort(401);
        }
        $estado = config('estados');
        return view('admin.sucursales.create',compact('estado'));
    }

    /**
     * Store a newly created Sucursale in storage.
     *
     * @param  \App\Http\Requests\StoreSucursalesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSucursalesRequest $request)
    {
        if (! Gate::allows('sucursale_create')) {
            return abort(401);
        }
        $sucursale = Sucursale::create($request->all());



        return redirect()->route('admin.sucursales.index');
    }


    /**
     * Show the form for editing Sucursale.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('sucursale_edit')) {
            return abort(401);
        }
        $sucursale = Sucursale::findOrFail($id);
        $estado = config('estados');

        return view('admin.sucursales.edit', compact('sucursale','estado'));
    }

    /**
     * Update Sucursale in storage.
     *
     * @param  \App\Http\Requests\UpdateSucursalesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSucursalesRequest $request, $id)
    {
        if (! Gate::allows('sucursale_edit')) {
            return abort(401);
        }
        $sucursale = Sucursale::findOrFail($id);
        $sucursale->update($request->all());



        return redirect()->route('admin.sucursales.index');
    }


    /**
     * Display Sucursale.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('sucursale_view')) {
            return abort(401);
        }
        $users = \App\User::where('sucursal_id', $id)->get();

        $sucursale = Sucursale::findOrFail($id);

        return view('admin.sucursales.show', compact('sucursale', 'users'));
    }


    /**
     * Remove Sucursale from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('sucursale_delete')) {
            return abort(401);
        }
        $sucursale = Sucursale::findOrFail($id);
        $sucursale->delete();

        return redirect()->route('admin.sucursales.index');
    }

    /**
     * Delete all selected Sucursale at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('sucursale_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Sucursale::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Sucursale from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('sucursale_delete')) {
            return abort(401);
        }
        $sucursale = Sucursale::onlyTrashed()->findOrFail($id);
        $sucursale->restore();

        return redirect()->route('admin.sucursales.index');
    }

    /**
     * Permanently delete Sucursale from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('sucursale_delete')) {
            return abort(401);
        }
        $sucursale = Sucursale::onlyTrashed()->findOrFail($id);
        $sucursale->forceDelete();

        return redirect()->route('admin.sucursales.index');
    }
}
