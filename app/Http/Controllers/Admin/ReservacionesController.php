<?php

namespace App\Http\Controllers\Admin;

use App\Reservacione;
use App\Corrida;
use App\Configuracion;
use App\Mensaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreReservacionesRequest;
use App\Http\Requests\Admin\UpdateReservacionesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Carbon\Carbon;
use Auth;
use Curl;
use Log;

class ReservacionesController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Reservacione.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (! Gate::allows('reservacione_access')) {
            return abort(401);
        }
        $reservaciones = [];
        $palabras = explode("-",$request->codigo);
        //print_r($palabras);
        if($request->codigo){
          $reservaciones = Reservacione::where('clave','like',$palabras[0].'%')
          //->where('clave_global','like',$palabras[0].'%')
          ->orWhere('cliente', 'like', '%'.$palabras[0].'%')
          ->orWhere('phone', '=', $palabras[0]);
          if($request->mostrar_cancelados){
            $reservaciones->where('estatus', '!=', 'cancelado');
          }
          $reservaciones =  $reservaciones->get();

        }
        foreach ($reservaciones as $r) {
          $r->corrida = Corrida::findOrFail($r->corrida_id);
        }
        /*if (request('show_deleted') == 1) {
            if (! Gate::allows('reservacione_delete')) {
                return abort(401);
            }
            $reservaciones = Reservacione::onlyTrashed()->get();
        } else {
            $reservaciones = Reservacione::all();
        }*/

        return view('admin.reservaciones.index',  compact('reservaciones'));
    }

    /**
     * Show the form for creating new Reservacione.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('reservacione_create')) {
            return abort(401);
        }

        $vendedors = \App\User::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $asientos = \App\Asiento::get()->pluck('numero', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $corridas = \App\Corrida::get()->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        return view('admin.reservaciones.create', compact('vendedors', 'asientos', 'corridas'));
    }

    /**
     * Store a newly created Reservacione in storage.
     *
     * @param  \App\Http\Requests\StoreReservacionesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReservacionesRequest $request)
    {
        if (! Gate::allows('reservacione_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $reservacione = Reservacione::create($request->all());



        return redirect()->route('admin.reservaciones.index');
    }


    /**
     * Show the form for editing Reservacione.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('reservacione_edit')) {
            return abort(401);
        }

        $vendedors = \App\User::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $asientos = \App\Asiento::get()->pluck('numero', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $corridas = \App\Corrida::get()->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        $reservacione = Reservacione::findOrFail($id);

        return view('admin.reservaciones.edit', compact('reservacione', 'vendedors', 'asientos', 'corridas'));
    }

    /**
     * Update Reservacione in storage.
     *
     * @param  \App\Http\Requests\UpdateReservacionesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReservacionesRequest $request, $id)
    {
        if (! Gate::allows('reservacione_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $reservacione = Reservacione::findOrFail($id);
        // $reservacione->update($request->all());



        return redirect()->route('admin.reservaciones.index');
    }


    /**
     * Display Reservacione.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('reservacione_view')) {
            return abort(401);
        }
        $reservacione = Reservacione::findOrFail($id);

        return view('admin.reservaciones.show', compact('reservacione'));
    }


    /**
     * Remove Reservacione from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('reservacione_delete')) {
            return abort(401);
        }
        $reservacione = Reservacione::findOrFail($id);
        $reservacione->delete();

        return redirect()->route('admin.reservaciones.index');
    }

    /**
     * Delete all selected Reservacione at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('reservacione_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Reservacione::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Reservacione from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('reservacione_delete')) {
            return abort(401);
        }
        $reservacione = Reservacione::onlyTrashed()->findOrFail($id);
        $reservacione->restore();

        return redirect()->route('admin.reservaciones.index');
    }

    /**
     * Permanently delete Reservacione from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('reservacione_delete')) {
            return abort(401);
        }
        $reservacione = Reservacione::onlyTrashed()->findOrFail($id);
        $reservacione->forceDelete();

        return redirect()->route('admin.reservaciones.index');
    }

    public function boletoCancelacion($clave) {
        $reservacion = Reservacione::where('clave',$clave)->first();
        //dd($reservacion);

        $salida = Corrida::where('id',$reservacion->corrida_id)->first();

        $reservacion_carbon =  Carbon::parse($reservacion->fecha_reservacion.' '.$salida->hora_salida) ;

        $now = Carbon::now();
        $diffMin =  ($now->diffInMinutes($reservacion_carbon,false));

        $espera = Configuracion::first()->tiempo_espera_reservacion;

        if($diffMin >= $espera){

            Reservacione::where('clave','=',$clave)
            ->update([
                'estatus'=> 'cancelado',
                'vendedor_cancela_id' => Auth::id(),
                'vendedor_cancela_fecha' => Carbon::now(),
            ]);

            if($reservacion->phone){
              $this->sendMessage([
                'message' => $this->ajustaNombreCliente($reservacion->cliente).'. tu reservación '.$reservacion->clave.' ha sido cancelada.',
                'phone' => $reservacion->phone
              ]);
            }
            return back();

        } else {
          return back()->withErrors([
            'clave' => 'El boleto '.$clave.' ya no puede ser cancelado, la corrida esta por salir o ya salió '. $salida->nombre.' '.$salida->hora_salida,
          ]);
        }

    }

    public function boletoLiquidacion($clave){
      $reservacion = Reservacione::where('clave',$clave)->first();
      if($reservacion->estatus == 'cancelado'){
        return back()->withErrors([
          'clave' => 'El boleto '.$clave.' tiene estatus cancelado, no se puede liquidar',
        ]);
      } else {
        $reservacion->estatus = "pagado";
        $reservacion->vendedor_liquida_id = Auth::id();
        $reservacion->vendedor_liquida_fecha = Carbon::now();
        $reservacion->save();
        return back();
      }
    }

    public function boletoCambio() {
      return view('admin.cambios.index');
    }

    private function ajustaNombreCliente($cadena){

      //Codificamos la cadena en formato utf8 en caso de que nos de errores
      //$cadena = utf8_encode($cadena);

      //Ahora reemplazamos las letras
      $cadena = str_replace(
          array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
          array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
          $cadena
      );

      $cadena = str_replace(
          array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
          array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
          $cadena );

      $cadena = str_replace(
          array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
          array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
          $cadena );

      $cadena = str_replace(
          array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
          array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
          $cadena );

      $cadena = str_replace(
          array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
          array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
          $cadena );

      $cadena = str_replace(
          array('ñ', 'Ñ', 'ç', 'Ç'),
          array('n', 'N', 'c', 'C'),
          $cadena
      );
      $cadena = title_case($cadena);
      $nombre_arr = explode(" ", $cadena);

      if(count($nombre_arr) > 1){
        $cadena = $nombre_arr[0].' '.substr($nombre_arr[1],0,1);
      }
      return $cadena;
    }

    private function quitarAcentos($cadena){

        //Codificamos la cadena en formato utf8 en caso de que nos de errores
        //$cadena = utf8_encode($cadena);

        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena );

        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );

        return $cadena;
    }

    private function sendMessage($sms){
      $response = Curl::to(env('WABOX_URL'))
        ->withData([
          'token' => env('WABOX_TOKEN'),
          'uid' => env('WABOX_UID'),
          'to' => '52'.$sms['phone'],
          'custom_uid' => env('WABOX_CUSTOM_ID_PREFIX').str_random(10),
          'text' => $sms['message']
        ])
        ->asJson()
        ->post();

        if(isset($response->error)){
          Log::alert($response->error);
          Mensaje::create([
            'message' => $this->quitarAcentos($sms['message']),
            'phone' => $sms['phone']
          ]);
        }
    }
}
