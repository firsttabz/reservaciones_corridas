<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Reservacione;
use App\Corrida;
use App\User;
use App\Envio;
use App\Asiento;
use Carbon\Carbon;
use Crypt;
use Auth;

class CorteDeCajasController extends Controller
{
    public function index(Request $request)
    {
        if (! Gate::allows('corte_de_caja_access')) {
            return abort(401);
        }
        // return dd($request->all());
        $users = User::all();
        $users = $users->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        //Verificando si existen ventas
        if($request->all()){
              $request->user_id = Auth::user()->role_id != 1 ? Crypt::decrypt($request->user_id) : $request->user_id;
              $vendedor = User::find($request->user_id);
              //validando el request
              $request->validate([
                  'from' => 'required|date_format:d/m/Y',
                  // 'tipo_fecha' => 'required|in:ingreso_dinero,fecha_reservacion'
                  //'to' => 'required|date_format:d/m/Y',
                  // 'user_id' => 'required',
                  //'corrida_id' => 'required',
              ]);
              //creando tiempo en base al formatgo dado para la cosulta
              $from    = Carbon::createFromFormat('d/m/Y',$request->from)->startOfDay();//;
              $to      = Carbon::createFromFormat('d/m/Y',$request->from)->endOfDay();//->toDateString();
              $from_individual = Carbon::createFromFormat('d/m/Y',$request->from)->toDateString();

              if($request->tipo == 'paqueteria') {
                return $this->view_paqueteria($from, $to, $request->user_id, $users);
              } else {
                if($request->tipo_fecha == 'ingreso_dinero') {
                  return $this->view_reservaciones_por_ingresa_dinero($from_individual,$from, $to, $request->user_id, $request->tipo_fecha, $users);
                } else {
                  if($request->user_id != '') {
                    return $this->view_reservaciones_por_fecha_reservacion($from, $to, $request->user_id, $users);
                  } else {
                    return back()->withErrors([
                      'user_id' => 'Para corte de caja por fecha de reservación requieres seleccionar a un vendedor',
                    ]);
                  }
                }
              }
      }
      return view('admin.corte_de_cajas.index', compact('users'));
  }

  private function  view_reservaciones_por_fecha_reservacion($from, $to, $user_id, $users) {
    $fechas_pagado = [];
    $fechas_anticipado = [];
    $fechas_liquidado = [];
    $_from = clone $from;
    $_to = clone $to->endOfDay();

    $fecha_mayor = false;
    do{
      $r_p = Reservacione::whereDate('fecha_reservacion', $_from)
                          ->where('estatus','!=', 'cancelado')
                          ->where('vendedor_pagado_id', $user_id);
      $fechas_pagado[] = [
        'fecha' => $_from->format('d/m/Y'),
        'reservaciones' => $r_p->get(),
        'sum' => $r_p->sum('costo_boleto')
      ];
      $r_a = Reservacione::whereDate('fecha_reservacion', $_from)
                          ->where('estatus','!=', 'cancelado')
                          ->where('vendedor_anticipa_id', $user_id);
      $fechas_anticipado[] = [
        'fecha' => $_from->format('d/m/Y'),
        'reservaciones' => $r_a->get(),
        'sum' => $r_a->sum('anticipo')

      ];
      $r_l = Reservacione::whereDate('fecha_reservacion', $_from)
                          ->where('estatus','!=', 'cancelado')
                          ->where('vendedor_liquida_id', $user_id);
      $fechas_liquidado [] = [
        'fecha' => $_from->format('d/m/Y'),
        'reservaciones' => $r_l->get(),
        'sum' => $r_l->sum('resta')

      ];
      $_from = $_from->addDay();
      $fecha_mayor = $_from->greaterThan($_to);
    } while($fecha_mayor == false);


    //return dd(compact('fechas_pagado', 'fechas_anticipado', 'fechas_liquidado', 'users'));
    return view('admin.corte_de_cajas.index', compact('fechas_pagado', 'fechas_anticipado', 'fechas_liquidado', 'users'));
  }
  private function view_reservaciones_por_ingresa_dinero($from_individual,$from, $to, $user_id, $tipo_fecha, $users) {
      $fechas_pagado = [];
      $fechas_anticipado = [];
      $fechas_liquidado = [];
      $_from = clone $from;
      $_to = clone $to;
      $fecha_mayor = false;
      //return dd($to);
        $tabla_r_pagado = 'vendedor_pagado_fecha';
        $tabla_r_anticipado = 'vendedor_anticipa_fecha';
        $tabla_r_liquidado = 'vendedor_liquida_fecha';
        /******* OPERACIONES DEL DIA SELECCIONADO (CREATED_AT Y VENDEDOR FECHA) **********/
        $result_pagado_del_dia = Reservacione::whereBetween('vendedor_pagado_fecha', [$from, $to])
              ->whereBetween('created_at', [$from, $to])
              ->whereBetween('fecha_reservacion',[$from, $to])
              ->where('vendedor_pagado_id',$user_id)
              ->whereNull('deleted_at')
              ->where('estatus','pagado')
              ->where(function ($q) {
                $q->whereNull('vendedor_liquida_fecha')
                  ->whereNull('vendedor_anticipa_fecha');
              })->get();
          $result_pagado_del_dia_PL = Reservacione::whereBetween('vendedor_pagado_fecha', [$from, $to])
            ->whereBetween('created_at', [$from, $to])
            ->whereBetween('fecha_reservacion',[$from, $to])
            ->where('vendedor_pagado_id',$user_id)
            ->whereNull('deleted_at')
            ->where('estatus','pagado')
            ->where(function ($q) {
              $q->whereNull('vendedor_liquida_fecha')
                ->whereNull('vendedor_anticipa_fecha');
            });
              $reservaciones_pagadas_NV = [];
              $array_reservaciones_pagadas = [];
              for ($i=0; $i < $result_pagado_del_dia->count() ; $i++) {
                if ($result_pagado_del_dia[$i]->redondo == 1) {
                    $palabras = explode("-",$result_pagado_del_dia[$i]->clave);
                    $reservaciones_pagadas_NV[] = Reservacione::where('clave','like',$palabras[0].'%')->orderBy('fecha_reservacion', 'ASC')->get();
                }else {
                    $reservaciones_pagadas_NV[] = Reservacione::where('clave',$result_pagado_del_dia[$i]->clave)->orderBy('fecha_reservacion', 'ASC')->get();
                }
              }
              foreach ($reservaciones_pagadas_NV as $rpd) {
                foreach ($rpd as $r) {
                  //print_r('<br>'.$r.'<br>');
                  $array_reservaciones_pagadas[] = $r;
                }
              }
              $array_reservaciones_pagadas = collect($array_reservaciones_pagadas);
          //return dd($array_reservaciones_pagadas->toArray());

          /******* LIQUIDACIONES *********/
        $result_liquidado_del_dia = Reservacione::whereBetween('vendedor_liquida_fecha', [$from, $to])
          //->whereBetween('created_at', [$from, $to])
          ->whereBetween('fecha_reservacion', [$from, $to])
          ->where('vendedor_liquida_id',$user_id)
          ->whereNull('vendedor_pagado_fecha')
          ->whereNull('deleted_at')
          ->where('estatus','pagado')->get();

          $reservaciones_liquidadas_NV = [];
          $array_reservaciones_liquidadas = [];
          for ($i=0; $i < $result_liquidado_del_dia->count() ; $i++) {
            if ($result_liquidado_del_dia[$i]->redondo == 1) {
                $palabras = explode("-",$result_liquidado_del_dia[$i]->clave);
                $reservaciones_liquidadas_NV[] = Reservacione::where('clave','like',$palabras[0].'%')->orderBy('fecha_reservacion', 'ASC')->get();
            }else {
                $reservaciones_liquidadas_NV[] = Reservacione::where('clave',$result_liquidado_del_dia[$i]->clave)->orderBy('fecha_reservacion', 'ASC')->get();
            }
          }
          foreach ($reservaciones_liquidadas_NV as $rpd) {
            foreach ($rpd as $r) {
              //print_r('<br>'.$r.'<br>');
              $array_reservaciones_liquidadas[] = $r;
            }
          }
          $array_reservaciones_liquidadas = collect($array_reservaciones_liquidadas);
          //return dd($array_reservaciones_liquidadas->toArray());
          $result_liquidado_de_otros_dias_v2 = Reservacione::whereBetween('vendedor_liquida_fecha', [$from, $to])
            ->whereNotBetween('fecha_reservacion', [$from, $to])
            ->whereNull('vendedor_pagado_fecha')
            ->whereNull('deleted_at')
            ->where('estatus','pagado')
            ->where('vendedor_liquida_id',$user_id);
          $result_liquidado_de_otros_dias_NV = Reservacione::whereBetween('vendedor_liquida_fecha', [$from, $to])
            ->whereNotBetween('fecha_reservacion', [$from, $to])
            ->whereNull('vendedor_pagado_fecha')
            ->whereNull('deleted_at')
            ->where('estatus','pagado')
            ->where('vendedor_liquida_id',$user_id)->get();

            $array_reservaciones_liquidadas_de_otros_dias = [];
            $reservaciones_liquidadas_de_otros_dias_NV = [];
            for ($i=0; $i < $result_liquidado_de_otros_dias_NV->count(); $i++) {
              $var = true;
              for ($j=0; $j < $array_reservaciones_liquidadas->count() ; $j++) {
                  if ($result_liquidado_de_otros_dias_NV[$i]->clave == $array_reservaciones_liquidadas[$j]->clave) {
                      $var = false;
                  }
              }
              if ($var == true) {
                $reservaciones_liquidadas_de_otros_dias_NV[] = Reservacione::where('clave',$result_liquidado_de_otros_dias_NV[$i]->clave)->orderBy('fecha_reservacion', 'ASC')->get();
              }
            }
            foreach ($reservaciones_liquidadas_de_otros_dias_NV as $rpdod) {
              foreach ($rpdod as $r) {
                $array_reservaciones_liquidadas_de_otros_dias[] = $r;
              }
            }
          $array_reservaciones_liquidadas_de_otros_dias = collect($array_reservaciones_liquidadas_de_otros_dias);
          //return dd($array_reservaciones_liquidadas_de_otros_dias->toArray());
            /**************** END LIQUIDACIONES *********/

        $result_anticipo_del_dia = Reservacione::whereBetween('vendedor_anticipa_fecha', [$from, $to])
          ->whereBetween('created_at', [$from, $to])
          ->whereBetween('fecha_reservacion',[$from, $to])
          ->where('vendedor_anticipa_id',$user_id)
          ->whereNull('deleted_at')
          ->whereNull('vendedor_pagado_fecha')
          ->where('estatus', '!=', 'cancelado')
          ->where('anticipo' , '>', 0);
        /**********************************************************************************/
        /******* OPERACIONES DE OTROS DÍAS A PARTIR DEL SELECCIONADO (CREATED_AT Y VENDEDOR FECHA) **********/
        $result_pagado_de_otros_dias = Reservacione::whereBetween('vendedor_pagado_fecha', [$from, $to])
          ->whereBetween('created_at', [$from, $to])
          ->whereNotBetween('fecha_reservacion',[$from, $to])
          ->where('vendedor_pagado_id',$user_id)
          ->whereNull('deleted_at')
          ->where('estatus','pagado')
          ->where(function ($q) {
            $q->whereNull('vendedor_liquida_fecha')
              ->whereNull('vendedor_anticipa_fecha');
          })->get();
          $result_pagado_de_otros_dias_PL = Reservacione::whereBetween('vendedor_pagado_fecha', [$from, $to])
            ->whereBetween('created_at', [$from, $to])
            ->whereNotBetween('fecha_reservacion',[$from, $to])
            ->where('vendedor_pagado_id',$user_id)
            ->whereNull('deleted_at')
            ->where('estatus','pagado')
            ->where(function ($q) {
              $q->whereNull('vendedor_liquida_fecha')
                ->whereNull('vendedor_anticipa_fecha');
            });
          $array_reservaciones_pagadas_de_otros_dias = [];
          $reservaciones_pagadas_de_otros_dias_NV = [];
          for ($i=0; $i < $result_pagado_de_otros_dias->count(); $i++) {
            $var = true;
            for ($j=0; $j < $array_reservaciones_pagadas->count() ; $j++) {
                if ($result_pagado_de_otros_dias[$i]->clave == $array_reservaciones_pagadas[$j]->clave) {
                    $var = false;
                }
            }
            if ($var == true) {
              $reservaciones_pagadas_de_otros_dias_NV[] = Reservacione::where('clave',$result_pagado_de_otros_dias[$i]->clave)->orderBy('fecha_reservacion', 'ASC')->get();
            }
          }
          foreach ($reservaciones_pagadas_de_otros_dias_NV as $rpdod) {
            foreach ($rpdod as $r) {
              $array_reservaciones_pagadas_de_otros_dias[] = $r;
            }
          }
        $array_reservaciones_pagadas_de_otros_dias = collect($array_reservaciones_pagadas_de_otros_dias);
          //return dd($array_reservaciones_pagadas_de_otros_dias->toArray());
        $result_liquidado_de_otros_dias = Reservacione::whereBetween('vendedor_liquida_fecha', [$from, $to])
          ->whereBetween('created_at', [$from, $to])
          ->whereNotBetween('fecha_reservacion',[$from, $to])
          ->where('vendedor_liquida_id',$user_id)
          ->whereNull('deleted_at')
          ->whereNull('vendedor_pagado_fecha')
          ->where('estatus','pagado');
          //dd($result_liquidado->get());
        $result_anticipo_de_otros_dias = Reservacione::whereBetween('vendedor_anticipa_fecha', [$from, $to])
          ->whereBetween('created_at', [$from, $to])
          ->whereNotBetween('fecha_reservacion',[$from, $to])
          ->where('vendedor_anticipa_id',$user_id)
          ->whereNull('deleted_at')
          ->whereNull('vendedor_pagado_fecha')
          ->where('estatus', '!=', 'cancelado')
          ->where('anticipo' , '>', 0);
        /**********************************************************************************/
        $result_pagado_de_otros_dias_v2 = Reservacione::whereBetween('vendedor_pagado_fecha', [$from, $to])
          ->whereBetween('created_at', [$from, $to])
          ->whereNotBetween('fecha_reservacion',[$from, $to])
          ->where('vendedor_pagado_id',$user_id)
          ->whereNull('deleted_at')
          ->where('estatus','pagado')
          ->where(function ($q) {
            $q->whereNull('vendedor_liquida_fecha')
              ->whereNull('vendedor_anticipa_fecha');
          });

          /*********************************************************************************/

        $data = [
          'list_pagado_del_dia_NV' => $array_reservaciones_pagadas/*->orderBy('fecha_reservacion', 'ASC')->get()*/,
          'list_pagado_del_dia' => $result_pagado_del_dia_PL->orderBy('fecha_reservacion', 'ASC')->get(),
          'sum_pagado_del_dia_NV' => $array_reservaciones_pagadas->sum('costo_boleto'),
          'sum_pagado_del_dia' => $result_pagado_del_dia->sum('costo_boleto'),

          'list_liquidado_del_dia' => $array_reservaciones_liquidadas/*$result_liquidado_del_dia->orderBy('fecha_reservacion', 'ASC')->get()*/,
          'sum_liquidado_del_dia' => $array_reservaciones_liquidadas->sum('resta')/*$result_liquidado_del_dia->sum('resta')*/,

          'list_anticipo_del_dia' => $result_anticipo_del_dia->orderBy($tabla_r_anticipado, 'ASC')->get(),
          'sum_anticipo_del_dia' => $result_anticipo_del_dia->sum('anticipo'),

          'list_pagado_de_otros_dias_NV' => $array_reservaciones_pagadas_de_otros_dias,
          'list_pagado_de_otros_dias' => $result_pagado_de_otros_dias_PL->orderBy('fecha_reservacion', 'ASC')->get(),
          'list_pagado_de_otros_dias_v2' => $result_pagado_de_otros_dias_v2->orderBy('created_at','ASC')->get(),

          'sum_pagado_de_otros_dias_NV' => $array_reservaciones_pagadas_de_otros_dias->sum('costo_boleto'),
          'sum_pagado_de_otros_dias_v2' => $result_pagado_de_otros_dias_v2->sum('costo_boleto'),

          'list_liquidado_de_otros_dias_NV' => $array_reservaciones_liquidadas_de_otros_dias,
          'list_liquidado_de_otros_dias' => $result_liquidado_de_otros_dias->orderBy($tabla_r_liquidado, 'ASC')->get(),
          'list_liquidado_de_otros_dias_v2' => $result_liquidado_de_otros_dias_v2->orderBy('created_at', 'ASC')->get(),

          'sum_liquidado_de_otros_dias_NV' => $array_reservaciones_liquidadas_de_otros_dias->sum('resta'),
          'sum_liquidado_de_otros_dias' => $result_liquidado_de_otros_dias->sum('resta'),
          'sum_liquidado_de_otros_dias_v2' => $result_liquidado_de_otros_dias_v2->sum('resta'),

          'list_anticipo_de_otros_dias' => $result_anticipo_de_otros_dias->orderBy($tabla_r_anticipado, 'ASC')->get(),
          'sum_anticipo_de_otros_dias' => $result_anticipo_de_otros_dias->sum('anticipo'),
        ];
          foreach ($data['list_liquidado_de_otros_dias_v2'] as $lq) {
            $lq->corrida_nombre = Corrida::find($lq->corrida_id)->nombre;
            $lq->vendedor_liquida_id = User::find($lq->vendedor_liquida_id)->name;
          }
          //return dd($data['list_liquidado_del_dia']);
          /****************** DE ESTE DÍAS *****************************************/
          $result = [];
          $resultL = [];
          $resultOD = [];
          $resultODL = [];

          foreach ($data['list_pagado_del_dia'] as $element) {
            $elemento = Carbon::parse($element->fecha_reservacion)->format('d/m/Y');
            $result[$element->fecha_reservacion][] = $elemento;
          }
          foreach ($data['list_liquidado_del_dia'] as $element) {
            $elementoL = Carbon::parse($element->fecha_reservacion)->format('d/m/Y');
            $resultL[$element->fecha_reservacion][] = $elementoL;
            $element->corrida_nombre = Corrida::find($element->corrida_id)->nombre;
            $element->vendedor_liquida_id = User::find($element->vendedor_liquida_id)->name;
          }
          foreach ($data['list_pagado_de_otros_dias'] as $element) {
            $elementOD = Carbon::parse($element->fecha_reservacion)->format('d/m/Y');
            $resultOD[$element->fecha_reservacion][] = $elementOD;
          }
          foreach ($data['list_liquidado_de_otros_dias'] as $element) {
            $elementODL = Carbon::parse($element->fecha_reservacion)->format('d/m/Y');
            $resultODL[$element->fecha_reservacion][] = $elementODL;
          }
          //return dd($resultOD);
          $count_result = count($result);
          $count_resultL = count($resultL);
          $count_resulOD = count($resultOD);
          $count_resulODL = count($resultODL);

          $contador = $count_result;
          $contadorL = $count_resultL;
          $contadorOD = $count_resulOD;
          $contadorODL = $count_resulODL;

          $mykey = [];
          $mykeyL = [];
          $mykeyOD = [];
          $mykeyODL = [];

          foreach($result as $key => $value){
            $mykey[$count_result --]  = $key;
            $var = Carbon::parse($key)->format('d/m/Y');
          }

          foreach($resultL as $key => $value){
            $mykeyL[$count_resultL --]  = $key;
            $varL = Carbon::parse($key)->format('d/m/Y');
          }

          foreach($resultOD as $key => $value){
            $mykeyOD[$count_resulOD --]  = $key;
            $varOD = Carbon::parse($key)->format('d/m/Y');
          }

          foreach($resultODL as $key => $value){
            $mykeyODL[$count_resulODL --]  = $key;
            $varODL = Carbon::parse($key)->format('d/m/Y');
          }

          $fechas_pagado = [];
          $fechas_liquidado = [];
          $fechas_pagado_otros_dias = [];
          $fechas_liquidado_otros_dias = [];

          for ($i=1; $i < ($contador+1); $i++) {
            $reservaciones_pagadas = Reservacione::whereDate('fecha_reservacion',  $mykey[$i])
            ->whereNull('deleted_at')
            ->where('estatus','pagado')
            ->where('vendedor_pagado_id', $user_id)
            ->where(function ($qb) {
              $qb->whereNull('vendedor_liquida_fecha')
                ->whereNull('vendedor_anticipa_fecha');
              })
              ->whereBetween('vendedor_pagado_fecha', [$from, $to])
              ->whereBetween('created_at', [$from, $to]);

              $fecha_llave = Carbon::parse($mykey[$i])->format('d/m/Y');
              $fechas_pagado[] = [
                'fecha' => $fecha_llave,
                'reservaciones' => $reservaciones_pagadas->get(),
                'sum' => $reservaciones_pagadas->sum('costo_boleto')
              ];
         }

          for ($i=1; $i < ($contadorL+1); $i++) {
            $reservaciones_liquidadas = Reservacione::whereDate('fecha_reservacion',  $mykeyL[$i])
            ->whereBetween('vendedor_liquida_fecha', [$from, $to])
            ->where('vendedor_liquida_id',$user_id)
            ->whereNull('vendedor_pagado_fecha')
            ->whereNull('deleted_at')
            ->where('estatus','pagado');
              //->whereBetween('created_at', [$from, $to]);

              $fecha_llaveL = Carbon::parse($mykeyL[$i])->format('d/m/Y');
              $fechas_liquidado[] = [
                'fecha' => $fecha_llaveL,
                'reservaciones' => $reservaciones_liquidadas->get(),
                'sum' => $reservaciones_liquidadas->sum('costo_boleto')
              ];
          }
          //return dd($mykeyOD);
          for ($i=1; $i < ($contadorOD+1); $i++) {
            $reservaciones_pagadas_OD = Reservacione::whereDate('fecha_reservacion', $mykeyOD[$i])
            ->whereNull('deleted_at')
            ->where('estatus','pagado')
            ->where('vendedor_pagado_id', $user_id)
            ->where(function ($qb) {
              $qb->whereNull('vendedor_liquida_fecha')
                ->whereNull('vendedor_anticipa_fecha');
              })
              ->whereBetween('created_at', [$from, $to])
              ->whereBetween('vendedor_pagado_fecha', [$from, $to]);

              $fecha_llaveOD = Carbon::parse($mykeyOD[$i])->format('d/m/Y');
              $fechas_pagado_otros_dias[] = [
                'fecha' => $fecha_llaveOD,
                'reservaciones' => $reservaciones_pagadas_OD->get(),
                'sum' => $reservaciones_pagadas_OD->sum('costo_boleto')
              ];
          }
          for ($i=1; $i < ($contadorODL+1); $i++) {
            $reservaciones_liquidadas_de_otros_dias = Reservacione::whereDate('fecha_reservacion',  $mykeyODL[$i])
            ->whereNull('deleted_at')
            ->where('estatus','pagado')
            ->where('vendedor_liquida_id', $user_id)
            ->where(function ($qb) {
              $qb->whereNull('vendedor_pagado_fecha')
                ->whereNull('vendedor_anticipa_fecha');
              })
              ->whereBetween('vendedor_liquida_fecha', [$from, $to])
              ->whereBetween('created_at', [$from, $to]);

              $fecha_llaveODL = Carbon::parse($mykeyODL[$i])->format('d/m/Y');
              $fechas_liquidado_otros_dias[] = [
                'fecha' => $fecha_llaveODL,
                'reservaciones' => $reservaciones_liquidadas_de_otros_dias->get(),
                'sum' => $reservaciones_liquidadas_de_otros_dias->sum('costo_boleto')
              ];
          }
          //return dd($fechas_liquidado_otros_dias);
          /************************************************************************/
          return view('admin.corte_de_cajas.index', compact(
            'users',
            'from',
            'to',
            'user_id',
            'data',
            'fechas_pagado',
            'fechas_anticipado',
            'fechas_liquidado',
            'fechas_pagado_otros_dias',
            'fechas_liquidado_otros_dias'
          ));
    }

    private function view_paqueteria($from, $to, $user_id, $users) {
      $result = Envio::whereBetween('fecha_ingresa_dinero', [$from, $to])
      ->where('pagado', true)
      ->where('estatus', '!=','cancelado');

      if($user_id != ''){
        $result->where('vendedor_id_dinero', $user_id);
      }
      $data = [
        'list' => $result->get(),
        'sum' => $result->sum('costo'),
      ];
      return view('admin.corte_de_cajas.index', compact('users', 'from', 'to','user_id', 'data'));
    }

}
