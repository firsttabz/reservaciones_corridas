<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use PayPal\Api\Payment;

use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use Auth;
use App\PaypalPayment;
use App\Corrida;
use App\Reservacione;

use Request as Req;

class RefundController extends Controller
{
  private $apiContext;
  public function __construct()
  {

      /** PayPal api context **/
      $paypal_conf = \Config::get('paypal');
      $this->apiContext = new ApiContext(new OAuthTokenCredential(
          $paypal_conf['client_id'],
          $paypal_conf['secret'])
      );
      $this->apiContext->setConfig($paypal_conf['settings']);

  }

    //
    public function index(Request $request){
      $paypal_reservacion = [];
      if($request->clave){
        $paypal_reservacion = Reservacione::where('clave_global', 'like', $request->clave.'%')
        //->where('estatus', 'pagado')
        ->where('is_plt_reservacion', '1')
        ->first();
        $suma_boletos = (int) Reservacione::where('clave_global','=',$request->clave)
        ->where('estatus','pagado')
        ->where('is_plt_reservacion', '1')
        ->sum('costo_boleto');
      }

      return view('admin.paypal.paypal_search', compact('paypal_reservacion', 'suma_boletos', 'reservaciones'));
    }

    public function executePayPalRefund($clave_global){
      $payment = PaypalPayment::whereNotNull('sales_id')
                 ->where('state', 'approved')
                 ->where('clave_reservacion_global', $clave_global)
                 ->first();
      if(!$payment) {
        return redirect()->back();
      }

      // You can retrieve the sale Id from Related Resources for each transactions.
      $saleId =  $payment->sales_id;

      try {
      // ### Retrieve the sale object
      // Pass the ID of the sale
      // transaction from your payment resource.
      $sale = Sale::get($saleId, $this->apiContext);

      } catch (Exception $ex) {
          // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
          //ResultPrinter::printError("Look Up A Sale", "Sale", $sale->getId(), null, $ex);
          echo "Look Up A Sale", "Sale", $sale->getId(), null, $ex;
          exit(1);
      }
      // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
       //ResultPrinter::printResult("Look Up A Sale", "Sale", $sale->getId(), null, $sale);
       //echo "Look Up A Sale", "Sale", $sale->getId(), null, $sale;
      //return $sale;

      $saleId = $sale->getId();

      // ### Refund amount
      // Includes both the refunded amount (to Payer)
      // and refunded fee (to Payee). Use the $amt->details
      // field to mention fees refund details.
      $suma_boletos = (int) Reservacione::where('clave_global','=',$clave_global)->where('estatus','pagado')->sum('costo_boleto');
      $amt = new Amount();
      $amt->setCurrency('MXN')
          ->setTotal($suma_boletos);

          // ### Refund object
      $refundRequest = new RefundRequest();
      $refundRequest->setAmount($amt);

      // ###Sale
      // A sale transaction.
      // Create a Sale object with the
      // given sale transaction id.
      $sale = new Sale();
      $sale->setId($saleId);

      try {
          // Create a new apiContext object so we send a new
          // PayPal-Request-Id (idempotency) header for this resource
          $paypal_conf = \Config::get('paypal');
          $apiContext = new ApiContext(new OAuthTokenCredential(
              $paypal_conf['client_id'],
              $paypal_conf['secret'])
          );
          // Refund the sale
          // (See bootstrap.php for more on `ApiContext`)
          $refundedSale = $sale->refundSale($refundRequest, $apiContext);
          Reservacione::where('clave_global','=',$clave_global)->where('estatus','pagado')->update([
            'vendedor_cancela_id' => Auth::id(),
            'vendedor_cancela_fecha' => \Carbon\Carbon::now(),
            'estatus' => 'cancelado'
          ]);
      } catch (Exception $ex) {
          // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
          //ResultPrinter::printError("Refund Sale", "Sale", null, $refundRequest, $ex);
          return 'error';
          exit(1);
      }

      // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
      //ResultPrinter::printResult("Refund Sale", "Sale", $refundedSale->getId(), $refundRequest, $refundedSale);
      //echo "Refund Sale", "Sale", $refundedSale->getId(), $refundRequest, $refundedSale;
      //return $refundedSale;
        return redirect()->back();

    }
}
