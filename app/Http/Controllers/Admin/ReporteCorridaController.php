<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Reservacione;
use App\Envio;
use App\Corrida;
use App\Asiento;
use App\User;
use App\Sucursale;
use Gate;
class ReporteCorridaController extends Controller
{
    public function index(Request $request){
      if(! Gate::allows('reporte_corrida_access')) {
          return abort(401);
      }
        $corridas = Corrida::where('activo',true)->get();
        foreach($corridas as $c) {
            $c->nombre = $c->nombre.' '.$c->hora_salida.' ';
            $c->nombre .= ($c->extraordinaria)? '(e)':'';
        }
        $corridas = $corridas->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        /*$corridas_e = Corrida::where('extraordinaria', true)
                      ->where('activo', false)
                      ->where(function($qb) {
                        $qb->orWhereDate('updated_at', Carbon::now()->subDays(3))
                        ->orWhereDate('created_at', Carbon::now()->subDays(6));
                      })
                      ->get();
        foreach ($corridas_e as $c) {
          $corridas[] =  $c;
        }*/
        $reservaciones = [];
        $paqueteria = [];
        $asientos_estructura = [];
        if($request->all()){
              //validando el request
              $request->validate([
                'from' => 'required|date_format:d/m/Y',
                'corrida_id' => 'required',
            ]);
            //creando tiempo en base al formatgo dado para la cosulta
            $from    = Carbon::createFromFormat('d/m/Y',$request->from)->toDateString();//->subDay();
            $corrida = Corrida::where('id',$request->corrida_id)->first();
            /************ OBTENIENDO EL TOTAL DE ASIENTOS DE LA CORRIDA ******************/
            $total_de_asientos = Corrida::obtieneLimiteDeAsientos(Carbon::parse($from) , $corrida);
            /****************************************************************************/
            $corrida->asientos = Asiento::where('corrida_id',$request->corrida_id)->limit($total_de_asientos)->orderBy('numero','ASC')->get();
            foreach ($corrida->asientos as $a) {
              $a->reservacion = Reservacione::whereDate('fecha_reservacion', $from)
              ->whereNull('deleted_at')
              ->where('estatus','!=','cancelado')
              ->where('corrida_id', $request->corrida_id)
              ->where('asiento_id', $a->id)->first();
            }
            //$asientos_estructura = [];
            foreach ($corrida->asientos as $a) {
              $asientos_estructura[] = [
              'asiento' => $a->numero,
              'clave' => ($a->reservacion == null)? '** vacio **': $a->reservacion->clave,
              'cliente' => ($a->reservacion == null)? '** vacio **': $a->reservacion->cliente,
              'phone' => ($a->reservacion == null)? '** vacio **': $a->reservacion->phone,
              'corrida' => $corrida->nombre,
              'fecha_reservacion' => ($a->reservacion == null)? '** vacio **' : Carbon::parse($a->reservacion->fecha_reservacion)->format('d/m/Y'),
              'costo_boleto' => ($a->reservacion == null)? '** vacio **' : $a->reservacion->costo_boleto,
              'descuento' => ($a->reservacion == null)? '** vacio **' : $a->reservacion->descuento,
              'redondo' => ($a->reservacion == null)? '** vacio **' : $a->reservacion->redondo,
              'estatus' => ($a->reservacion == null)? '** vacio **' : $a->reservacion->estatus,
              'anticipo' => ($a->reservacion == null)? '** vacio **' : $a->reservacion->anticipo,
              'resta' => ($a->reservacion == null)? '** vacio **' : $a->reservacion->resta,
              'sucursal' => ($a->reservacion == null)? '** vacio **' : Sucursale::find($a->reservacion->sucursal_id)->nombre,
              'vendedor_pagado' => ($a->reservacion == null)? '** vacio **' : (User::find($a->reservacion->vendedor_pagado_id)['name']),
              'fecha_pagado' => ($a->reservacion == null)? '** vacio **' : User::find($a->reservacion->vendedor_pagado_fecha)['name'],
              'vendedor_aparto' => ($a->reservacion == null)? '** vacio **' : User::find($a->reservacion->vendedor_anticipa_id)['name'],
              'vendedor_liquido' => ($a->reservacion == null)? '** vacio **' : User::find($a->reservacion->vendedor_liquida_id)['name'],
            ];
            }
            //return dd($asientos_estructura);
            //creando consulta para paqueteria
            $paqueteria = Envio::whereDate('created_at', $from)
            ->where('estatus','!=','cancelado')
            ->where('id_corrida', $request->corrida_id)->get();

        }
        return view('admin.reporte_corrida.index', compact('corridas','asientos_estructura', 'paqueteria'));
    }
}
