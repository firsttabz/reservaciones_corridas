<?php

namespace App\Http\Controllers\Admin;

use App\Envio;
use App\Corrida;
use App\Mensaje;
use App\Configuracion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreEnviosRequest;
use Carbon\Carbon;
use Auth;

class EnviosController extends Controller
{
    //
    public function index(Request $request){
        /*if(! Gate::allows('buscar_envio_access')) {
            return abort(401);
        }*/
        $envios = [];
        if($request->clave){
            $envios = Envio::where('clave','like',$request->clave.'%')
            ->orWhere('remitente', 'like', '%'.$request->clave.'%')
            ->get();
            //dd ($envios);
        }
        foreach ($envios as $e) {
            $e->corrida = Corrida::findOrFail($e->id_corrida);
        }

        return view('admin.envios.buscar', compact('envios'));
    }

    public function envioCancelacion($clave){

        $envio = Envio::where('clave',$clave)->first();
        //dd($envio);
        $salida = Corrida::where('id',$envio->id_corrida)->first();

        $envio_carbon = Carbon::parse($envio->fecha_envio.' '.$salida->hora_salida);

        $now = Carbon::now();
        $diffMin = ($now->diffInMinutes($envio_carbon,false));

        $espera = Configuracion::first()->tiempo_espera_envio;
        //configuracion del tiempo de espera limite para que un cliente pueda hacer una cancelacion de un envio
        //dd($espera);

        if($diffMin >= $espera){
            Envio::where('clave','=',$clave)
            ->update([
                'estatus' => 'cancelado',
                'vendedor_id_cancela' => Auth::id(),
                'vendedor_cancela_fecha' => Carbon::now(),
            ]);
            return back();
        }else{
            return back()->withErrors([
                'clave' => 'El envio '.$clave.' ya no puede ser cancelado'
              ]);
        }

    }

    public function envioEntrega($clave){
        if (Auth::user()->sucursal_id == null) {
          return back()->withErrors([
            'sucursal_id' => 'No tienes sucursal asignada, no puedes hacer ventas'
          ]);
        }
        $envio = Envio::where('clave',$clave)->first();
        //dd($envio);
        $salida = Corrida::where('id',$envio->id_corrida)->first();

        if($envio->estatus != 'cancelado' and $envio->estatus == 'recibido'){
            Envio::where('clave','=',$clave)
            ->update([
                'estatus' => 'entregado',
                'vendedor_id_entrega' => Auth::id(),
                'pagado' => true,
                'vendedor_id_dinero' => $envio->pagado == false? Auth::id(): $envio->vendedor_id_dinero,
            ]);
            $envio->estatus = 'entregado';
            $envio->vendedor_id_entrega = Auth::id();
            $envio->vendedor_id_dinero = $envio->pagado == false? Auth::id(): $envio->vendedor_id_dinero;
            if($envio->pagado == false) {
              $envio->fecha_ingresa_dinero = Carbon::now();
              $envio->sucursal_id_dinero = Auth::user()->sucursal_id;
            }
            $envio->pagado = true;
            $envio->save();

            if(strlen($envio->telefono_destinatario) == 10) {
              Mensaje::create([
                'message' => 'Hola '.$envio->destina.' tu un envío con clave '.$envio->clave. ' ha sido entregado.'.' - Notificación del SISTEMA de '.env('EMPRESA_NOMBRE').', favor de no responder.',
                'phone' => $envio->telefono_destinatario
              ]);
            }
            return back();
        }else{
            return back()->withErrors([
                'clave' => 'El envio '.$clave.' ya no puede ser entregado'
            ]);
        }

    }
    public function create(){
        if (! Gate::allows('envio_access')) {
            return abort(401);
        }
        //$vendedors = \App\User::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $vendedor = Auth::user();
        // $corridas = \App\Corrida::get()->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $corridas = Corrida::where('activo',1)->get();
        foreach ($corridas as $value) {
          $value->nombre .= ' '.$value->hora_salida;
          $value->nombre .= $value->extraordinaria == 1? '(e)':'';
        }
        $corridas = $corridas->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        return view('admin.envios.envio', compact('vendedor','corridas'));
    }

    public function store(StoreEnviosRequest $request){
        if (Auth::user()->sucursal_id == null) {
          return back()->withErrors([
            'sucursal_id' => 'No tienes sucursal asignada, no puedes hacer ventas'
          ]);
        }
        $request = $request->except('_token');
        $request['vendedor_id_recibe'] = Auth::id();
        $request['clave'] = strtoupper(str_random(10));
        $request['estatus'] = 'recibido';
        $request['vendedor_id_dinero'] = ($request['pagado'] == 0)? null: Auth::id();
        //return dd($request); exit;
        $envio = Envio::create($request);
        //return back();
        $estatus = ($request['pagado'] == true)? 'pagado':'por pagar';
        if($estatus == 'pagado') {
          $envio->fecha_ingresa_dinero = Carbon::now();
          $envio->sucursal_id_dinero = Auth::user()->sucursal_id;
          $envio->save();
        }
        if(strlen($request['telefono_destinatario']) == 10) {
          Mensaje::create([
            'message' => 'Hola '.$this->ajustaNombreCliente($request['destinatario']).' tienes un envío con código de seguimiento '.$request['clave'].' con estatus '. $estatus.', tu envío sale a destino el día de hoy. - mensaje automático de '.env('EMPRESA_NOMBRE'),
            'phone' => $request['telefono_destinatario']
          ]);
        }
        return redirect(url('admin/envios').'?clave='.$envio->clave);
    }

    private function ajustaNombreCliente($cadena){

      //Codificamos la cadena en formato utf8 en caso de que nos de errores
      //$cadena = utf8_encode($cadena);

      //Ahora reemplazamos las letras
      $cadena = str_replace(
          array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
          array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
          $cadena
      );

      $cadena = str_replace(
          array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
          array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
          $cadena );

      $cadena = str_replace(
          array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
          array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
          $cadena );

      $cadena = str_replace(
          array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
          array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
          $cadena );

      $cadena = str_replace(
          array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
          array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
          $cadena );

      $cadena = str_replace(
          array('ñ', 'Ñ', 'ç', 'Ç'),
          array('n', 'N', 'c', 'C'),
          $cadena
      );
      $cadena = title_case($cadena);
      $nombre_arr = explode(" ", $cadena);

      if(count($nombre_arr) > 1){
        $cadena = $nombre_arr[0].' '.substr($nombre_arr[1],0,1);
      }
      return $cadena;
    }

}
