<?php

namespace App\Http\Controllers\Admin;

use App\Autobus;
use App\Asiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAutobusesRequest;
use App\Http\Requests\Admin\UpdateAutobusesRequest;

class AutobusesController extends Controller
{
    /**
     * Display a listing of Autobus.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('autobus_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('autobus_delete')) {
                return abort(401);
            }
            $autobuses = Autobus::onlyTrashed()->get();
        } else {
            $autobuses = Autobus::all();
        }

        return view('admin.autobuses.index', compact('autobuses'));
    }

    /**
     * Show the form for creating new Autobus.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('autobus_create')) {
            return abort(401);
        }
        return view('admin.autobuses.create');
    }

    /**
     * Store a newly created Autobus in storage.
     *
     * @param  \App\Http\Requests\StoreAutobusesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAutobusesRequest $request)
    {
        if (! Gate::allows('autobus_create')) {
            return abort(401);
        }
        $autobus = Autobus::create($request->all());

        return redirect()->route('admin.autobuses.index');
    }


    /**
     * Show the form for editing Autobus.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('autobus_edit')) {
            return abort(401);
        }
        $autobus = Autobus::findOrFail($id);

        return view('admin.autobuses.edit', compact('autobus'));
    }

    /**
     * Update Autobus in storage.
     *
     * @param  \App\Http\Requests\UpdateAutobusesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAutobusesRequest $request, $id)
    {
        if (! Gate::allows('autobus_edit')) {
            return abort(401);
        }
        $autobus = Autobus::findOrFail($id);
        $autobus->update($request->all());



        return redirect()->route('admin.autobuses.index');
    }


    /**
     * Display Autobus.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('autobus_view')) {
            return abort(401);
        }
        //$asientos = \App\Asiento::where('autobus_id', $id)->get();$corridas = \App\Corrida::where('autobus_id', $id)->get();

        $autobus = Autobus::findOrFail($id);

        return view('admin.autobuses.show', compact('autobus', 'corridas'));
    }


    /**
     * Remove Autobus from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('autobus_delete')) {
            return abort(401);
        }
        $autobus = Autobus::findOrFail($id);
        $autobus->delete();

        return redirect()->route('admin.autobuses.index');
    }

    /**
     * Delete all selected Autobus at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('autobus_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Autobus::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Autobus from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('autobus_delete')) {
            return abort(401);
        }
        $autobus = Autobus::onlyTrashed()->findOrFail($id);
        $autobus->restore();

        return redirect()->route('admin.autobuses.index');
    }

    /**
     * Permanently delete Autobus from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('autobus_delete')) {
            return abort(401);
        }
        $autobus = Autobus::onlyTrashed()->findOrFail($id);
        $autobus->forceDelete();

        return redirect()->route('admin.autobuses.index');
    }
}
