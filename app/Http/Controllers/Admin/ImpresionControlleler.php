<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImprimirBoletoRequest;
use App\ticket;
use App\Envio;
use App\User;
use App\Reservacione;
use Carbon\Carbon;
use Auth;

class ImpresionControlleler extends Controller
{
    public function imprimirBoleto($clave){
      $r = Reservacione::where('clave',$clave)
      ->join('corridas','reservaciones.corrida_id','=','corridas.id')
      ->join('asientos','reservaciones.asiento_id','=','asientos.id')
      ->first();
      if(!$r){
        die("no se encontró la clave");
      }
      $data=[
        'nombre_cliente'=> $r->cliente,
        'nombre_corrida'=> $r->nombre,
        'hora_corrida'=> $r->hora_salida,
        'id_asiento'=> $r->numero,
        'codigo_reservacion'=> $r->clave,
        'status_reservacion'=> $r->estatus,
        'fecha_reservacion'=> Carbon::createFromFormat('Y-m-d',$r->fecha_reservacion),
        'hora_reservacion'=> $r->fecha_reservacion,
        'costo_boleto'=> $r->costo_boleto,
        'fecha_boleto'=> date('d-m-Y'),
        'nombre_empresa'=> env('EMPRESA_NOMBRE', 'Transportes XYZ'),
        'ubicacion_empresa'=> env('EMPRESA_UBICACION', 'Calle prol. de la noria 123'),
        'telefono_empresa'=> env('EMPRESA_TELEFONO', 9513285920),
        'pagina_empresa'=> env('EMPRESA_SITIO_WEB', 'jasbit.com'),
        'pagina_empresa'=> env('EMPRESA_SITIO_WEB', 'jasbit.com'),
        'vendedor' => User::find($r->vendedor_id)->name
      ];
      return ticket::generarTickets($data);
    }

    public function imprimirEnvio($clave){
      $e = Envio::where('clave',$clave)->join('corridas','envios.id_corrida','=','corridas.id')->first();
      $x = Envio::where('clave',$clave)->first();
      //->join('asientos','reservaciones.asiento_id','=','asientos.id')
      //modificar
      if(!$e){
        die("no se encontró la clave");
      }
      $data=[
        'remitente' => $e->remitente,
        'destinatario'=> $e->destinatario,
        'costo'=> $e->costo,
        'estatus'=> $e->estatus,
        'clave'=> $e->clave,
        'descripcion'=> $e->descripcion,
        'categoria'=> $e->categoria,
        'id_corrida'=> $e->nombre,
        'pagado'=> $e->pagado,
        'fecha_envio' => Carbon::parse($x->created_at)->format('d-m-Y'),
        'fecha_boleto' => Carbon::parse($x->created_at)->format('d-m-Y'),
        'vendedor' => User::find($e->vendedor_id_recibe)->name,
        'nombre_empresa' => 'Linea Dorada',
        'pagina_empresa' => 'Jasbit.com'
    ];
      return Envio::generarTicketEnvio($data);
    }
}
