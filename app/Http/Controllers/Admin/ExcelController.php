<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reservacione;
use App\Corrida;
use App\Asiento;
use App\Envio;
use App\Sucursale;
use App\User;
use Carbon\Carbon;
use Excel;
class ExcelController extends Controller
{
  public function reporteCorrida(Request $request){
    if($request->all()){
          $request->validate([
            'from' => 'required|date_format:d/m/Y',
            'corrida_id' => 'required',
        ]);
        //creando tiempo en base al formatgo dado para la cosulta
        $from    = Carbon::createFromFormat('d/m/Y',$request->from)->toDateString();//->subDay();
        $corrida = Corrida::where('id',$request->corrida_id)->first();
        //return dd($corrida);
        /************ OBTENIENDO EL TOTAL DE ASIENTOS DE LA CORRIDA ******************/
        $total_de_asientos = Corrida::obtieneLimiteDeAsientos(Carbon::parse($from) , $corrida);
        /****************************************************************************/
        $corrida->asientos = Asiento::where('corrida_id',$request->corrida_id)->limit($total_de_asientos)->orderBy('numero','ASC')->get();
        foreach ($corrida->asientos as $a) {
          $a->reservacion = Reservacione::whereDate('fecha_reservacion', $from)
          ->whereNull('deleted_at')
          ->where('estatus','!=','cancelado')
          ->where('corrida_id', $request->corrida_id)
          ->where('asiento_id', $a->id)->first();
        }
        $asientos_estructura = [];
        foreach ($corrida->asientos as $a) {
          $asientos_estructura[] = [
          'asiento' => $a->numero,
          'clave' => ($a->reservacion == null)? 'Asiento vacio ': $a->reservacion->clave,
          'cliente' => ($a->reservacion == null)? '': $a->reservacion->cliente,
          'phone' => ($a->reservacion == null)? '': $a->reservacion->phone,
          'corrida' => $corrida->nombre,
          'fecha_reservacion' => ($a->reservacion == null)? '' : Carbon::parse($a->reservacion->fecha_reservacion)->format('d/m/Y'),
          'costo_boleto' => ($a->reservacion == null)? '' : $a->reservacion->costo_boleto,
          'descuento' => ($a->reservacion == null)? '' : $a->reservacion->descuento,
          'redondo' => ($a->reservacion == null)? '' : $a->reservacion->redondo,
          'estatus' => ($a->reservacion == null)? '' : $a->reservacion->estatus,
          'anticipo' => ($a->reservacion == null)? '' : $a->reservacion->anticipo,
          'resta' => ($a->reservacion == null)? '' : $a->reservacion->resta,
          'sucursal' => ($a->reservacion == null)? '' : Sucursale::find($a->reservacion->sucursal_id)->nombre,
          'vendedor_pagado' => ($a->reservacion == null)? '' : (($a->reservacion->vendedor_pagado_id == null)?(User::find($a->reservacion->vendedor_liquida_id)['name']):(User::find($a->reservacion->vendedor_pagado_id)['name'])),
          //'vendedor_aparto' => ($a->reservacion == null)? '' : User::find($a->reservacion->vendedor_anticipa_id)['name'],
          //'vendedor_liquido' => ($a->reservacion == null)? '' : User::find($a->reservacion->vendedor_liquida_id)['name'],
          //'fecha_pagado' => ($a->reservacion == null)? '' : ((User::find($a->reservacion->vendedor_pagado_fecha)['name'] == null)?(User::find($a->reservacion->vendedor_liquida_fecha)['name']):(User::find($a->reservacion->vendedor_pagado_fecha)['name'])),
          'fecha_pagado' => ($a->reservacion == null)? '' : (($a->reservacion->vendedor_pagado_fecha == null)?($a->reservacion->vendedor_liquida_fecha):($a->reservacion->vendedor_pagado_fecha)),
        ];
        }
              $nombre_archivo = 'Corrida  '.$corrida->nombre.' '.$corrida->hora_salida.' '.$from;
              $nombre_archivo_v2  = str_replace("/","-",$nombre_archivo);
              $nombre_archivo_v3  = str_replace("\\","-",$nombre_archivo_v2);
              //$nombre_archivo_v4  = str_replace("_",":",$nombre_archivo);
              //return dd($nombre_archivo);
              foreach ($asientos_estructura as $r) {
                $r['cliente'] = $this->quitarAcentos($r['cliente']);
              }
              return Excel::download(new \App\Exports\Reservaciones($asientos_estructura), $nombre_archivo_v3.'.xlsx');
    }
  }

  public function reportePaqueteria(Request $request){
    if($request->all()){
          $request->validate([
            'from' => 'required|date_format:d/m/Y',
            'corrida_id' => 'required',
        ]);
        $from    = Carbon::createFromFormat('d/m/Y',$request->from)->toDateString();//->subDay();

        //creando consulta
        $paqueteria = Envio::whereDate('created_at', $from)
        ->select([
          'clave',
          'remitente',
          'destinatario',
          'estatus',
          'pagado',
          'categoria',
          'descripcion',
        ])
        ->where('estatus','!=','cancelado')
        ->limit(5000)
        ->where('id_corrida', $request->corrida_id);

        $paqueteria = $paqueteria->get();
        $c = Corrida::findOrFail($request->corrida_id);

        foreach($paqueteria as $r){
            $r->corrida = $c->nombre.' '.$c->hora_salida;
            $r->pagado = $r->pagado? 'Sí' : 'No';
        }

        $tipo_vehiculo = ($c->camioneta)? 'Sprinter/Crafter':'';
        $extraordinaria = ($c->extraordinaria)? 'extraordinaria':'';
        $nombre_archivo = 'Paqueteria '.$c->nombre.' '.$c->hora_salida.' '.$request->fecha_inicio.' '.$tipo_vehiculo.' '.$extraordinaria;

         return Excel::download(new \App\Exports\Paqueteria($paqueteria), $nombre_archivo.'.xlsx');

    }
  }

  private function quitarAcentos($cadena){

      //Codificamos la cadena en formato utf8 en caso de que nos de errores
      //$cadena = utf8_encode($cadena);

      //Ahora reemplazamos las letras
      $cadena = str_replace(
          array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
          array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
          $cadena
      );

      $cadena = str_replace(
          array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
          array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
          $cadena );

      $cadena = str_replace(
          array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
          array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
          $cadena );

      $cadena = str_replace(
          array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
          array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
          $cadena );

      $cadena = str_replace(
          array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
          array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
          $cadena );

      $cadena = str_replace(
          array('ñ', 'Ñ', 'ç', 'Ç'),
          array('n', 'N', 'c', 'C'),
          $cadena
      );

      return $cadena;
  }

}
