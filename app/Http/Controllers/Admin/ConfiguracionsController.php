<?php

namespace App\Http\Controllers\Admin;

use App\Configuracion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreConfiguracionsRequest;
use App\Http\Requests\Admin\UpdateConfiguracionsRequest;

class ConfiguracionsController extends Controller
{
    /**
     * Display a listing of Configuracion.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('configuracion_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('configuracion_delete')) {
                return abort(401);
            }
            $configuracions = Configuracion::onlyTrashed()->get();
        } else {
            $configuracions = Configuracion::all();
        }

        return view('admin.configuracions.index', compact('configuracions'));
    }

    /**
     * Show the form for creating new Configuracion.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('configuracion_create')) {
            return abort(401);
        }
        return view('admin.configuracions.create');
    }

    /**
     * Store a newly created Configuracion in storage.
     *
     * @param  \App\Http\Requests\StoreConfiguracionsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreConfiguracionsRequest $request)
    {
        if (! Gate::allows('configuracion_create')) {
            return abort(401);
        }
        $configuracion = Configuracion::create($request->all());



        return redirect()->route('admin.configuracions.index');
    }


    /**
     * Show the form for editing Configuracion.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('configuracion_edit')) {
            return abort(401);
        }
        $configuracion = Configuracion::findOrFail($id);

        return view('admin.configuracions.edit', compact('configuracion'));
    }

    /**
     * Update Configuracion in storage.
     *
     * @param  \App\Http\Requests\UpdateConfiguracionsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateConfiguracionsRequest $request, $id)
    {
        if (! Gate::allows('configuracion_edit')) {
            return abort(401);
        }
        $configuracion = Configuracion::findOrFail($id);
        $configuracion->update($request->all());



        return redirect()->route('admin.configuracions.index');
    }


    /**
     * Display Configuracion.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('configuracion_view')) {
            return abort(401);
        }
        $configuracion = Configuracion::findOrFail($id);

        return view('admin.configuracions.show', compact('configuracion'));
    }


    /**
     * Remove Configuracion from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('configuracion_delete')) {
            return abort(401);
        }
        $configuracion = Configuracion::findOrFail($id);
        $configuracion->delete();

        return redirect()->route('admin.configuracions.index');
    }

    /**
     * Delete all selected Configuracion at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('configuracion_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Configuracion::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Configuracion from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('configuracion_delete')) {
            return abort(401);
        }
        $configuracion = Configuracion::onlyTrashed()->findOrFail($id);
        $configuracion->restore();

        return redirect()->route('admin.configuracions.index');
    }

    /**
     * Permanently delete Configuracion from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('configuracion_delete')) {
            return abort(401);
        }
        $configuracion = Configuracion::onlyTrashed()->findOrFail($id);
        $configuracion->forceDelete();

        return redirect()->route('admin.configuracions.index');
    }
}
