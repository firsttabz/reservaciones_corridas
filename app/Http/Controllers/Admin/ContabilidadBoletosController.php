<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Corrida;
use App\User;
use App\Envio;
use App\Autobus;
use App\Sucursale;
use App\Reservacione;
use Carbon\Carbon;

class ContabilidadBoletosController extends Controller
{
  public function index(Request $request) {
    if (! Gate::allows('contabilidad_de_boletos')) {
        return abort(401);
    }
    $corridas = Corrida::where('activo',1)->get()->pluck('nombre','id')->prepend(trans('quickadmin.qa_please_select'), '');
    $from = Carbon::now()->startOfDay()->toDateString();

    if($request->from){
      $from = Carbon::createFromFormat('d/m/Y',$request->from)->toDateString();
    }

    //checando si existe una consulta
    $reservaciones = [];
    $count_reservaciones = 0;
    $sumatorias = [];
    $totales = [];
    $totales_personas = [];
    $venta_estados =[];
    if($request->all()) {
      //SQL de reservaciones (eloquent)
      $reservaciones_sql = Reservacione::select([
        'reservaciones.costo_boleto','reservaciones.estatus', 'reservaciones.vendedor_pagado_id',
        'reservaciones.vendedor_liquida_id', 'reservaciones.vendedor_anticipa_id',
        'reservaciones.redondo','reservaciones.clave','reservaciones.cliente','reservaciones.phone',

      ])
      ->whereDate('fecha_reservacion',$from)
      ->where('corrida_id', $request->corrida_id)
      ->where('estatus','pagado');
      //obteniend las reservaciones
      $reservaciones = $reservaciones_sql->get();
      //contando la cantidad real de reservcaciones
      $count_reservaciones = count($reservaciones);
      //contando redondos
      $count_redondos = (int)(collect($reservaciones)->where('redondo',1)->count()/2 + 0.5);
      //obteniendo la variedad de los costos
      $variedad_de_costos = [];

      foreach ($reservaciones as $r) {
        //recuperando vendedor a quien pagaron
        $user = (($r->vendedor_pagado_id == null)?(User::find($r->vendedor_liquida_id)):(User::find($r->vendedor_pagado_id)));
        $r->vendedor_pagado = $user;

        $r->sucursal_pagado = Sucursale::find($user->sucursal_id);
        /*Al hacer la migración editar esto*/
        $r->venta_estado = $r->sucursal_pagado->estado == 0 ? 'mexico':'oaxaca';
        /***********************************/
        if(in_array($r->costo_boleto, $variedad_de_costos) == false) {
          $variedad_de_costos[] = $r->costo_boleto;
        }
      }
      //sumatorias para sencillos
      $total_sencillos = 0;
      $total_personas_sencillos = 0;
      foreach ($variedad_de_costos as $variedad) {
        $reservaciones_collection = collect($reservaciones);
        $numero_personas = $reservaciones_collection
        ->where('costo_boleto', $variedad)
        ->where('redondo',0)
        ->count();
        $total = (float) $variedad * $numero_personas;
        $sumatorias['sencillos'][$variedad] = [
          'numero_personas' => $numero_personas,
          'costo_boleto' => number_format($variedad,2),
          'total' => $total
        ];
        $total_sencillos += $total;
        $total_personas_sencillos += $numero_personas;
      }
      $totales['sencillos'] = $total_sencillos;
      $totales_personas['sencillos'] = $total_personas_sencillos;
      //////////////////////////////////////////////////////////////////
      //sumatorias para redondos-1
      $total_redondos_1 = 0;
      $total_personas_redondos_1 = 0;
      foreach ($variedad_de_costos as $variedad) {
        $reservaciones_collection = collect($reservaciones)->filter(function($item,$index) {
          if(strpos($item->clave,'-1') === false){
            return false;
          } else {
            return true;
          }
        });
        $numero_personas = $reservaciones_collection
        ->where('costo_boleto', $variedad)
        ->count();
        $total = ((float) $variedad * $numero_personas)*2;
        $sumatorias['redondos-1'][$variedad] = [
          'numero_personas' => $numero_personas,
          'costo_boleto' => number_format((float)$variedad*2,2),
          'total' => $total,
        ];
        $total_redondos_1 += $total;
        $total_personas_redondos_1 += $numero_personas;
      }
      $totales['redondos-1'] = $total_redondos_1;
      $totales_personas['redondos-1'] = $total_personas_redondos_1;
      ////////////////////////////////////////////////////////////////////////////7
      //sumatorias para redondos-2
      $total_redondos_2 = 0;
      $total_personas_redondos_2 = 0;
      foreach ($variedad_de_costos as $variedad) {
        $reservaciones_collection = collect($reservaciones)->filter(function($item,$index) {
          if(strpos($item->clave,'-2') === false){
            return false;
          } else {
            return true;
          }
        });
        $numero_personas = $reservaciones_collection
        ->where('costo_boleto', $variedad)
        ->count();
        $total = (float) $variedad * $numero_personas;
        $sumatorias['redondos-2'][$variedad] = [
          'numero_personas' => $numero_personas,
          'costo_boleto' => number_format($variedad,2),
          'total' => $total,
        ];
        $total_redondos_2 += $total;
        $total_personas_redondos_2 += $numero_personas;
      }
      $totales['redondos-2'] = $total_redondos_2;
      $totales_personas['redondos-2'] = $total_personas_redondos_2;
      ////////////////////////////////////////////////////////////////////////////7
      //Total de totales
      $totales['total'] = $totales['redondos-1']+$totales['redondos-2']+$totales['sencillos'];
      //Separando reservaciones por venta_estado
      $reservaciones_collection = collect($reservaciones);

      foreach ($reservaciones_collection->groupBy('venta_estado') as $key => $e) {
        $venta_estados[$key] = [
          'reservaciones' => $e,
          'total' => $e->sum('costo_boleto')
        ];

      }
      //dd(compact('from','corridas','reservaciones','count_reservaciones', 'count_redondos','sumatorias', 'totales', 'venta_estados'));
    }

    //return dd(compact('from','corridas','reservaciones','count_reservaciones', 'count_redondos','sumatorias', 'totales','totales_personas','venta_estados'));
    return view('admin.contabilidad_de_boletos.index', compact('from','corridas','reservaciones','count_reservaciones', 'count_redondos','sumatorias', 'totales','totales_personas','venta_estados'));
  }
}
