<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Envio;
use App\User;
use App\Reservacione;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
class ListaLiquidacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if (! Gate::allows('lista_liquidaciones_access')) {
          return abort(401);
      }

      $lista = [];
      if($request->all()) {
        $from = Carbon::createFromFormat('d/m/Y',$request->from)->startOfDay();
        if($request->tipo == 'paqueteria') {
          $lista = Envio::where('estatus', 'entregado')
          ->whereDate('fecha_ingresa_dinero', $from)
          ->get();
        } else {
          $lista = Reservacione::whereNull('deleted_at')
          ->where('estatus', "!=",'cancelado')
          ->whereNotNull('vendedor_liquida_id')
          ->whereDate('vendedor_liquida_fecha', $from)
          ->get();
          foreach ($lista as $it) {
            $it->vendedor_liquida = User::find($it->vendedor_liquida_id)->name;
          }
        }
      }
      return view('admin.boletos_liquidacion.index', compact('lista'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
