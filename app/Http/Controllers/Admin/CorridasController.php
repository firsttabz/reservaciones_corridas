<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Corrida;
use App\Asiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCorridasRequest;
use App\Http\Requests\Admin\UpdateCorridasRequest;
use Session;
class CorridasController extends Controller
{
    /**
     * Display a listing of Corrida.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('corrida_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('corrida_delete')) {
                return abort(401);
            }
            $corridas = Corrida::onlyTrashed()->get();
        } else {
            $corridas = Corrida::all();
        }

        return view('admin.corridas.index', compact('corridas'));
    }

    /**
     * Show the form for creating new Corrida.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('corrida_create')) {
            return abort(401);
        }

        $chofers = \App\Chofer::get()->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $autobuses = \App\Autobus::get()->pluck('placas', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $configuracion = Session::get('configuracion');
        $autobuses_autocomplete =\App\Autobus::all();
        return view('admin.corridas.create', compact('chofers', 'autobuses', 'configuracion','autobuses_autocomplete'));
    }

    /**
     * Store a newly created Corrida in storage.
     *
     * @param  \App\Http\Requests\StoreCorridasRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCorridasRequest $request)
    {
        if (! Gate::allows('corrida_create')) {
            return abort(401);
        }
        if ($request->extraordinaria == 1) {
          $request->validate([
            'fecha_extraordinaria' => 'required'
          ]);
          $request->merge([
            'fecha_extraordinaria' => Carbon::createFromFormat('d/m/Y', $request->fecha_extraordinaria)->startOfDay()
          ]);
        } else {
          $request->request->remove('fecha_extraordinaria');
        }

        $corrida = Corrida::create($request->all());
        for($i = 1; $i <= $request->total_asientos; $i++){
           Asiento::create(['numero' => $i,'corrida_id' => $corrida->id]);
         }

        return redirect()->route('admin.corridas.index');
    }


    /**
     * Show the form for editing Corrida.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('corrida_edit')) {
            return abort(401);
        }

        $chofers = \App\Chofer::get()->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $autobuses = \App\Autobus::get()->pluck('placas', 'id','numero_asientos')->prepend(trans('quickadmin.qa_please_select'), '');
        $configuracion = Session::get('configuracion');
        $autobuses_autocomplete =\App\Autobus::all();
        $corrida = Corrida::findOrFail($id);

        return view('admin.corridas.edit', compact('corrida', 'chofers', 'autobuses', 'autobuses_autocomplete', 'configuracion'));
    }

    /**
     * Update Corrida in storage.
     *
     * @param  \App\Http\Requests\UpdateCorridasRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) //UpdateCorridas
    {
      //return dd($request->all());exit;
        if (! Gate::allows('corrida_edit')) {
            return abort(401);
        }
        $old_corrida = Corrida::findOrFail($id);
        $old_corrida->asientos = Asiento::whereNull('deleted_at')->where('corrida_id', $id)->count('id');

        if($request->total_asientos > $old_corrida->asientos){
          for($i = $old_corrida->asientos+1; $i <= $request->total_asientos; $i++){
            Asiento::create(['numero' => $i,'corrida_id' => $id]);
          }
        }

        $corrida = Corrida::findOrFail($id);
        $corrida->update($request->all());



        return redirect()->route('admin.corridas.index');
    }


    /**
     * Display Corrida.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('corrida_view')) {
            return abort(401);
        }

        $chofers = \App\Chofer::get()->pluck('nombre', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $autobuses = \App\Autobus::get()->pluck('placas', 'id')->prepend(trans('quickadmin.qa_please_select'), '');$reservaciones = \App\Reservacione::where('corrida_id', $id)->get();

        $corrida = Corrida::findOrFail($id);

        return view('admin.corridas.show', compact('corrida', 'reservaciones'));
    }


    /**
     * Remove Corrida from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('corrida_delete')) {
            return abort(401);
        }
        $corrida = Corrida::findOrFail($id);
        $corrida->delete();

        return redirect()->route('admin.corridas.index');
    }

    /**
     * Delete all selected Corrida at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('corrida_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Corrida::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Corrida from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('corrida_delete')) {
            return abort(401);
        }
        $corrida = Corrida::onlyTrashed()->findOrFail($id);
        $corrida->restore();

        return redirect()->route('admin.corridas.index');
    }

    /**
     * Permanently delete Corrida from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('corrida_delete')) {
            return abort(401);
        }
        $corrida = Corrida::onlyTrashed()->findOrFail($id);
        $corrida->forceDelete();

        return redirect()->route('admin.corridas.index');
    }
}
