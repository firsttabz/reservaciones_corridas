<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Reservacione;
use App\Corrida;
use App\User;
use App\Envio;
use App\Asiento;
use Carbon\Carbon;
use Crypt;
use Auth;

class CorteCajaBasicoController extends Controller
{
  public function index(Request $request)
 {
     if (! Gate::allows('corte_de_caja_access')) {
         return abort(401);
     }
     $users = User::all();

     $users = $users->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
     $hay_quest = false;
     $reservaciones = [];
     $envios = [];
     $sumatorias = [
       'sum_pagado' => 0,
       'sum_liquidado' => 0,
       'sum_anticipado' => 0,
       'sum_total' => 0,
     ];
     $vendedor = null;
     //Verificando si existen ventas
     if($request->all()){
         $hay_quest = true; //True
         $request->user_id = Auth::user()->role_id != 1 ? Crypt::decrypt($request->user_id) : $request->user_id;
         $vendedor = User::find($request->user_id);
         //validando el request
         $request->validate([
             'from' => 'required|date_format:d/m/Y',
             // 'to' => 'required|date_format:d/m/Y',
             'user_id' => 'required',
             //'corrida_id' => 'required',
         ]);
         //creando tiempo en base al formatgo dado para la cosulta
         $from    = Carbon::createFromFormat('d/m/Y',$request->from)->startOfDay();//->toDateString();
         $to      = Carbon::createFromFormat('d/m/Y',$request->to)->endOfDay();//->toDateString();
         //return dd($to);
         //creando consulta
         // $reservaciones = Reservacione::whereBetween('created_at', [$from, $to])
         $reservaciones = Reservacione::whereBetween('created_at', [$from, $to])
         ->whereNull('deleted_at')
         ->where('vendedor_id',$request->user_id)
         ->where('estatus','=','pagado')
         ->limit(5000);

         $envios = Envio::whereBetween('fecha_ingresa_dinero', [$from, $to])
         ->where('vendedor_id_dinero', $request->user_id)
         ->where('pagado', true)
         // ->where('estatus', 'entregado')
         ->limit(5000)
         ->get();

         $sum_envios = Envio::whereBetween('fecha_ingresa_dinero', [$from, $to])
         ->where('vendedor_id_dinero', $request->user_id)
         //->where('pagado', true)
         //->where('estatus', 'entregado')
         ->limit(5000)
         ->sum('costo');

         if($request->corrida_id != null){
             $reservaciones = $reservaciones->where('corrida_id',$request->corrida_id);
         }

         if($request->user_id) {
           $reservaciones = $reservaciones->where('vendedor_id',$request->user_id);
         }

         $reservaciones = $reservaciones->get();

         foreach($reservaciones as $r){
             $c = Corrida::find($r->corrida_id);
             if(!$c){ $c = new Corrida(); $c->nombre = 'Corrida borrada';$c->hora_salida = "";}
             $r->corrida = $c->nombre.' '.$c->hora_salida;
             $r->asiento = Asiento::find($r->asiento_id)->numero;
             $r->fecha_reservacion =  Carbon::createFromFormat('Y-m-d',$r->fecha_reservacion)->format('d/m/Y');
         }

       ///////////////////////////////////////////////////////////////////////Borrar
       // sumatorias
         //Recibio dinero de boletos pagados PAGADO
         $sum_pagado = Reservacione::whereBetween('created_at', [$from, $to])
                   ->whereNull('deleted_at')
                   ->whereNull('vendedor_pagado_fecha')
                   ->where('estatus','pagado')
                   ->where('vendedor_pagado_id',$request->user_id)
                   ->sum('costo_boleto');
         //Recibio dinero de liquidaciones de boletos
         $sum_liquidado = Reservacione::whereBetween('created_at', [$from, $to])
                     ->whereNull('deleted_at')
                     ->whereNull('vendedor_liquida_fecha')
                     ->where('estatus','pagado')
                     ->where('vendedor_liquida_id',$request->user_id)
                     ->sum('resta');
         //Recibo dinero de boletos con anticipo
         $sum_anticipo = Reservacione::whereBetween('created_at', [$from, $to])
                     ->whereNull('deleted_at')
                     ->whereNull('vendedor_anticipa_fecha')
                     ->where('vendedor_anticipa_id',$request->user_id)
                     ->where(function($qb){
                       $qb->where('estatus','pagado')
                       ->orWhere('estatus','apartado');
                     })
                     ->sum('anticipo');

       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
       // sumatorias
         //Recibio dinero de boletos pagados PAGADO
         $sum_pagado += Reservacione::whereBetween('vendedor_pagado_fecha', [$from, $to])
                   ->whereNull('deleted_at')
                   ->where('estatus','pagado')
                   ->where('vendedor_pagado_id',$request->user_id)
                   ->sum('costo_boleto');
                   //return dd($sum_pagado);

         //Recibio dinero de liquidaciones de boletos
         $sum_liquidado += Reservacione::whereBetween('vendedor_liquida_fecha', [$from, $to])
                     ->whereNull('deleted_at')
                     ->where('estatus','pagado')
                     ->where('vendedor_liquida_id',$request->user_id)
                     ->sum('resta');
                  //return dd($sum_liquidado);
         //Recibo dinero de boletos con anticipo
         $sum_anticipo += Reservacione::whereBetween('vendedor_anticipa_fecha', [$from, $to])
                     ->whereNull('deleted_at')
                     ->where('vendedor_anticipa_id',$request->user_id)
                     ->where(function($qb){
                       $qb->where('estatus','pagado')
                       ->orWhere('estatus','apartado');
                     })
                     ->sum('anticipo');
        $sumatorias = [
          'sum_pagado' => $sum_pagado,
          'sum_liquidado' => $sum_liquidado,
          'sum_anticipado' => $sum_anticipo,
          'sum_total' => ($sum_pagado+$sum_liquidado+$sum_anticipo),
        ];

         // return dd( compact('vendedor','users','reservaciones', 'envios', 'sum_envios', 'sumatorias'));

     }


     return view('admin.corte_de_caja_basico.index', compact('vendedor','users','reservaciones', 'envios', 'sum_envios', 'sumatorias' ,'hay_quest'));
 }
}
