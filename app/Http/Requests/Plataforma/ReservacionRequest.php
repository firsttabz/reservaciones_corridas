<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;

class ReservacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'redondo' => 'required|in:true,false',
            'ida' => 'required|exists:corridas,id',
            'ida_fecha' => 'required|date_format:d/m/Y',
        ];
    }
}
