<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;

class ReservacionPlataforma extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'redondo' => 'required|in:true,false',
            'reservaciones' => 'required',
            'reservaciones.ida' => 'required|array|min:1',
        ];
    }
}
