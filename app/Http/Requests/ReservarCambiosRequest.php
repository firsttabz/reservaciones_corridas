<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservarCambiosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          "fecha_cambio" => "required",
          "asiento_cambio" => "required",
          "corrida_cambio" => "required",
          "clave" => "required",
          "user_id" => "required",
        ];
    }
}
