<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_viaje' => 'required|in:sencillo,redondo',
            'cliente' => 'required|string|max:191|min:1',
            //'reservaciones.*.costo_boleto' => 'required|numeric',
            //'reservaciones.*.asiento.id' => 'required|integer',
            //'reservaciones.*.estatus' => 'required|in:apartado,pagado',
            //'reservaciones.*.corrida_id' => 'required|integer',
            //'reservaciones.*.fecha_reservacion' => 'required',
        ];
    }
}
