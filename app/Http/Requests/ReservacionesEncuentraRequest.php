<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservacionesEncuentraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_viaje' => 'required|in:sencillo,redondo',
            'cliente' => 'required|string|max:191',
            //'reservaciones.*.corrida_id' => 'required|integer'
        ];
    }
}
