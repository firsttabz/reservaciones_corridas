<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'razon_social' => 'required',
          'rfc' => 'required',
          'domicilio' => 'required',
          'celular' => 'required',
          'regimen_fiscal' => 'required',
          'uso_cfdi' => 'required',
          'email' => 'required|email|unique:clientes',
        ];
    }
}
