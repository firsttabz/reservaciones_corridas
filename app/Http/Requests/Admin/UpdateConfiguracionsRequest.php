<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateConfiguracionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'costo_boleto_sencillo' => 'required',
            'costo_boleto_redondo' => 'required',
            'tiempo_espera_reservacion_plataforma' => 'max:2147483647|required|numeric',
            'tiempo_espera_reservacion' => 'max:2147483647|required|numeric',
        ];
    }
}
