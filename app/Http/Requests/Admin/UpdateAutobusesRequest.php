<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAutobusesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'marca' => 'required',
            'modelo' => 'required',
            'placas' => 'required|unique:autobuses,placas,'.$this->route('autobus'),
            'propietario' => 'required',
            //'numero_asientos' => 'max:2147483647|required|numeric',
            'costo_renta' => 'required|numeric'

        ];
    }
}
