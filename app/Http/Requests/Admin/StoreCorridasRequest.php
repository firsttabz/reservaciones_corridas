<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreCorridasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'hora_salida' => 'required|date_format:H:i:s',
            'costo_boleto_sencillo' => 'required',
            'costo_boleto_redondo' => 'required',
            'total_asientos' => 'required|in:53,51,50,48,47,46,20',
            'camioneta' => 'required|in:0,1'
            //'autobus_id' => 'required|integer',
        ];
    }
}
