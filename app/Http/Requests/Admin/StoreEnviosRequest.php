<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreEnviosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'remitente' => 'required|max:191',
            'telefono_remitente' => 'required|numeric',
            'destinatario' => 'required|max:191',
            'telefono_destinatario' => 'required|numeric',
            'costo' => 'required|numeric',
            'pagado' => 'required',
            //'estatus' => 'required',
            // 'clave' => 'max:6',
            'descripcion' => 'required|max:200',
            'categoria' => 'required|max:35',
            'id_corrida' => 'required|integer',
            //'vendedor_id_entrega' => 'required',
            //'vendedor_id_envio' => 'required',
        ];
    }
}
