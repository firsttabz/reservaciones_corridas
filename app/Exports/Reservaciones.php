<?php

namespace App\Exports;

use App\Reservacione;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class Reservaciones implements FromCollection,WithHeadings,WithMapping,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $reservaciones;

    public function collection()
    {
      return $this->reservaciones;
    }
    public function __construct( $reservaciones ){

      $this->reservaciones = collect($reservaciones);
      //return dd($reservaciones->values());
    }
    public function headings(): array
  {
    return [
      'Asiento',
      'Clave',
      'Cliente',
      'Teléfono',
      'Corrida',
      'Fecha de reservación',
      'Costo boleto',
      'Descuento',
      'Redondo',
      'Estatus',
      'Anticipo',
      'Liquidacion',
      'Sucursal',
      'Vendedor recibe pago',
      'Fecha pagado',
      //'Vendedor aparta pago',
      //'Vendedor liquida pago',
      ];
  }
  public function map($reservaciones): array
   {
       return [
           $reservaciones['asiento'],
           $reservaciones['clave'],
           $reservaciones['cliente'],
           $reservaciones['phone'],
           $reservaciones['corrida'],
           $reservaciones['fecha_reservacion'],
           $reservaciones['costo_boleto'],
           $reservaciones['descuento'],
           $reservaciones['redondo'],
           $reservaciones['estatus'],
           $reservaciones['anticipo'],
           $reservaciones['resta'],
           $reservaciones['sucursal'],
           $reservaciones['vendedor_pagado'],
           $reservaciones['fecha_pagado'],
           //$reservaciones['vendedor_aparto'],
           //$reservaciones['vendedor_liquido']
       ];
 }
}
