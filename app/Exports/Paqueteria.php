<?php

namespace App\Exports;

use App\Envio;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class Paqueteria implements FromCollection,WithHeadings,WithMapping,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $paqueteria;

    public function collection()
    {
      return $this->paqueteria;
    }
    public function __construct( $paqueteria ){
      $this->paqueteria = $paqueteria;
    }
   public function headings(): array
     {
      return [
        'Clave',
        'Estatus',
        'Remitente',
        'Teléfono remitente',
        'Destinatario',
        'Teléfono destinatario',
        'Pagado',
        'Estatus',
        'Descripción',
        'Corrida'
          ];
      }
      public function map($paqueteria): array
       {
           return [
               $paqueteria->clave,
               $paqueteria->estatus,
               $paqueteria->remitente,
               $paqueteria->telefono_remitente,
               $paqueteria->destinatario,
               $paqueteria->telefono_destinatario,
               $paqueteria->pagado,
               $paqueteria->estatus,
               $paqueteria->descripcion,
               $paqueteria->corrida,
           ];
     }
}
