<?php

namespace App\Mail;

use App\Reservacione;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class SendEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $fecha_reservacion;
    public $tipo_viaje;
    public $email;
    public $clave;
    public $cliente;
    public $suma_boletos;
    public $cantidad_boletos_pagados;
    public $reservaciones;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($clave, $suma_boletos, $cantidad_boletos_pagados, $reservaciones, $cliente, $email, $tipo_viaje, $fecha_reservacion)
    {

      if($tipo_viaje == 1){
        $tipo_viaje = "redondo(s)";
      }else{
        $tipo_viaje = "sencillo(s)";
      }

      $this->fecha_reservacion = $fecha_reservacion;
      $this->tipo_viaje = $tipo_viaje;
      $this->email = $tipo_viaje;
      $this->cliente = $cliente;
      $this->clave = $clave;
      $this->suma_boletos = $suma_boletos;
      $this->cantidad_boletos_pagados = $cantidad_boletos_pagados;
      $this->reservaciones = $reservaciones;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.send_email');
    }
}
