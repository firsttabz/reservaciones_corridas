<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Milon\Barcode\DNS1D;
use App\Configuracion;
use DateTime;
class Envio extends Model
{
    protected $fillable = [
        'remitente',
        'telefono_remitente',
        'destinatario',
        'telefono_destinatario',
        'costo',
        'estatus',
        'clave',
        'descripcion',
        'categoria',
        'id_corrida',
        'vendedor_id_entrega',
        'vendedor_id_recibe',
        'vendedor_id_dinero',
        'vendedor_id_cancela',
        'vendedor_cancela_fecha',
        'pagado',
        'sucursal_id_dinero',
    ];

    public static function generarTicketEnvio($data){
      /*  $data=[
            'remitente' => 'Alejandro bernal zamorano',
            'destinatario'=> 'Jhonathan duarte coronel',
            'costo'=> '300.00',
            'estatus'=> 'Enviado',
            'clave'=> '853DF1',
            'descripcion'=> 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu',
            'categoria'=> 'Libros o similares',
            'id_corrida'=> '2358A',
            'pagado'=> 'pendiente',
            'fecha_envio' => '2018/09/20',
            'fecha_boleto' => '2018/09/2018',
            'vendedor' => 'Administrador uno',
            'nombre_empresa' => 'Linea Dorada',
            'pagina_empresa' => 'Jasbit.com'
        ];*/
            $configuracion = Configuracion::first();
            $img = Image::make('quickadmin/images/envio.png');
            $watermark = Image::make('quickadmin/images/autobus.jpg')->opacity(20);
            $img->insert($watermark, 'top-center', 10, 5);
            $img->text($data['nombre_empresa'],175,20, function($font) {
            $font->file(public_path('font/RobotoCondensed-Bold.ttf'));
            $font->size(22);
            $font->color('#0E0E0D');
            $font->align('center');
            $font->valign('top');
            });
          /* TERMINAL MÉXICO */
          $img->text("Terminal México: Privada de Vallarta #13",175,48, function($font) {
            $font->file(public_path('font/Roboto-BoldItalic.ttf'));
            $font->size(17.6);
            $font->color('#0E0E0D');
            $font->align('center');
            $font->valign('top');
            });
    $img->text("Entre Ignacio Vallarta y Ignacio Ramírez", 175, 75, function($font) {
            $font->file(public_path('font/Roboto-BoldItalic.ttf'));
            $font->size(17.6);
            $font->color('#0E0E0D');
            $font->align('center');
            $font->valign('top');
            });
    $img->text("tel: 55-570-333-06", 175, 102, function($font) {
            $font->file(public_path('font/Roboto-BoldItalic.ttf'));
            $font->size(17.6);
            $font->color('#0E0E0D');
            $font->align('center');
            $font->valign('top');
            });
    /* END TERMINAL OAXACA */
          /* TERMINAL OAXACA */
          $img->text("Terminal Oaxaca: Periférico sur #317",175, 130, function($font) {
                    $font->file(public_path('font/Roboto-BoldItalic.ttf'));
                    $font->size(20);
                    $font->color('#0E0E0D');
                    $font->align('center');
                    $font->valign('top');
                    });
          $img->text("Entre huzares y Guadalupe victoria", 175, 157, function($font) {
                    $font->file(public_path('font/Roboto-BoldItalic.ttf'));
                    $font->size(20);
                    $font->color('#0E0E0D');
                    $font->align('center');
                    $font->valign('top');
                    });

          $img->text("tel: 951-351-81-23", 175, 184, function($font) {
                    $font->file(public_path('font/Roboto-BoldItalic.ttf'));
                    $font->size(20);
                    $font->color('#0E0E0D');
                    $font->align('center');
                    $font->valign('top');
                    });
          /* END TERMINAL OAXACA */
          /* DATOS DE LA VENTA */
         $img->line(30, 100+110, 315, 100+110, function ($draw) {
             $draw->color('#0E0E0D');
             });
                $img->text("*** ENVIO ***", 175, 220, function($font) {
                    $font->file(public_path('font/Roboto-Bold.ttf'));
                    $font->size(25);
                    $font->color('#0E0E0D');
                    $font->align('center');
                    $font->valign('top');
                    });
                    setlocale(LC_ALL,"es_ES.UTF8");
                    $string = date("d/m/Y", strtotime($data['fecha_envio']));
                    $dateTime = DateTime::createFromFormat("d/m/Y", $string);
                $img->text(ucwords(strftime("%A, %e",$dateTime->getTimestamp())), 175, 249, function($font) {
                    $font->file(public_path('font/Roboto-Bold.ttf'));
                    $font->size(22);
                    $font->color('#0E0E0D');
                    $font->align('right');
                    $font->valign('top');
                    });
                $img->text(ucwords(strftime("%B %G",$dateTime->getTimestamp())), 100, 277, function($font) {
                    $font->file(public_path('font/Roboto-Bold.ttf'));
                    $font->size(22);
                    $font->color('#0E0E0D');
                    $font->align('left');
                    $font->valign('top');
                    });
                $img->text("Rtte: ".$data['remitente'], 175, 304, function($font) {
                    $font->file(public_path('font/Roboto-Bold.ttf'));
                    $font->size(21);
                    $font->color('#0E0E0D');
                    $font->align('center');
                    $font->valign('top');
                    });
                $img->text("Dest: ".$data['destinatario'], 175, 329, function($font) {
                          $font->file(public_path('font/Roboto-Bold.ttf'));
                          $font->size(21);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
                $img->text("Costo: $".$data['costo']."MXN", 175, 351, function($font) {
                          $font->file(public_path('font/Roboto-Bold.ttf'));
                          $font->size(22);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
                $img->text("Estatus: ".$data['estatus'], 175, 382, function($font) {
                          $font->file(public_path('font/Roboto-Bold.ttf'));
                          $font->size(21);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
                $img->text("Clave N°: ".$data['clave'], 175, 407, function($font) {
                          $font->file(public_path('font/Roboto-Bold.ttf'));
                          $font->size(21);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
                $img->text("Categoria: ".$data['categoria'], 175, 431, function($font) {
                        $font->file(public_path('font/Roboto-Bold.ttf'));
                        $font->size(21);
                        $font->color('#0E0E0D');
                        $font->align('center');
                        $font->valign('top');
                        });
                $img->text("Descripción: ".substr($data['descripcion'],0,30), 175, 458, function($font) {
                          $font->file(public_path('font/OpenSans-SemiBoldItalic.ttf'));
                          $font->size(15);
                          $font->color('#0E0E0D');
                          $font->align('center');
                          $font->valign('top');
                          });
                $img->text(substr($data['descripcion'],30,40), 175, 476, function($font) {
                            $font->file(public_path('font/OpenSans-SemiBoldItalic.ttf'));
                            $font->size(15);
                            $font->color('#0E0E0D');
                            $font->align('center');
                            $font->valign('top');
                            });
                $img->text("Corrida ".$data['id_corrida'], 175, 500, function($font) {
                                $font->file(public_path('font/Roboto-Bold.ttf'));
                                $font->size(21);
                                $font->color('#0E0E0D');
                                $font->align('center');
                                $font->valign('top');
                                });
            /*    $img->text("Entrega: ".$data['vendedor_id_entrega'], 175, 508, function($font) {
                                    $font->file(public_path('font/Roboto-Bold.ttf'));
                                    $font->size(19);
                                    $font->color('#0E0E0D');
                                    $font->align('center');
                                    $font->valign('top');
                                    });
                $img->text("Recibe: ".$data['vendedor_id_recibe'], 175, 528, function($font) {
                                        $font->file(public_path('font/Roboto-Bold.ttf'));
                                        $font->size(19);
                                        $font->color('#0E0E0D');
                                        $font->align('center');
                                        $font->valign('top');
                                        }); */
                $img->text("Pago  ".($data['pagado'] == 0 ? "Pendiente":"Realizado"), 175, 524, function($font) {
                            $font->file(public_path('font/Roboto-Bold.ttf'));
                            $font->size(19);
                            $font->color('#0E0E0D');
                            $font->align('center');
                            $font->valign('top');
                                            });
                $img->text("(ticket impreso el ".date("d/m/Y", strtotime($data['fecha_boleto'])).")", 175, 548, function($font) {
                            $font->file(public_path('font/OpenSans-SemiBoldItalic.ttf'));
                            $font->size(14);
                            $font->color('#0E0E0D');
                            $font->align('center');
                            $font->valign('top');
                                                });
                $img->line(30, 568, 315, 568, function ($draw) {
                            $draw->color('#0E0E0D');
                            });
                /* START TERMINOS */
                $tiempo = $configuracion->tiempo_espera_reservacion/60;
                $img->text("*El contenido de la paquetería es responsabilidad", 175, 581, function($font) {
                              $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                              $font->size(16);
                              $font->color('#0E0E0D');
                              $font->align('center');
                              $font->valign('top');
                              });
                 $img->text("del propietaria de la misma.", 175, 598, function($font) {
                                  $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                                  $font->size(16);
                                  $font->color('#0E0E0D');
                                  $font->align('center');
                                  $font->valign('top');
                                  });
                $img->text("*La empresa no se hace responsable de pérdidas,", 175, 615, function($font) {
                              $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                              $font->size(16);
                              $font->color('#0E0E0D');
                              $font->align('center');
                              $font->valign('top');
                              });
                $img->text("lesiones o desperfectos en la paquetería.", 175, 632, function($font) {
                                $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                                $font->size(16);
                                $font->color('#0E0E0D');
                                $font->align('center');
                                $font->valign('top');
                                });
                $img->text("*Se tendrá un tiempo máximo de 3 días", 175, 649, function($font) {
                              $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                              $font->size(16);
                              $font->color('#0E0E0D');
                              $font->align('center');
                              $font->valign('top');
                              });
                $img->text("para recoger la paquetería.", 175, 666, function($font) {
                                $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                                $font->size(16);
                                $font->color('#0E0E0D');
                                $font->align('center');
                                $font->valign('top');
                                });
                $img->text("*Toda paquetería será previamente revisada.", 175, 683, function($font) {
                                $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                                $font->size(16);
                                $font->color('#0E0E0D');
                                $font->align('center');
                                $font->valign('top');
                                });
               $img->text("antes de ser empaquetada.", 175, 700, function($font) {
                                  $font->file(public_path('font/RobotoCondensed-Italic.ttf'));
                                  $font->size(16);
                                  $font->color('#0E0E0D');
                                  $font->align('center');
                                  $font->valign('top');
                                  });
                $img->line(30, 722, 315, 722, function ($draw) {
                                    $draw->color('#0E0E0D');
                                    });
                /* END TERMINOS */
              $img->text("Le atendio ".$data['vendedor'], 175, 724, function($font) {
                        $font->file(public_path('font/Roboto-Bold.ttf'));
                        $font->size(15);
                        $font->color('#0E0E0D');
                        $font->align('center');
                        $font->valign('top');
                        });
             $img->text("Visitenos en ".$data['pagina_empresa'], 175, 738, function($font) {
                       $font->file(public_path('font/Roboto-Bold.ttf'));
                       $font->size(15);
                       $font->color('#0E0E0D');
                       $font->align('center');
                       $font->valign('top');
                       });
              $barcode = Image::make(DNS1D::getBarcodePNG($data['clave'],"C128A"))->resize(300, 55);
              $img->insert($barcode, 'top-center', 1200, 759);
                return $img->response('jpg', 100);
 }
}
