<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sucursale
 *
 * @package App
 * @property string $nombre
 * @property string $direccion
*/
class Sucursale extends Model
{
    use SoftDeletes;

    protected $fillable = ['nombre', 'direccion','estado'];
    protected $hidden = [];


    public static function boot()
    {
        parent::boot();

        Sucursale::observe(new \App\Observers\UserActionsObserver);
    }

}
