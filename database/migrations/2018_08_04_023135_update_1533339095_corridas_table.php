<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533339095CorridasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corridas', function (Blueprint $table) {
            if(Schema::hasColumn('corridas', 'chofer_id')) {
                $table->dropForeign('192217_5b64d27836483');
                $table->dropIndex('192217_5b64d27836483');
                $table->dropColumn('chofer_id');
            }
            if(Schema::hasColumn('corridas', 'autobus_id')) {
                $table->dropForeign('192217_5b64d27846cb9');
                $table->dropIndex('192217_5b64d27846cb9');
                $table->dropColumn('autobus_id');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corridas', function (Blueprint $table) {
                        
        });

    }
}
