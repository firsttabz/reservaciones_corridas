<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnviosSucursal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('envios', function(Blueprint $table) {
        if (!Schema::hasColumn('envios', 'sucursal_id_dinero')) {
          $table->integer('sucursal_id_dinero')->unsigned()->nullable();
          $table->foreign('sucursal_id_dinero')->references('id')->on('sucursales')->onDelete('cascade');
        }
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
