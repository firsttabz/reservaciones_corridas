<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533337833ReservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservaciones', function (Blueprint $table) {
            if(Schema::hasColumn('reservaciones', 'fecha')) {
                $table->dropColumn('fecha');
            }
            if(Schema::hasColumn('reservaciones', 'estatus')) {
                $table->dropColumn('estatus');
            }
            
        });
Schema::table('reservaciones', function (Blueprint $table) {
            
if (!Schema::hasColumn('reservaciones', 'estatus')) {
                $table->enum('estatus', array('reservado', 'anticipo', 'cancelado'))->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservaciones', function (Blueprint $table) {
            $table->dropColumn('estatus');
            
        });
Schema::table('reservaciones', function (Blueprint $table) {
                        $table->date('fecha')->nullable();
                $table->enum('estatus', array('disponible', 'reservado', 'anticipo'))->nullable();
                
        });

    }
}
