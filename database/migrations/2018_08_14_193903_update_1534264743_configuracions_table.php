<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1534264743ConfiguracionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configuracions', function (Blueprint $table) {
            
if (!Schema::hasColumn('configuracions', 'forzar_chofer')) {
                $table->tinyInteger('forzar_chofer')->nullable()->default('0');
                }
if (!Schema::hasColumn('configuracions', 'forzar_autobus')) {
                $table->tinyInteger('forzar_autobus')->nullable()->default('0');
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configuracions', function (Blueprint $table) {
            $table->dropColumn('forzar_chofer');
            $table->dropColumn('forzar_autobus');
            
        });

    }
}
