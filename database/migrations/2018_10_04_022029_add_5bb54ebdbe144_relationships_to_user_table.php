<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5bb54ebdbe144RelationshipsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            if (!Schema::hasColumn('users', 'role_id')) {
                $table->integer('role_id')->unsigned()->nullable();
                $table->foreign('role_id', '214221_5bb244f524c60')->references('id')->on('roles')->onDelete('cascade');
                }
                if (!Schema::hasColumn('users', 'sucursal_id')) {
                $table->integer('sucursal_id')->unsigned()->nullable();
                $table->foreign('sucursal_id', '214221_5bb54ebce271e')->references('id')->on('sucursales')->onDelete('cascade');
                }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {

        });
    }
}
