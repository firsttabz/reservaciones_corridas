<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533334879CorridasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corridas', function (Blueprint $table) {
            if(Schema::hasColumn('corridas', 'total_asientos')) {
                $table->dropColumn('total_asientos');
            }
            
        });
Schema::table('corridas', function (Blueprint $table) {
            
if (!Schema::hasColumn('corridas', 'total_asientos')) {
                $table->integer('total_asientos')->nullable()->unsigned();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corridas', function (Blueprint $table) {
            $table->dropColumn('total_asientos');
            
        });
Schema::table('corridas', function (Blueprint $table) {
                        $table->string('total_asientos')->nullable();
                
        });

    }
}
