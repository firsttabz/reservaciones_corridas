<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533333081ConfiguracionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('configuracions')) {
            Schema::create('configuracions', function (Blueprint $table) {
                $table->increments('id');
                $table->decimal('costo_boleto_sencillo', 15, 2)->nullable();
                $table->decimal('costo_boleto_redono', 15, 2)->nullable();
                $table->integer('tiempo_espera_reservacion')->nullable()->unsigned();
                $table->integer('tiempo_espera_envio')->nullable()->unsigned();
                $table->integer('total_asientos')->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuracions');
    }
}
