<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5b64e5d7a8dbfRelationshipsToCorridaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corridas', function(Blueprint $table) {
            if (!Schema::hasColumn('corridas', 'chofer_id')) {
                $table->integer('chofer_id')->unsigned()->nullable();
                $table->foreign('chofer_id', '192217_5b64d27836483')->references('id')->on('chofers')->onDelete('cascade');
                }
                if (!Schema::hasColumn('corridas', 'autobus_id')) {
                $table->integer('autobus_id')->unsigned()->nullable();
                $table->foreign('autobus_id', '192217_5b64d27846cb9')->references('id')->on('autobuses')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corridas', function(Blueprint $table) {
            
        });
    }
}
