<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaqueteriaUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('envios', function (Blueprint $table) {

          $table->integer('vendedor_id_cancela')->unsigned()->nullable();
          $table->foreign('vendedor_id_cancela')->references('id')->on('users')->onDelete('cascade');
          $table->timestamp('vendedor_cancela_fecha')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
