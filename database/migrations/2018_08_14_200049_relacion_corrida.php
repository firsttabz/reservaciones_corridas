<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelacionCorrida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('corridas', function(Blueprint $table) {
          //if (!Schema::hasColumn('corridas', 'chofer_id')) {
              $table->integer('chofer_id')->unsigned()->nullable();
              $table->foreign('chofer_id')->references('id')->on('chofers')->onDelete('cascade');
            //  }
              //if (!Schema::hasColumn('corridas', 'autobus_id')) {
              $table->integer('autobus_id')->unsigned()->nullable();
              $table->foreign('autobus_id')->references('id')->on('autobuses')->onDelete('cascade');
              //}

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
