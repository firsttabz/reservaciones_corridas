<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533334808CorridasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corridas', function (Blueprint $table) {
            
if (!Schema::hasColumn('corridas', 'total_asientos')) {
                $table->string('total_asientos')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corridas', function (Blueprint $table) {
            $table->dropColumn('total_asientos');
            
        });

    }
}
