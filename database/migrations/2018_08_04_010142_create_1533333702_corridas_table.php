<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533333702CorridasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('corridas')) {
            Schema::create('corridas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre')->nullable();
                $table->integer('activo')->default(1);
                $table->integer('extraordinaria')->default(0);
                $table->tinyInteger('camioneta')->default(0);
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corridas');
    }
}
