<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('reservaciones', function (Blueprint $table) {

          $table->integer('vendedor_liquida_id')->unsigned()->nullable();
          $table->foreign('vendedor_liquida_id')->references('id')->on('users')->onDelete('cascade');
          $table->timestamp('vendedor_liquida_fecha')->nullable();

          $table->integer('vendedor_anticipa_id')->unsigned()->nullable();
          $table->foreign('vendedor_anticipa_id')->references('id')->on('users')->onDelete('cascade');
          $table->timestamp('vendedor_anticipa_fecha')->nullable();


          $table->integer('vendedor_cancela_id')->unsigned()->nullable();
          $table->foreign('vendedor_cancela_id')->references('id')->on('users')->onDelete('cascade');
          $table->timestamp('vendedor_cancela_fecha')->nullable();

          $table->integer('vendedor_pagado_id')->unsigned()->nullable();
          $table->foreign('vendedor_pagado_id')->references('id')->on('users')->onDelete('cascade');
          $table->timestamp('vendedor_pagado_fecha')->nullable();

          $table->integer('vendedor_cambio_id')->unsigned()->nullable();
          $table->foreign('vendedor_cambio_id')->references('id')->on('users')->onDelete('cascade');
          $table->timestamp('vendedor_cambio_fecha')->nullable();

      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
