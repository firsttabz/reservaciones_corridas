<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533334136CorridasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corridas', function (Blueprint $table) {
            
if (!Schema::hasColumn('corridas', 'hora_salida')) {
                $table->time('hora_salida')->nullable();
                }
if (!Schema::hasColumn('corridas', 'costo_boleto_sencillo')) {
                $table->decimal('costo_boleto_sencillo', 15, 2)->nullable();
                }
if (!Schema::hasColumn('corridas', 'costo_boleto_redondo')) {
                $table->string('costo_boleto_redondo')->nullable();
                }
if (!Schema::hasColumn('corridas', 'observaciones')) {
                $table->text('observaciones')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corridas', function (Blueprint $table) {
            $table->dropColumn('hora_salida');
            $table->dropColumn('costo_boleto_sencillo');
            $table->dropColumn('costo_boleto_redondo');
            $table->dropColumn('observaciones');
            
        });

    }
}
