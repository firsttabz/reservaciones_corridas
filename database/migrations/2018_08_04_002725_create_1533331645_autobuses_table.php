<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533331645AutobusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('autobuses')) {
            Schema::create('autobuses', function (Blueprint $table) {
                $table->increments('id');
                $table->string('marca')->nullable();
                $table->string('modelo')->nullable();
                $table->string('placas')->nullable();
                $table->string('propietario')->nullable();
                $table->float('costo_renta');
                $table->decimal('costo_boleto_redondo', 15, 2)->nullable();
                $table->decimal('costo_boleto_sencillo', 15, 2)->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autobuses');
    }
}
/*
51
50
46
53
48
20
*/
