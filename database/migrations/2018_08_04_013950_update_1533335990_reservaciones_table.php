<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533335990ReservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservaciones', function (Blueprint $table) {
            
if (!Schema::hasColumn('reservaciones', 'fecha')) {
                $table->date('fecha')->nullable();
                }
if (!Schema::hasColumn('reservaciones', 'estatus')) {
                $table->enum('estatus', array('disponible', 'reservado', 'anticipo'))->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservaciones', function (Blueprint $table) {
            $table->dropColumn('fecha');
            $table->dropColumn('estatus');
            
        });

    }
}
