<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1534263185ReservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservaciones', function (Blueprint $table) {
            if(Schema::hasColumn('reservaciones', 'estatus')) {
                $table->dropColumn('estatus');
            }
            
        });
Schema::table('reservaciones', function (Blueprint $table) {
            
if (!Schema::hasColumn('reservaciones', 'estatus')) {
                $table->string('estatus')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservaciones', function (Blueprint $table) {
            $table->dropColumn('estatus');
            
        });
Schema::table('reservaciones', function (Blueprint $table) {
                        $table->enum('estatus', array('reservado', 'anticipo', 'cancelado'))->nullable();
                
        });

    }
}
