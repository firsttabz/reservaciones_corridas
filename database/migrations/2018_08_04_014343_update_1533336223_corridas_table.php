<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533336223CorridasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corridas', function (Blueprint $table) {
            
if (!Schema::hasColumn('corridas', 'generar_lunes')) {
                $table->tinyInteger('generar_lunes')->nullable()->default('1');
                }
if (!Schema::hasColumn('corridas', 'generar_martes')) {
                $table->tinyInteger('generar_martes')->nullable()->default('1');
                }
if (!Schema::hasColumn('corridas', 'generar_miercoles')) {
                $table->tinyInteger('generar_miercoles')->nullable()->default('1');
                }
if (!Schema::hasColumn('corridas', 'generar_jueves')) {
                $table->tinyInteger('generar_jueves')->nullable()->default('1');
                }
if (!Schema::hasColumn('corridas', 'generar_viernes')) {
                $table->tinyInteger('generar_viernes')->nullable()->default('1');
                }
if (!Schema::hasColumn('corridas', 'generar_sabado')) {
                $table->tinyInteger('generar_sabado')->nullable()->default('1');
                }
if (!Schema::hasColumn('corridas', 'generar_domingo')) {
                $table->tinyInteger('generar_domingo')->nullable()->default('1');
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corridas', function (Blueprint $table) {
            $table->dropColumn('generar_lunes');
            $table->dropColumn('generar_martes');
            $table->dropColumn('generar_miercoles');
            $table->dropColumn('generar_jueves');
            $table->dropColumn('generar_viernes');
            $table->dropColumn('generar_sabado');
            $table->dropColumn('generar_domingo');
            
        });

    }
}
