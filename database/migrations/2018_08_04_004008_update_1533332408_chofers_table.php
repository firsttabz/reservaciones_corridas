<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533332408ChofersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chofers', function (Blueprint $table) {
            
if (!Schema::hasColumn('chofers', 'domicili_gmaps_address')) {
                $table->string('domicili_gmaps_address')->nullable();
                $table->double('domicili_gmaps_latitude')->nullable();
                $table->double('domicili_gmaps_longitude')->nullable();
                }
if (!Schema::hasColumn('chofers', 'celular')) {
                $table->string('celular')->nullable();
                }
if (!Schema::hasColumn('chofers', 'correo')) {
                $table->string('correo')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chofers', function (Blueprint $table) {
            $table->dropColumn('domicili_gmaps_address');
            $table->dropColumn('domicili_gmaps_latitude');
            $table->dropColumn('domicili_gmaps_longitude');
            $table->dropColumn('celular');
            $table->dropColumn('correo');
            
        });

    }
}
