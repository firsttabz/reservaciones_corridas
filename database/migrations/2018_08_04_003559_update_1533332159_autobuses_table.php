<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1533332159AutobusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autobuses', function (Blueprint $table) {
            
if (!Schema::hasColumn('autobuses', 'descripcion')) {
                $table->text('descripcion')->nullable();
                }
if (!Schema::hasColumn('autobuses', 'numero_asientos')) {
                $table->integer('numero_asientos')->nullable()->unsigned();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autobuses', function (Blueprint $table) {
            $table->dropColumn('descripcion');
            $table->dropColumn('numero_asientos');
            
        });

    }
}
