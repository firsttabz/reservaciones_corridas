<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorridasUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('corridas', function (Blueprint $table) {
        $table->string('nombre_punto_a',25)->nullable();
        $table->string('nombre_punto_b',25)->nullable();
        $table->string('lat_a',20)->nullable();
        $table->string('lng_a',20)->nullable();
        $table->string('lat_b',20)->nullable();
        $table->string('lng_b',20)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
