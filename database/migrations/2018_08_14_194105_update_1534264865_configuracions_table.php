<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1534264865ConfiguracionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configuracions', function (Blueprint $table) {
            if(Schema::hasColumn('configuracions', 'costo_boleto_redono')) {
                $table->dropColumn('costo_boleto_redono');
            }
            
        });
Schema::table('configuracions', function (Blueprint $table) {
            
if (!Schema::hasColumn('configuracions', 'costo_boleto_redondo')) {
                $table->decimal('costo_boleto_redondo', 15, 2)->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configuracions', function (Blueprint $table) {
            $table->dropColumn('costo_boleto_redondo');
            
        });
Schema::table('configuracions', function (Blueprint $table) {
                        $table->decimal('costo_boleto_redono', 15, 2)->nullable();
                
        });

    }
}
