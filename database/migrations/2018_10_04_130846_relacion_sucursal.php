<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelacionSucursal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('reservaciones', function(Blueprint $table) {
          if (!Schema::hasColumn('reservaciones', 'sucursal_id')) {
              $table->integer('sucursal_id')->unsigned()->nullable();
              $table->foreign('sucursal_id')->references('id')->on('sucursales')->onDelete('cascade');
              }
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
