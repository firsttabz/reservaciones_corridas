<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5b64db03a7bf1RelationshipsToReservacioneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservaciones', function(Blueprint $table) {
            if (!Schema::hasColumn('reservaciones', 'vendedor_id')) {
                $table->integer('vendedor_id')->unsigned()->nullable();
                $table->foreign('vendedor_id', '192218_5b64d6c1cdbc9')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('reservaciones', 'corrida_id')) {
                $table->integer('corrida_id')->unsigned()->nullable();
                $table->foreign('corrida_id', '192218_5b64d6c1dedec')->references('id')->on('corridas')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservaciones', function(Blueprint $table) {
            
        });
    }
}
