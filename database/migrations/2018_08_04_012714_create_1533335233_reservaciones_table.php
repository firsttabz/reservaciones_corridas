<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533335233ReservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('reservaciones')) {
            Schema::create('reservaciones', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cliente')->nullable();
                $table->string('phone',15)->nullable();
                $table->decimal('costo_boleto', 15, 2)->nullable();
                $table->decimal('anticipo', 15, 2)->nullable();
                $table->decimal('resta', 15, 2)->nullable();
                $table->decimal('costo_boleto_original',15,2)->nullable();
                $table->boolean('redondo')->default(0);
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservaciones');
    }
}
