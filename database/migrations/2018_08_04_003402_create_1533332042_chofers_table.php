<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533332042ChofersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('chofers')) {
            Schema::create('chofers', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre')->nullable();
                $table->string('edad')->nullable();
                $table->string('domicilio')->nullable();
                $table->string('telefono')->nullable();
                $table->text('nota')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chofers');
    }
}
