<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnviosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('remitente',191);
            $table->string('telefono_remitente',60);
            $table->string('destinatario',60);
            $table->string('telefono_destinatario',191);
            $table->string('costo',10);
            $table->string('estatus',10);
            $table->boolean('pagado')->default(0);
            $table->string('clave',10);
            $table->string('descripcion',200);
            $table->string('categoria',35);
            $table->date('fecha_envio')->nullable();
            $table->integer('id_corrida')->unsigned()->nullable();
            $table->integer('vendedor_id_entrega')->unsigned()->nullable();
            $table->integer('vendedor_id_recibe')->unsigned()->nullable();
            $table->integer('vendedor_id_dinero')->unsigned()->nullable();
            $table->timestamp('fecha_ingresa_dinero')->nullable();
            $table->timestamps();

            $table->foreign('id_corrida')->references('id')->on('corridas');
            $table->foreign('vendedor_id_entrega')->references('id')->on('users');
            $table->foreign('vendedor_id_recibe')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envios');
    }
}
