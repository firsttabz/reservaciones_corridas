<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5b64dd9a9aac8RelationshipsToAsientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asientos', function(Blueprint $table) {
            if (!Schema::hasColumn('asientos', 'autobus_id')) {
                $table->integer('corrida_id')->unsigned()->nullable();
                $table->foreign('corrida_id', '192223_5b64dd9a4a71f')->references('id')->on('corridas')->onDelete('cascade');
                }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asientos', function(Blueprint $table) {

        });
    }
}
