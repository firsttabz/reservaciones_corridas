<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(ConfiguracionSeed::class);
        $this->call(RoleSeed::class);
        $this->call(UserSeed::class);
        $this->call(UserActionSeed::class);
        //$this->call(SucursaleSeed::class);
        //$this->call(CorridasTableSeeder::class);
        //$this->call(EnviosTableSeeder::class);

    }
}
