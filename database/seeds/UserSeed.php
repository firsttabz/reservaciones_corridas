<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => '$2y$10$OxRNXaDe3Eao1Np1CaWaCe8YvO7B8H.vPEefVhXNvWUIyV5d/MR3O', 'role_id' => 1, 'remember_token' => '',],
            ['id' => 2, 'name' => 'Samuel', 'email' => 'samuel@jasbit.com', 'password' => bcrypt('Sammy007'), 'role_id' => 1, 'remember_token' => '',],
            ['id' => 3, 'name' => 'Jonathan', 'email' => 'jonathan@jasbit.com', 'password' => bcrypt('tarjetas.'), 'role_id' => 1, 'remember_token' => '',],
            ['id' => 4, 'name' => 'Alejandro Bernal', 'email' => 'alejandro@jasbit.com', 'password' => bcrypt('pambreala-05'), 'role_id' => 1, 'remember_token' => '',],
            ['id' => 5, 'name' => 'Angel Cruz Lpz', 'email' => 'angelcruzlpz@jasbit.com', 'password' => bcrypt('jasbit@1A'), 'role_id' => 1, 'remember_token' => '',],

        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
