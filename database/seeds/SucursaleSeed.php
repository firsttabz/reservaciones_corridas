<?php

use Illuminate\Database\Seeder;

class SucursaleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            [ 'nombre' => 'Colón', 'direccion' => 'por asignar', 'estado' =>'1'],
            [ 'nombre' => 'Periférico', 'direccion' => 'por asignar', 'estado' =>'1'],
            [ 'nombre' => 'Tlacolula', 'direccion' => 'por asignar', 'estado' =>'1'],
            [ 'nombre' => 'México', 'direccion' => 'por asignar', 'estado' =>'1'],

        ];

        foreach ($items as $item) {
            \App\Sucursale::create($item);
        }
    }
}
