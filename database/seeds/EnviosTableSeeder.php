<?php

use Illuminate\Database\Seeder;

class EnviosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'remitente' => 'Roberto Cruz García', 'destinatario' => 'Erick Adan Cruz García', 'id_corrida' => '1', 'categoria' => 'Sobre (10x10)', 'telefono_remitente' => '9511234567', 'telefono_destinatario' => '9517654321', 'descripcion' => 'Sobre con valor de $5,000 pesos mexicanos. En caso de que el destinatario con conteste su numero telfonico llamar al 1234567891', 'costo' => '560', 'vendedor_id_entrega' => 3, 'vendedor_id_recibe' => 2, 'estatus' => 'por pagar', 'clave' => '111111', 'fecha_envio' => '2018-09-20', 'created_at' => '2018-09-11 01:00:00'],
            ['id' => 2, 'remitente' => 'Juana De La Rosa Vasquez', 'destinatario' => 'Abigail Ramírez Álvarz', 'id_corrida' => '1', 'categoria' => 'Sobre (10x10)', 'telefono_remitente' => '9511234567', 'telefono_destinatario' => '9517654321', 'descripcion' => 'Sobre con valor de $5,000 pesos mexicanos. En caso de que el destinatario con conteste su numero telfonico llamar al 1234567891', 'costo' => '560', 'vendedor_id_entrega' => 3, 'vendedor_id_recibe' => 2, 'estatus' => 'por pagar', 'clave' => '222222', 'fecha_envio' => "2018-09-20", 'created_at' => '2018-09-11 01:00:00'],

        ];

        foreach ($items as $item) {
            \App\Envio::create($item);
        }
    }
}
