<?php

use Illuminate\Database\Seeder;

class CorridasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'nombre' => 'oaxaca - mexico', 'activo' => 1, 'extraordinaria' => 0, 'camioneta' => 1, 'hora_salida' => '17:00:00', 'costo_boleto_sencillo' => 250.00, 'costo_boleto_redondo' => 480, 'observaciones' => null, 'total_asientos' => 20, 'generar_lunes' => 1, 'generar_martes' => 1, 'generar_miercoles' => 1, 'generar_jueves' => 1, 'generar_viernes' => 1, 'generar_sabado' => 1, 'generar_domingo' => 1, 'chofer_id' => null, 'autobus_id' => null],

        ];

        foreach ($items as $item) {
            \App\Corrida::create($item);
        }
    }
}
