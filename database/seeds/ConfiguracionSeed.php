<?php

use Illuminate\Database\Seeder;

class ConfiguracionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['costo_boleto_sencillo' => '250.00', 'costo_boleto_redondo' => '480.00', 'tiempo_espera_reservacion' => 60, 'tiempo_espera_envio' => 60, 'editar_precios' => 1, 'total_asientos' => 46],

        ];

        foreach ($items as $item) {
            \App\Configuracion::create($item);
        }
    }
}
