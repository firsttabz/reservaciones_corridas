<?php

$factory->define(App\Configuracion::class, function (Faker\Generator $faker) {
    return [
        "costo_boleto_sencillo" => $faker->randomNumber(2),
        "costo_boleto_redondo" => $faker->randomNumber(2),
        "tiempo_espera_reservacion" => $faker->randomNumber(2),
        "editar_precios" => 0,
        "aplicar_descuentos" => 0,
        "forzar_chofer" => 0,
        "forzar_autobus" => 0,
    ];
});
