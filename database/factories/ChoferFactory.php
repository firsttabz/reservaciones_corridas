<?php

$factory->define(App\Chofer::class, function (Faker\Generator $faker) {
    return [
        "nombre" => $faker->name,
        "edad" => $faker->name,
        "domicilio" => $faker->name,
        "telefono" => $faker->name,
        "celular" => $faker->name,
        "nota" => $faker->name,
        "correo" => $faker->safeEmail,
    ];
});
