<?php

$factory->define(App\Reservacione::class, function (Faker\Generator $faker) {
    return [
        "cliente" => $faker->name,
        "costo_boleto" => $faker->randomNumber(2),
        "vendedor_id" => factory('App\User')->create(),
        "asiento_id" => factory('App\Asiento')->create(),
        "corrida_id" => factory('App\Corrida')->create(),
        "clave" => $faker->name,
        "descuento" => 0,
        "observaciones" => $faker->name,
        "estatus" => collect(["cancelado","apartado","pagado",])->random(),
    ];
});
