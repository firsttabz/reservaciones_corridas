<?php

$factory->define(App\Sucursale::class, function (Faker\Generator $faker) {
    return [
        "nombre" => $faker->name,
        "direccion" => $faker->name,
    ];
});
