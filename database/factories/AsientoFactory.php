<?php

$factory->define(App\Asiento::class, function (Faker\Generator $faker) {
    return [
        "numero" => $faker->randomNumber(2),
        "autobus_id" => factory('App\Autobus')->create(),
    ];
});
