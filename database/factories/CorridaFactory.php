<?php

$factory->define(App\Corrida::class, function (Faker\Generator $faker) {
    return [
        "nombre" => $faker->name,
        "hora_salida" => $faker->date("H:i:s", $max = 'now'),
        "costo_boleto_sencillo" => $faker->randomNumber(2),
        "costo_boleto_redondo" => $faker->name,
        "total_asientos" => $faker->randomNumber(2),
        "observaciones" => $faker->name,
        "generar_lunes" => 1,
        "generar_martes" => 1,
        "generar_miercoles" => 1,
        "generar_jueves" => 1,
        "generar_viernes" => 1,
        "generar_sabado" => 1,
        "generar_domingo" => 1,
        "chofer_id" => factory('App\Chofer')->create(),
        "autobus_id" => factory('App\Autobus')->create(),
    ];
});
