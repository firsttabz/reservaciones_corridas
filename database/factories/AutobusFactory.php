<?php

$factory->define(App\Autobus::class, function (Faker\Generator $faker) {
    return [
        "marca" => $faker->name,
        "modelo" => $faker->name,
        "placas" => $faker->name,
        "propietario" => $faker->name,
        "numero_asientos" => $faker->randomNumber(2),
        "descripcion" => $faker->name,
    ];
});
