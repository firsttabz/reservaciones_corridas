<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Tu password debe tener 6 caracteres como mínimo.',
    'reset' => 'Tu password ha sido reseteado',
    'sent' => 'Te enviamos un email al correo especificado',
    'token' => 'El token del password es inválido.',
    'user' => "No existe el registo del email en el sistema.",

];
