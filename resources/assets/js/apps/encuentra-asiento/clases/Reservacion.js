import * as moment from 'moment';
export const Reservacion = {
    tipo_viaje:'sencillo',
    cliente: null,
    finalizado: false,
    reservaciones: [
        {
            costo_boleto: 0,
            fecha_reservacion: null,
            asiento: null,
            estatus:'pagado',
            anticipo:0,
            descuento:0,
        },
        {
            costo_boleto: 0,
            fecha_reservacion: null,
            asiento: null,
            estatus:'pagado',
            anticipo:0,
            descuento:0,
        }
    ],
    err:{
      corridas: false,
    },
    reservacion_salida: null,
    reservacion_regreso: null,
}
