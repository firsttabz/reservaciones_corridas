import Vue from 'vue'
//interceptor de requuest
window.axios.interceptors.request.use(function (config) {
  return config;
}, function (error) {
    // Do something with request error
    console.error(":(")

    return Promise.reject(error);
  });


  //inbterceprtor de response
  window.axios.interceptors.response.use(function (response) {
    // Do something with response data
    console.log('success',response.data);
    if(response.data.message) {
      Vue.notify({
        group: 'notification',
        title: 'Exito',
        type: 'success',
        position:'top center',
        text: response.data.message,
        duration: 10000,
      });
    }
    return response;
  },
  function (error) {
    console.log('err',error.response);
    //imprimiendo otros errores
    if(error.response.data.errors){
      let errors = error.response.data.errors;
      for (var key in errors){
          var attrName = key;
          var attrValue = errors[key];
          Vue.notify({
            group: 'notification',
            //title: 'Error',
            type: 'error',
            position:'top center',
            text: attrValue,
            duration: 10000,
          });
      }
    }
    //imprimiendo el mensaje principal
    if(error.response.data.message){
      Vue.notify({
        group: 'notification',
        title: 'Error',
        //type: 'warn',
        type: 'error',
        position:'top center',
        text: error.response.data.message,
        duration: 10000,
      });
    }
    // Do something with response error
    return Promise.reject(error);
  });
