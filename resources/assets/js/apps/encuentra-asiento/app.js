
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/*
*Implementamos interceptores de http
*/
require('./interceptors.js');


window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 import Vue           from 'vue'
 import Notifications from 'vue-notification'

 /*
 or for SSR:
 import Notifications from 'vue-notification/dist/ssr.js'
 */

 Vue.use(Notifications)

Vue.component('encuentra-asiento-app', require('./components/MainComponent.vue'));
//Vue.component('venta', require('./components/VentaComponent.vue'));

const app = new Vue({
    el: '#app-encuentra-asiento'
});
