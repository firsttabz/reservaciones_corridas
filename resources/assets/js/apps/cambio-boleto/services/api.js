const server = '/api/v1';
//const server = 'http://bus.jasbit.com/api/v1';
export const api = {
    listarCorridas: function(__callback){
        window.axios.get(server + '/corridas')
        .then((res) => {__callback(res.data)})
        .catch((err) => {__callback(null);console.log('ocurrio un error al obtener las corridas');console.log(err);})
    },
    buscarAsientos: function(reservacion,__callback){
      window.axios.post(server + '/reservaciones/cambios/encuentra',reservacion)
      .then((res) => {__callback(res.data)})
      .catch((err) => {__callback(null);console.log('ocurrio un error al buscarAsientos');console.log(err);})
    },
    buscarClave: function(clave,__callback){
      window.axios.post(server + '/reservaciones/reservacion',{clave})
      .then((res) => {__callback(res.data)})
      .catch((err) => {__callback(null);console.log('ocurrio un error al buscarAsientos');console.log(err);})
    },
    realziarCambio: function(data, __callback){
      window.axios.post(server + '/reservaciones/cambios/reserva',data)
      .then((res) => {__callback(res.data)})
      .catch((err) => {__callback(null);console.log('ocurrio un error al buscarAsientos');console.log(err);})
    }
}
