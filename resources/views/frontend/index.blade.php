<?php  use Carbon\Carbon;?>
@extends('layouts.base')
@section('content')
<!-- BEGIN .outer-wrapper -->
<div class="outer-wrapper">

  <!-- BEGIN .header-area-1 -->
  <div class="header-area-1">
    <!-- BEGIN .header-content -->
    <div class="header-content">

      <!-- BEGIN .logo -->
      <div class="logo">
        <h2><a href="#"><img src="{{url('page/assets/images/logo_vuz.png')}}" style="width:200px;" alt=""></a></h2>
      <!-- END .logo -->
      </div>

      <!-- BEGIN .header-icons-wrapper -->
      <div class="header-icons-wrapper clearfix">

        <a href="#" class="topright-button"><span>Venta en linea</span></a>
        <!-- BEGIN .header-icons-inner -->
        <div class="header-icons-inner clearfix">

          <!-- BEGIN .header-icon -->
          <div class="header-icon">
            <p><i class="fa fa-check-square-o" aria-hidden="true"></i><strong>Texto por asignar 1</strong></p>
            <p class="header-icon-text">Submenu texto 1</p>
          <!-- END .header-icon -->
          </div>

          <!-- BEGIN .header-icon -->
          <div class="header-icon">
            <p><i class="fa fa-check-square-o" aria-hidden="true"></i><strong>Texto por asignar 2</strong></p>
            <p class="header-icon-text">Submenu texto 2</p>
          <!-- END .header-icon -->
          </div>

          <!-- BEGIN .header-icon -->
          <div class="header-icon">
            <p><i class="fa fa-check-square-o" aria-hidden="true"></i><strong>Texto por asignar 3</strong></p>
            <p class="header-icon-text">Submenu texto 1</p>
          <!-- END .header-icon -->
          </div>

        <!-- END .header-icons-inner -->
        </div>

      <!-- END .header-icons-wrapper -->
      </div>

      <div id="mobile-navigation">
        <a href="#" id="mobile-navigation-btn"><i class="fa fa-bars"></i></a>
      </div>

      <div class="clearboth"></div>

      <!-- BEGIN .mobile-navigation-wrapper -->
      <div class="mobile-navigation-wrapper">

        <ul>
          <!--li class="current-menu-item current_page_item menu-item-has-children"><a href="#">Login</a></li-->
          <li class="menu-item-has-children"><a href="#">Servicios</a></li>
          <!--li class="menu-item-has-children"><a href="testimonials.html">Testimonios</a></li-->
          <li class="menu-item-has-children"><a href="#">Conocenos</a></li>
          <li class="menu-item-has-children"><a href="#">Contactanos</a></li>
        </ul>

      <!-- END .mobile-navigation-wrapper -->
      </div>

    <!-- END .header-content -->
    </div>

    <!-- BEGIN #primary-navigation -->
    <nav id="primary-navigation" class="navigation-wrapper fixed-navigation clearfix">

      <!-- BEGIN .navigation-inner -->
      <div class="navigation-inner">

        <!-- BEGIN .navigation -->
        <div class="navigation" style="padding: 0px 0px 0px 350px;/*padding: 0px 258px 0px 270px;*/">

          <ul>
            <!--li class="current-menu-item current_page_item menu-item-has-children"><a href="#">Login</a></li-->
            <li class="menu-item-has-children"><a href="#">Servicios</a></li>
            <!--li class="menu-item-has-children"><a href="testimonials.html">Testimonios</a></li-->
            <li class="menu-item-has-children"><a href="#">Conocenos</a></li>
            <li class="menu-item-has-children"><a href="#">Contactanos</a></li>
          </ul>

        <!-- END .navigation -->
        </div>

      <!-- END .navigation-inner -->
      </div>

    <!-- END #primary-navigation -->
    </nav>

  <!-- END .header-area-1 -->
  </div>
<!-- MAPA -->
<!--div id="googlemaps"></div-->
<!-- END MAPA -->
  <!-- BEGIN .large-header-wrapper -->
  <div class="large-header-wrapper" style="">

    <!-- BEGIN .large-header -->
    <div class="large-header">

      <!-- BEGIN .header-booking-form-wrapper -->
      <!-- EMPIEZA FORMULARIO -->
      <div class="header-booking-form-wrapper" >

        <!-- BEGIN #booking-tabs -->
        <div id="booking-tabs" style="z-index: 1;position: relative;background: #00000096;">

          <ul class="nav clearfix">
            <li><a href="#sencillo" id="btn-sencillo">Sencillo</a></li>
            <li><a href="#redondo" id="btn-redondo">Redondo</a></li>
            <!--li><a href="#tab-hourly">Todos los tours</a></li-->
          </ul>

          <!-- BEGIN #tab-one-way -->
          <div id="sencillo" style="height: 365px;">

            <!-- BEGIN .booking-form-1 -->
            <form action="" class="booking-form-1" method="post">

              <!--input type="text" name="pickup-address" value="" placeholder="Nombre (cliente)" />
              <input type="text" name="dropoff-address" value="" placeholder="Número celular" /-->

              <div class="booking-form-time">
                <label>Corrida</label>
              </div>
              <div class="booking-form-hour">
                <div class="select-wrapper">
                  <i class="fa fa-angle-down"></i>
                  <select required="required" name="corrida_sencillo" id="servicio">
                    @if (count($corridas) > 0)
                    <option value="" selected>-- Seleccione una corrida --</option>
                        @foreach ($corridas as $corrida)
                    <option value="{{$corrida->id}}" placeholder"selecciones una corrida">{{$corrida->nombre}} {{$corrida->hora_salida}}</option>
                    @endforeach
                @else
                <option value="">Sin corridas</option>
                  @endif
                  </select>
                </div>
              </div>
              <div class="booking-form-time">
                <label>Fecha de ida</label><br>
              </div>
              <!--div class="booking-form-hour col-xs-12"-->
                <input type="text" name="ida_fecha" class="datepicker" value="" placeholder="Fecha de ida" />
              <!--/div-->
              <button type="button" class="btn-reservar-sencillo" style="margin-top:30px;">
                <span>Reserve ahora</span>
              </button>

            <!-- END .booking-form-1 -->
            </form>

          <!-- END #tab-one-way -->
          </div>

          <!-- BEGIN #tab-hourly -->
          <div id="redondo">

            <!-- BEGIN .booking-form-1 -->
            <!-- BEGIN .booking-form-1 -->
            <form action="" class="booking-form-1" method="post">

              <!--input type="text" name="pickup-address" value="" placeholder="Nombre (cliente)" />
              <input type="text" name="dropoff-address" value="" placeholder="Número celular" /-->

              <div class="booking-form-time">
                <label>Corrida de ida</label>
              </div>

              <div class="booking-form-hour">
                <div class="select-wrapper">
                  <i class="fa fa-angle-down"></i>
                  <select required="required" name="corrida_ida_redondo" id="servicio">
                    <option value="">-- Seleccione una corrida --</option>
                    @if (count($corridas) > 0)
                        @foreach ($corridas as $corrida)
                    <option value="{{$corrida->id}}">{{$corrida->nombre}} {{$corrida->hora_salida}}</option>
                    @endforeach
                @else
                <option value="">Sin corridas</option>
                  @endif
                  </select>
                </div>
              </div>
              <input type="text" name="fecha_ida_redondo" class="datepicker" value="" placeholder="Fecha de ida" />
              <div class="booking-form-time">
                <label>Corrida de regreso</label>
              </div>

              <div class="booking-form-hour">
                <div class="select-wrapper">
                  <i class="fa fa-angle-down"></i>
                  <select required="required" name="corrida_regreso_redondo" id="servicio">
                    <option value="">-- Seleccione una corrida --</option>
                    @if (count($corridas) > 0)
                        @foreach ($corridas as $corrida)
                    <option value="{{$corrida->id}}">{{$corrida->nombre}} {{$corrida->hora_salida}}</option>
                    @endforeach
                @else
                <option value="">Sin corridas</option>
                  @endif
                  </select>
                </div>
              </div>
              <input type="text" name="fecha_regreso_redondo" class="datepicker" value="" placeholder="Fecha de ida" />
              <button type="button" class="btn-reservar-redondo">
                <span>Reserve ahora</span>
              </button>

            <!-- END .booking-form-1 -->
            </form>

          <!-- END #tab-hourly -->
        </div>

        <!-- END #booking-tabs -->
        </div>

      <!-- END .header-booking-form-wrapper -->
      </div>

    <!-- END .large-header -->
    </div>
    <div class="mapa-wrapper-home" id="googlemaps" style="margin-top: 150px;">

      <div class="mapa-wrapper-home" id="googlemaps2">

      </div>

    </div>

  <!-- END .large-header-wrapper -->
  </div>
  <!-- BEGIN .content-wrapper-outer -->
  <section class="content-wrapper-outer content-wrapper clearfix our-fleet-sections" style="margin-top:50px;">

    <h3 class="center-title">Titulo por asignar 1</h3>
    <div class="title-block2"></div>

    <p class="fleet-intro-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  </p>

    <!-- BEGIN .fleet-block-wrapper -->
    <div class="owl-carousel1 fleet-block-wrapper">

      <!-- BEGIN .fleet-block -->
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>
      <div class="fleet-block">
        <div class="fleet-block-image">
          <a href="our-fleet-single.html"><img src="http://via.placeholder.com/600x380" alt="" /></a>
        </div>
        <div class="fleet-block-content">
          <div class="fleet-price">Etiqueta</div>
          <h4><a href="fleet-charter-single.html">Texto por asignar</a></h4>
          <div class="title-block3"></div>
          <ul class="list-style4">
            <li>Texto por asignar</li>
            <li>Texto por asignar</li>
          </ul>
        </div>
      <!-- END .fleet-block -->
      </div>

    <!-- END .fleet-block-wrapper -->
    </div>

  <!-- END .content-wrapper-outer -->
  </section>

  <!-- BEGIN .content-wrapper-outer -->
  <section class=".content-wrapper-outer clearfix call-to-action-1-section">

    <div class="call-to-action-1-section-inner">

      <h3>Contamos con evio Ocurre en nuestros destinos de ciudades, el tiempo de llegada de tu paquete es menor a 24 hrs.</h3>
      <a href="service-rates.html" class="button0">Texto por asignar</a>

    </div>

  <!-- END .content-wrapper-outer -->
  </section>

  <!-- BEGIN .content-wrapper-outer -->
  <section class="content-wrapper-outer home-icons-outer-wrapper-2">

    <!-- BEGIN .clearfix -->
    <div class="clearfix">

      <!-- BEGIN .qns-one-third -->
      <div class="qns-one-half home-icon-wrapper-2">
        <div class="qns-home-icon"><i class="fa fa-calendar-check-o"></i></div>
        <div class="home-icon-inner">
          <h4>Salidas diarias</h4>
          <div class="title-block3"></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
      <!-- END .qns-one-third -->
      </div>

      <!-- BEGIN .qns-one-third -->
      <div class="qns-one-half home-icon-wrapper-2 qns-last">
        <div class="qns-home-icon"><i class="fa fa-star"></i></div>
        <div class="home-icon-inner">
          <h4>Personal calificado</h4>
          <div class="title-block3"></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
      <!-- END .qns-one-third -->
      </div>

      <!-- BEGIN .qns-one-third -->
      <div class="qns-one-half home-icon-wrapper-2">
        <div class="qns-home-icon"><i class="fa fa-car"></i></div>
        <div class="home-icon-inner">
          <h4>Servicio personalizado</h4>
          <div class="title-block3"></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
      <!-- END .qns-one-third -->
      </div>

      <!-- BEGIN .qns-one-third -->
      <div class="qns-one-half home-icon-wrapper-2 qns-last">
        <div class="qns-home-icon"><i class="fa fa-cc-visa"></i></div>
        <div class="home-icon-inner">
          <h4>Costos accesibles</h4>
          <div class="title-block3"></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
      <!-- END .qns-one-third -->
      </div>

    <!-- END .clearfix -->
    </div>

  <!-- END .content-wrapper-outer -->
  </section>

  <!-- BEGIN .clearfix -->
  <section class="clearfix">

    <!-- BEGIN .about-us-block -->
    <div class="about-us-block about-us-block-1 col-xs-6" style="margin-bottom: 0px;">

      <h3>VUZ</h3>
      <div class="title-block4"></div>
      <p style="margin: 0 0 10px 0;">Destacamos como la plataforma que cuenta con la mejor red de autobuses, conductores calificados para que tu viaje sea seguro y puedas llegar sin preocupación alguna a tu destino.</p>
      <p style="margin: 0 0 10px 0;">Una de nuestras prioridades, es mantener un costo accesible, para que puedas disfrutar de nuestros diversos destinos y no te pierdas de las maravillas que hay en cada uno de ellos.</p>
      <p style="margin: 0 0 10px 0;">Si lo que necesitas es viajar facil, a bajo costo, con seguridad y un buen aperitivo, vuz es la opción.</p>
      <!--a href="about-us.html" class="button0">Leer más</a-->

    <!-- END .about-us-block -->
    </div>

    <div class="video-wrapper video-wrapper-home col-xs-6">

      <div class="video-play">
        <a href="https://www.youtube.com/watch?v=Uv5ZRiAreHA" data-gal="prettyPhoto"><i class="fa fa-play"></i></a>
      </div>

    </div>

  <!-- END .clearfix -->
  </section>

  <!-- BEGIN .call-to-action-2-section -->
  <section class="clearfix call-to-action-2-section">

    <div class="call-to-action-2-section-inner">

      <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
      <div class="title-block5"></div>
      <p>Llamanos al 9513310578 o envianos un email a vuz@ejemplo.com</p>
      <a href="booking.html" class="button0">Reservaciónes online</a>

    </div>

  <!-- END .call-to-action-2-section -->
  </section>

  <!-- BEGIN .content-wrapper-outer -->
  <section class="content-wrapper-outer content-wrapper clearfix testimonial-gallery-section">

    <!-- BEGIN .qns-one-half -->
    <div class="qns-one-half col-xs-12 col-sm-12">

      <h3 class="center-title">Galeria</h3>
      <div class="title-block2"></div>

      <!-- BEGIN .home-photos-wrapper -->
      <div class="home-photos-wrapper clearfix">
        <div class="home-photo"><a href="http://via.placeholder.com/250x235" data-gal="prettyPhoto"><img src="http://via.placeholder.com/250x235" alt="" /></a></div>
        <div class="home-photo"><a href="http://via.placeholder.com/250x235" data-gal="prettyPhoto"><img src="http://via.placeholder.com/250x235" alt="" /></a></div>
        <div class="home-photo"><a href="http://via.placeholder.com/250x235" data-gal="prettyPhoto"><img src="http://via.placeholder.com/250x235" alt="" /></a></div>
        <div class="home-photo"><a href="http://via.placeholder.com/250x235" data-gal="prettyPhoto"><img src="http://via.placeholder.com/250x235" alt="" /></a></div>
        <div class="home-photo"><a href="http://via.placeholder.com/250x235" data-gal="prettyPhoto"><img src="http://via.placeholder.com/250x235" alt="" /></a></div>
        <div class="home-photo"><a href="http://via.placeholder.com/250x235" data-gal="prettyPhoto"><img src="http://via.placeholder.com/250x235" alt="" /></a></div>
      <!-- END .home-photos-wrapper -->
      </div>

    <!-- END .qns-one-half -->
    </div>

    <!-- BEGIN .qns-one-half -->
    <!-- BEGIN .qns-one-half -->
    <div class="qns-one-half qns-last">

      <h3 class="center-title"><img src="{{url('page/assets/images/v1.png')}}" style="width:120px;" alt=""></h3>
      <!--div class="title-block2"></div-->

      <!-- BEGIN .testimonial-wrapper-outer -->
      <div class="testimonial-wrapper-outer">

        <!-- BEGIN .testimonial-list-wrapper -->
        <div class="testimonial-list-wrapper owl-carousel3">

          <!-- BEGIN .testimonial-wrapper -->
          <div class="testimonial-wrapper">
            <p><span class="qns-open-quote">“</span>CONOCE NUEVOS LUGARES DE LA MANERA MÁS FÁCIL<span class="qns-close-quote">”</span></p>
            <div class="testimonial-image"><img src="http://via.placeholder.com/80x80" alt="" /></div>
            <div class="testimonial-author"><p>Lugar: por asignar</p></div>
          <!-- END .testimonial-wrapper -->
          </div>

          <!-- BEGIN .testimonial-wrapper -->
          <div class="testimonial-wrapper">
            <p><span class="qns-open-quote">“</span>NUESTROS COSTOS, SERVICIO Y DIVERSAS RUTAS, TE ASOMBRARAN!<span class="qns-close-quote">”</span></p>
            <div class="testimonial-image"><img src="http://via.placeholder.com/80x80" alt="" /></div>
            <div class="testimonial-author"><p>Lugar: por asignar</p></div>
          <!-- END .testimonial-wrapper -->
          </div>
        <!-- END .testimonial-list-wrapper -->
        </div>

      <!-- END .testimonial-wrapper-outer -->
      </div>

    <!-- END .qns-one-half -->
    </div>
  <!-- END .content-wrapper-outer -->
  </section>

  <!-- BEGIN .footer -->
  <footer class="footer">

    <!-- BEGIN .footer-inner -->
    <div class="footer-inner clearfix">

      <!-- BEGIN .one-half -->
      <div class="one-fourth">

        <h5>Legal</h5>
        <div class="title-block6"></div>
        <a href="#">Aviso de privacidad</a> <br><br><br>
        <a href="#">Politicas de viaje</a> <br><br><br>
        <a href="#">Terminos y condiciones</a> <br><br><br>
        <!-- BEGIN .newsletter-form -->


      <!-- END .one-fourth -->
      </div>
      <!-- BEGIN .one-half -->
      <div class="one-fourth">

        <h5>Más de VUZ</h5>
        <div class="title-block6"></div>
        <a href="#">Renta de autobuses</a> <br><br><br>
        <a href="#">Formas de pago</a> <br><br><br>
        <a href="#">Ubicación de puntos de venta</a> <br><br><br>
        <!-- BEGIN .newsletter-form -->
      <!-- END .one-fourth -->
      </div>
      <!-- BEGIN .one-fourth -->
      <div class="one-fourth">
        <h5>Envianos un mensaje!</h5>
        <div class="title-block6"></div>
        <!-- BEGIN .newsletter-form -->
        <form action="#" class="newsletter-form" method="post">
          <input type="text" name="your-name" value="" />
          <button type="submit">
            enviar <i class="fa fa-envelope"></i>
          </button>
        <!-- END .newsletter-form -->
        </form>
      <!-- END .one-fourth -->
      </div>
      <!-- BEGIN .one-fourth -->
      <div class="one-fourth">
        <h5>Detalles de contacto</h5>
        <div class="title-block6"></div>
        <ul class="contact-widget">
          <li class="cw-address">Direccón</li>
          <li class="cw-phone">Teléfono<span>Horario</span></li>
          <li class="cw-cell">vuz@example.com<span>Hora de servicio</span></li>
        </ul>
      <!-- END .one-fourth -->
      </div>
      <div class="clearboth"></div>
      <!-- END .footer-bottom -->
      </div>
      <!-- BEGIN .footer-bottom -->
      <div class="footer-bottom">
        <div class="footer-social-icons-wrapper">
          <a href="#"><i class="fa fa-facebook"></i></a>
          <a href="#"><i class="fa fa-twitter"></i></a>
          <a href="#"><i class="fa fa-instagram"></i></a>
          <a href="#"><i class="fa fa-youtube-play"></i></a>
        </div>
        <p class="footer-message">&copy; 2018 jasbit.com</p>
    <!-- END .footer-inner -->
     </div>
  <!-- END .footer -->
  </footer>
<!-- END .outer-wrapper -->
</div>
@endsection
@section("script")
<!--script-->
<style>
#googlemaps {
  height: 600px;
  width: 100%;
  position:absolute;
  top: 0;
  left: 0;
  z-index: 0; /* Set z-index to 0 as it will be on a layer below the contact form */
}
#googlemaps2 {
  height: 600px;
  width: 100%;
  position:absolute;
  top: 0;
  left: 0;
  z-index: 0; /* Set z-index to 0 as it will be on a layer below the contact form */
}
#contactform {
  position: relative;
  z-index: 1; /* The z-index should be higher than Google Maps */
  width: 300px;
  margin: 60px auto 0;
  padding: 10px;
  background: black;
  height: auto;
  opacity: .45; /* Set the opacity for a slightly transparent Google Form */
  color: white;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiJFor-Eqrhp4L00gJxQnHJieLe7LeXLQ&sensor=false" ></script>
<script>

var position2 = [23.568710, -102.761093];

function showGoogleMaps2() {

  var latLng2 = new google.maps.LatLng(position2[0], position2[1]);

  var mapOptions = {
      zoom: 6, // initialize zoom level - the max value is 21
      streetViewControl: false, // hide the yellow Street View pegman
      scaleControl: true, // allow users to zoom the Google Map
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: latLng2,
      disableDefaultUI: true,
      styles: [
          {
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#242f3e"
          }
          ]
          },
          {
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#746855"
          }
          ]
          },
          {
          "elementType": "labels.text.stroke",
          "stylers": [
          {
          "color": "#242f3e"
          }
          ]
          },
          {
          "featureType": "administrative",
          "elementType": "geometry",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "administrative.land_parcel",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "administrative.locality",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#d59563"
          }
          ]
          },
          {
          "featureType": "administrative.neighborhood",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "poi",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "poi",
          "elementType": "labels.text",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#d59563"
          }
          ]
          },
          {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#263c3f"
          }
          ]
          },
          {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#6b9a76"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#38414e"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "geometry.stroke",
          "stylers": [
          {
          "color": "#212a37"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "labels",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#9ca5b3"
          }
          ]
          },
          {
          "featureType": "road.arterial",
          "elementType": "labels",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#746855"
          }
          ]
          },
          {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
          {
          "color": "#1f2835"
          }
          ]
          },
          {
          "featureType": "road.highway",
          "elementType": "labels",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#f3d19c"
          }
          ]
          },
          {
          "featureType": "road.local",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "transit",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "transit",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#2f3948"
          }
          ]
          },
          {
          "featureType": "transit.station",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#d59563"
          }
          ]
          },
          {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#17263c"
          }
          ]
          },
          {
          "featureType": "water",
          "elementType": "labels.text",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#515c6d"
          }
          ]
          },
          {
          "featureType": "water",
          "elementType": "labels.text.stroke",
          "stylers": [
          {
          "color": "#17263c"
          }
          ]
          }
          ]
  };

  map = new google.maps.Map(document.getElementById('googlemaps2'),mapOptions);
}

google.maps.event.addDomListener(window, 'load', showGoogleMaps2);

</script>
<script>
    const corridas = JSON.parse('@json($corridas)');
    function getCorrida(corrida_id) {
      var c;
      corridas.forEach(function(corrida, index){
        if(corrida.id == corrida_id) {
          c = corrida;
        }
      });
      return c;
    }
    var position;
    function showGoogleMaps() {

      // var coordenadas = new Array(3);
      // console.log(corridas)
      $(document).on('change', '#servicio', function(event) {
        coordenadas = $(this).val();
        console.log(coordenadas);
      //  console.log(coordenadas);
      var crrds = getCorrida(coordenadas);
      console.log(getCorrida());
        /* posición de origen y de destino */
      var position = [crrds.lat_a,crrds.lng_a,crrds.lat_b,crrds.lng_b];
      /*latitud y longitud */
      var directionsService = new google.maps.DirectionsService();
      var directionsDisplay = new google.maps.DirectionsRenderer();
      var latLng_origen = new google.maps.LatLng(position[0], position[1]);
      var latLng_destino = new google.maps.LatLng(position[2], position[3]);
      /* opciones para los mapas */
      var mapOptions = {
          zoom: 4, // zoom del mapa
          streetViewControl: false, // desaparición del muñeco amarillo de google maps
          scaleControl: true, // permite hacer zoom en el mapa de google
          mapTypeId: google.maps.MapTypeId.ROADMAP, // tipo de mapa (2D, satellite, hybrid)
          center: latLng_origen, // recoge la latitud y la longitud de la variable
          disableDefaultUI: true,
          styles: [
          {
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#242f3e"
          }
          ]
          },
          {
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#746855"
          }
          ]
          },
          {
          "elementType": "labels.text.stroke",
          "stylers": [
          {
          "color": "#242f3e"
          }
          ]
          },
          {
          "featureType": "administrative",
          "elementType": "geometry",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "administrative.land_parcel",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "administrative.locality",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#d59563"
          }
          ]
          },
          {
          "featureType": "administrative.neighborhood",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "poi",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "poi",
          "elementType": "labels.text",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#d59563"
          }
          ]
          },
          {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#263c3f"
          }
          ]
          },
          {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#6b9a76"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#38414e"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "geometry.stroke",
          "stylers": [
          {
          "color": "#212a37"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "labels",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#9ca5b3"
          }
          ]
          },
          {
          "featureType": "road.arterial",
          "elementType": "labels",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#746855"
          }
          ]
          },
          {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
          {
          "color": "#1f2835"
          }
          ]
          },
          {
          "featureType": "road.highway",
          "elementType": "labels",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#f3d19c"
          }
          ]
          },
          {
          "featureType": "road.local",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "transit",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "transit",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#2f3948"
          }
          ]
          },
          {
          "featureType": "transit.station",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#d59563"
          }
          ]
          },
          {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
          {
          "color": "#17263c"
          }
          ]
          },
          {
          "featureType": "water",
          "elementType": "labels.text",
          "stylers": [
          {
          "visibility": "off"
          }
          ]
          },
          {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
          {
          "color": "#515c6d"
          }
          ]
          },
          {
          "featureType": "water",
          "elementType": "labels.text.stroke",
          "stylers": [
          {
          "color": "#17263c"
          }
          ]
          }
          ]

      };
      var  map = new google.maps.Map(document.getElementById('googlemaps'),mapOptions);
      /* marker de vuz ORIGEN */
      marker_origen = new google.maps.Marker({
            position: latLng_origen, // posición del marker y recoge la latitud y la longitud de la variable
            icon: '{{url("../page/assets/images/vuz_marker_origen_v2.png")}}',
            map: map, // asigna el mapa
            draggable: false, // para arrastrar el marker
            animation: google.maps.Animation.DROP //animación del marker (bouce)
      });
      /* marker de vuz DESTINO */
      marker_destino = new google.maps.Marker({
            position: latLng_destino, // posición del marker y recoge la latitud y la longitud de la variable
            icon: '{{url("../page/assets/images/vuz_marker_destino_v2.png")}}',
            map: map, // asigna el mapa
            draggable: false, // para arrastrar el marker
            animation: google.maps.Animation.DROP //animación del marker (bouce)
      });
      var objConfigDR = {
        map: map,
        suppressMarkers: true
      }
      var objConfigDS = {
        origin: latLng_origen,
        destination: latLng_destino,
        travelMode: google.maps.TravelMode.DRIVING
      }
      var ds = new google.maps.DirectionsService();
      var dr = new google.maps.DirectionsRenderer( objConfigDR );
      ds.route(objConfigDS, funRutear);
      function funRutear( resultados , status){
        //muestra ruta entrea origen y destino
        if (status == 'OK') {
          dr.setDirections(resultados);
        }else{
          alert('Error '+status);
        }
      }
      });
}
google.maps.event.addDomListener(window, 'load', showGoogleMaps);
</script>
<script type="text/javascript">
  var redondo = false;
  $(document).ready(function() {
    $('#btn-redondo').click(function() {
      redondo = true;
      console.log('->redondo', redondo)
      //$('#btn-reservar').attr('disabled','disabled')
    });

    $('#btn-sencillo').click(function() {
      redondo = false;
      console.log('->redondo', redondo)

    });

    $('.btn-reservar-sencillo').click(function() {
      console.log('empeszara reservar sencillo', redondo)
      var band = true;
      var ida = $('select[name=corrida_sencillo').val();
      var ida_fecha = $('input[name=ida_fecha').val();


      if(ida.length  < 1){
        band = false;
      }

      console.log('/reservar?redondo='+redondo+'&ida='+ida+'&ida_fecha='+ida_fecha)
      if(band == true) {
        location.href = '/reservar?redondo='+redondo+'&ida='+ida+'&ida_fecha='+ida_fecha;
      } else {
        alert('selecciona una corrida')
      }

    });

    $('.btn-reservar-redondo').click(function() {
      console.log('empeszara reservar sencillo', redondo)
      var band = true;
      var ida = $('select[name=corrida_ida_redondo').val();
      var ida_fecha = $('input[name=fecha_ida_redondo').val();
      var regreso = $('select[name=corrida_regreso_redondo').val();
      var regreso_fecha = $('input[name=fecha_regreso_redondo').val();
      if(ida.length  < 1){
        band = false;
        alert('selecciona una corrida de ida')
      }
      if(regreso.length  < 1){
        band = false;
        alert('selecciona una corrida de regreso')
      }
      if(band == true) {
        if(ida == regreso) {
          band = false;
          alert("tu corridas son idénticas, escoge bien tus corridas")
        }
      }

      console.log('/reservar?redondo='+redondo+'&ida='+ida+'&ida_fecha='+ida_fecha+'&regreso='+regreso+'&regreso_fecha='+regreso_fecha)
      if(band == true) {
        location.href = '/reservar?redondo='+redondo+'&ida='+ida+'&ida_fecha='+ida_fecha+'&regreso='+regreso+'&regreso_fecha='+regreso_fecha;
      }

    });
  });
</script>
@endsection
