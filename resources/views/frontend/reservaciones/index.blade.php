@extends('frontend.app')

@section('head')
<link rel="stylesheet" href="{{asset('css/bus.css')}}" />
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.9/dist/sweetalert2.all.min.js" type="text/javascript"></script>
  <!-- <meta http-equiv="refresh" content="{{$configuracion->tiempo_espera_reservacion_plataforma * 60}}" /> -->
  <meta name="expiracion" content="10">
  <meta name="ida" content="{{Request::input('ida')}}">
  <meta name="ida_fecha" content="{{Request::input('ida_fecha')}}">
  <meta name="regreso" content="{{Request::input('regreso')}}">
  <meta name="regreso_fecha" content="{{Request::input('regreso_fecha')}}">
  <meta name="redondo" content="{{Request::input('redondo')}}">
  <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
@endsection

@section('content')
<section id="tabs">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 bus mt-3 ml-3">
        <h4>Corrida <small>{{$corrida->nombre}}</small></h4>


        <nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-ida-tab" data-toggle="tab" href="#nav-ida" role="tab" aria-controls="nav-ida" aria-selected="true">
              IDA
            </a>
            <a class="nav-item nav-link" id="nav-regreso-tab" data-toggle="tab" href="#nav-regreso" role="tab" aria-controls="nav-regreso" aria-selected="false">
              REGRESO
            </a>
						<a class="nav-item nav-link disabled count-people" data-toggle="tab"role="tab">
              <i class="fa fa-user"></i> <span id="numb" class="badge badge-light">0</span>
            </a>
					</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-ida" role="tabpanel" aria-labelledby="nav-ida-tab">

            <!-- aqui template generado por una peticion post jqueyr -->

					</div>
					<div class="tab-pane fade" id="nav-regreso" role="tabpanel" aria-labelledby="nav-regreso-tab">
						<!-- aqui template generado por una peticion post jqueyr -->
					</div>
				</div>
        <div class="asientos_estados_container mt-3">
          <table class="table letras_blanca">
              <tr>
                  <td><span class="circulo circulo_disponible"></span> Disponible</td>
                  <td><span class="circulo circulo_apartado"></span> Apartado</td>
              </tr>
          </table>
        </div>
			</div>
		</div>

	</div>
</section>

<section class="container">
  <div class="row">
    <div class="col py-3">
      <h4>Información del cliente</h4>
    </div>
  </div>

  <div class="row">
    <div class="col-sm">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">
            <span class="fa fa-user" aria-hidden="true"></span>
          </span>
        </div>
        <input type="text" name="nombre" class="form-control" placeholder="Escribe tu nombre" aria-label="Escribe tu nombre" aria-describedby="basic-addon1">
      </div>
    </div>
    <div class="col-sm">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">
            <span class="fa fa-envelope" aria-hidden="true"></span>
          </span>
        </div>
        <input type="email" name="email" class="form-control" placeholder="Escribe tu email" aria-label="Escribe tu email" aria-describedby="basic-addon1">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <button type="button" class="btn btn-primary btn-block" id="btn-aceptar" onclick="hacerReservacion()">Reservar</button>
    </div>
  </div>
</section>
<script type="text/javascript" src="{{asset('js/bus.js')}}"></script>
@endsection

@section('javascript')
<script type="text/javascript">

const csrf = document.head.querySelector('meta[name="csrf-token"]').content;
const expiracion = document.head.querySelector('meta[name="expiracion"]').content;
const redondo = document.head.querySelector('meta[name="redondo"]').content;
const ida = document.head.querySelector('meta[name="ida"]').content;
const ida_fecha = document.head.querySelector('meta[name="ida_fecha"]').content;
var regreso = null;
var regreso_fecha = null;
console.log('viaje redondo: ' + redondo)
if(redondo == 'true') {
  regreso = document.head.querySelector('meta[name="regreso"]').content;
  regreso_fecha = document.head.querySelector('meta[name="regreso_fecha"]').content;
  $('#nav-regreso-tab').show()
}

var reservaciones = {
  ida: [],
  regreso: []
};

function reservarAsientoIda(obj){
  var reservacion = {
    obj,
    asiento_id: $(obj).attr('asiento'),
    corrida_id: $(obj).attr('corrida'),
    fecha: ida_fecha
  };
  // para seleccionar un asiento disponible y reservarlo
  if($(reservacion.obj).hasClass('asiento_el_h')) {
    marcarComoSeleccionado(reservacion, function(reservacion) {
      if(reservacion) {
        reservaciones.ida.push(reservacion);
      } else {
        //ocurrio un error
        getBusRender(ida, ida_fecha, 'nav-ida','reservarAsientoIda');
      }
      actualizarVistaPersonasReservaciones()
    });
  }
  // para quitar un asiento ya seleccionado que quiere desmarcar
  else if($(reservacion.obj).hasClass('seleccionado_h')) {
    desmarcarComoSeleccionado(reservacion, 'ida');
  }

}

function reservarAsientoRegreso(obj){
  var reservacion = {
    obj,
    asiento_id: $(obj).attr('asiento'),
    corrida_id: $(obj).attr('corrida'),
    fecha: regreso_fecha
  };
  // para seleccionar un asiento disponible y reservarlo
  if($(reservacion.obj).hasClass('asiento_el_h')) {
    marcarComoSeleccionado(reservacion, function(reservacion) {
      if(reservacion) {
        reservaciones.regreso.push(reservacion);
      } else {
        //ocurrio un error
        getBusRender(regreso, regreso_fecha, 'nav-regreso','reservarAsientoRegreso');
      }
      actualizarVistaPersonasReservaciones()
    });
  }
  // para quitar un asiento ya seleccionado que quiere desmarcar
  else if($(reservacion.obj).hasClass('seleccionado_h')) {
    desmarcarComoSeleccionado(reservacion, 'regreso');
  }

}

function desmarcarComoSeleccionado(reservacion, pila) {
  var band = false;
  var clave = ''
    reservaciones[pila].forEach(function(item,index) {
      if(item.asiento_id ==  reservacion.asiento_id) {
        clave = item.clave;
        reservaciones.ida.splice(index,1);
        band =true;
      }
    });
    if(band) {
      $(reservacion.obj).addClass('asiento_el_h');
      $(reservacion.obj).removeClass('seleccionado_h');
    /*  $.ajax({
        type: "POST",
        url: '/api/v1/reservacion/cancelar/'+clave,
        data: reservacion,
        success: function (response) {
          actualizarVistaPersonasReservaciones();
        },
        error:function (error) {
          swal('Error',error.response.data.message, 'error');
          getBusRender(ida, ida_fecha, 'nav-ida','reservarAsientoIda');
          __callback(null)
        }
      });
      */

      axios.post('/api/v1/reservacion/cancelar/'+clave, reservacion)
      .then(function (response) {
        actualizarVistaPersonasReservaciones();
      })
      .catch(function (error) {
        swal('Error',error.response.data.message, 'error');
        getBusRender(ida, ida_fecha, 'nav-ida','reservarAsientoIda');
        __callback(null)
      });

    }

  console.log('reservaciones', reservaciones)
}

function getBusRender(corrida_id, fecha, id_div, function_name, __callback) {
  const data =  {
    _token: csrf,
    fecha,
    corrida_id,
    function_name
  };
  $.ajax({
    type: "POST",
    url: '/reservar/renderBus',
    data: data,
    success: function(response) {
      // console.log(response)
      $('#'+ id_div).html(response);
    },
    error: function(error) {
      // console.log((error));
      swal('Error',error.response.data.message, 'error');
      if(__callback)
        __callback(false)
    }
  });
  /*
  const res = axios.post('/reservar/renderBus/', {
    _token: csrf,
    fecha,
    corrida_id,
    function_name
  })
  .then((response) => {
    // handle success
    $('#'+ id_div).html(response.data);
    if(__callback)
      __callback(true)
  })
  .catch((error) => {
    console.log((error));
    swal('Error',error.response.data.message, 'error');
    if(__callback)
      __callback(false)
  });*/
}

function marcarComoSeleccionado(reservacion, __callback) {
      console.log('marcando como seleccionado')
      $(reservacion.obj).removeClass('asiento_el_h');
      $(reservacion.obj).addClass('cargando_h');
      /*$.ajax({
        type: "POST",
        url: '/api/v1/reservacion/disponibilidad',
        data: reservacion,
        success: function (response) {
          // handle success
          console.log(response.data);
          $(reservacion.obj).removeClass('cargando_h');
          $(reservacion.obj).addClass('seleccionado_h')
          __callback(response.data);
        },
        error: function (error) {
          swal('Error',error.response.data.message, 'error');
          __callback(null)
        }
      });*/

      axios.post('/api/v1/reservacion/disponibilidad', reservacion)
      .then(function (response) {
        // handle success
        console.log(response.data);
        $(reservacion.obj).removeClass('cargando_h');
        $(reservacion.obj).addClass('seleccionado_h')
        __callback(response.data);
      })
      .catch(function (error) {
        swal('Error',error.response.data.message, 'error');
        __callback(null)
      });

}

function actualizarVistaPersonasReservaciones() {
  $('#numb').text(reservaciones.ida.length);
}

function reAsignarAsientosIdaSeleccionados() {
  console.log('re-asignando asientos seleccionados de ida');
  reservaciones.ida.forEach(function(reservacion, index) {
    console.log('r -> ',reservacion);
    $('#nav-ida > table > tbody').find('td').each(function() {
      var asiento = ($(this).children('div').attr('asiento'));
      var corrida = ($(this).children('div').attr('corrida'));
      // console.log(reservacion.asiento_id + "==" +asiento + "&&" + reservacion.corrida_id + "==" + corrida)
      if(parseInt(reservacion.asiento_id) == parseInt(asiento) && parseInt(reservacion.corrida_id) == parseInt(parseInt(corrida))) {
        // console.log('-------------->>>>>>>>>>> encontrado')
        $($(this).children('div')).removeClass('apartado_h');
        $($(this).children('div')).addClass('seleccionado_h');
      }
    })
  });

}

function reAsignarAsientosRegresoSeleccionados() {
  console.log('re-asignando asientos seleccionados de regreso');
  reservaciones.regreso.forEach(function(reservacion, index) {
    console.log('r -> ',reservacion);
    $('#nav-regreso > table > tbody').find('td').each(function() {
      var asiento = ($(this).children('div').attr('asiento'));
      var corrida = ($(this).children('div').attr('corrida'));
      // console.log(reservacion.asiento_id + "==" +asiento + "&&" + reservacion.corrida_id + "==" + corrida)
      if(parseInt(reservacion.asiento_id) == parseInt(asiento) && parseInt(reservacion.corrida_id) == parseInt(parseInt(corrida))) {
        // console.log('-------------->>>>>>>>>>> encontrado')
        $($(this).children('div')).removeClass('apartado_h');
        $($(this).children('div')).addClass('seleccionado_h');
      }
    })
  });

}

function hacerReservacion(){
  const nombre = $('input[name="nombre"]').val();
  const email = $('input[name="email"]').val();
  var band = true;
  //validando
  if(nombre.length == 0){ band = false; swal('Error', 'Escribe tu nombre', 'warning'); }
  if(email.length == 0){ band = false; swal('Error', 'Escribe tu email', 'warning'); }
  if(reservaciones.ida.length == 0) { band = false; swal('Error', 'Selecciona un asiento para reservar', 'warning'); }
  if(redondo == 'true'){
    if(reservaciones.ida.length != reservaciones.regreso.length) {
      band = false;
      if(reservaciones.ida.length > reservaciones.regreso.length) {
        const asientos = reservaciones.ida.length - reservaciones.regreso.length;
        swal('Error', 'Te falta seleccionar ' + asientos + ' asiento(s) de regreso', 'warning');
      }
      if(reservaciones.ida.length < reservaciones.regreso.length) {
        const asientos = reservaciones.regreso.length - reservaciones.ida.length;
        swal('Error', 'Te falta seleccionar ' + asientos + ' asiento(s) de ida', 'warning');
      }
    }
  }
  if(band == true) {
    $('#btn-aceptar').attr('disabled','disabled')
    axios.post('/api/v1/reservacion/plataforma', {reservaciones,redondo, nombre, email})
    .then(function (response) {
      console.log(response);
      $('#btn-aceptar').removeAttr('disabled')
      ///pagoconpaypal/{clave}
      location.href = '/pagoconpaypal/'+response.data.clave;
    }, function(error) {
      swal('Error', error.response.data.message, 'error');
      $('#btn-aceptar').removeAttr('disabled')
    })
  }
}

$(document).ready(() => {
      console.log('jquery ready!!!')
      getBusRender(ida, ida_fecha, 'nav-ida','reservarAsientoIda');

      $('#nav-ida-tab').click(function() {
        console.log('#nav-ida-tab" clicked!')
        getBusRender(ida, ida_fecha, 'nav-ida','reservarAsientoIda', function() {
          reAsignarAsientosIdaSeleccionados();
        });
      });

      $('#nav-regreso-tab').click(function() {
        console.log('#nav-regreso-tab" clicked!')
        getBusRender(regreso, regreso_fecha, 'nav-regreso','reservarAsientoRegreso', function() {
          reAsignarAsientosRegresoSeleccionados();
        });
      });
      setTimeout(() => {
        alert("Tu sesion exipiró");
        location.href = '/'
      }, expiracion * 60000);
});

</script>
@endsection
