@extends('frontend.app')
@section('content')
<br>
<div class="container">
  <div class="row">
    <div class="col-sm-8" style="margin-bottom:30px">
      <div class="card">
         <div class="card-header bg-warning">
            Desglose
            <strong></strong>
            <span class="float-right"> <strong>Estatus:</strong> Pendiente de pago</span>
         </div>
         <div class="card-body">
            <div class="row mb-4">
               <div class="col-sm-6">
                  <h6 c lass="mb-3">Vendedor:</h6>
                  <div>
                     <strong>Jasbit</strong>
                  </div>
                  <!--<div>Madalinskiego 8</div>-->
                  <!--<div>71-101 Szczecin, Poland</div>-->
                  <div>Correo: jasbit.com@gmail.com</div>
                  <div>Telefono: +52 951 316 4836</div>
                  <br>
                  <br>
                  <div><strong>Código de reservación: </strong>{{ $clave }}</div>
                  <div>Fecha: {{ Carbon\Carbon::parse($reservacion->created_at)->format('d-m-Y') }}</div>
               </div>
               <div class="col-sm-6">
                  <h6 class="mb-3">Cliente:</h6>
                  <div>
                     <strong>{{ $reservacion->cliente }}</strong>
                  </div>
                  <!--<div>Attn: Daniel Marek</div>
                     <div>43-190 Mikolow, Poland</div>
                     <div>Email: marek@daniel.com</div>-->
                  @if($reservacion->phone != null)
                  <div>Telefono: {{ $reservacion->phone }}</div>
                  @endif
               </div>
            </div>
            <div class="table-responsive-sm">
               <table class="table table-striped">
                  <thead class="bg-warning">
                     <tr>
                        <th class="center">#</th>
                        <th>Corrida</th>
                        <th>Fecha de Salida</th>
                        <th class="right">Costo</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach ($reservaciones as $key => $r)
                     <tr>
                       <td class="center">{{++$key}}</td>
                       <td class="left">{{ $r->corrida->nombre }}</td>
                       <td class="left strong">{{ Carbon\Carbon::parse($r->fecha_reservacion)->format('d-m-Y') }}</td>
                       <td class="right">${{$r->costo_boleto}} MXN</td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
            <div class="row">
               <div class="col-lg-4 col-sm-5">
               </div>
               <div class="col-lg-4 col-sm-5 ml-auto">
                  <table class="table table-clear">
                     <tbody>
                        <tr>
                           <td class="left">
                              <strong>Total</strong>
                           </td>
                           <td class="right">
                              <strong>${{ $suma_boletos}} MXN</strong>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-4">
     <div class="container">
       @if ($message = Session::get('success'))
       <div class="container">
           <span onclick="this.parentElement.style.display='none'"
                   class="btn btn-warning display-topright">&times;</span>
           <p>{!! $message !!}</p>
       </div>
       <?php Session::forget('success');?>
       @endif
       @if ($message = Session::get('error'))
       <div class="container">
           <span onclick="this.parentElement.style.display='none'"
                   class="btn btn-warning display-topright">&times;</span>
           <p>{!! $message !!}</p>
       </div>
       <?php Session::forget('error');?>
       @endif
       <div class="card">
       <div class="card-header bg-warning">
             Resumen
             <!--<a class="nav-link disabled">Código de reservacion: {{ $clave }}</a>-->
       </div>
       <div class="card-body">
         <h5 class="card-title">Cliente : {{ $reservacion->cliente}}</h5>
         <p class="card-text">{{ $cantidad_boletos}} Boleto(s) {{ $reservacion->redondo == "1" ? "Redondo(s)" : "Sencillo(s)"}}</p>
         <p class="card-text text-right">Total a pagar : <strong>${{ $suma_boletos }} MXN</strong></p>
         <form class="container-fluid" method="POST" id="payment-form"  action="{!! URL::to('paypal')!!}">
             {{ csrf_field() }}
             <input type="hidden" name="clave_global" value="{{ $clave }}">
             <button class="btn btn-primary btn-block">Pagar con PayPal</button></p>
           </form>
           <img src="{{url('/images/paypal.png')}}" class="img-fluid" alt="Responsive image">
       </div>
     </div>
     </div>
   </div>
  </div>
</div>

@endsection
