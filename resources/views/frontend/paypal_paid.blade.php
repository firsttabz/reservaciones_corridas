@extends('frontend.app')
@section('content')
<br>
<div class="container">
   <div class="col-12">
     <div class="container">
       @if($reservacion->estatus == 'pagado')
       <div class="card">
       <div class="card-header bg-warning">
            <strong>Estatus: </strong> pagado
             <!--<a class="nav-link disabled">Código de reservacion: {{ $clave }}</a>-->
       </div>
       <div class="card-body">
         <h5 class="card-title">Pago realizado por {{ $reservacion->cliente }}</h5>
         <p>Boleto(s) pagado(s) con el siguiente código de reservación <strong>"{{ $clave }}"</strong></p>
         <p>Revisa la bandeja de entrada de tu correo electrónico, hemos enviado los detalles de tu reservación.</p>
         <div class="text-right">
           <a class="btn btn-primary" href="{{url('/pageWeb')}}">Regresar a página de inicio</a>
         </div>
       </div>
     </div>

     <!--<div class="card">
     <div class="card-header bg-warning">
          <strong>Estatus: </strong> finalizado
     <div class="card-body">
       <h5 class="card-title">No hay reservaciones pendientes para el código <strong>"{{ $clave }}"</strong>.</h5>
       <div class="text-right">
         <a class="btn btn-primary" href="{{url('/pageWeb')}}">Regresar</a>
       </div>
     </div>
   </div>-->
     @endif
     </div>
   </div>
  </div>
</div>
@endsection
