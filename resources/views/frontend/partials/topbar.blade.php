<!-- Image and text -->
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="{{url('/')}}">
    <img src="{{url('page/assets/images/logo_vuz.png')}}" width="130" class="d-inline-block align-top" alt="Logo empresa {{env('EMPRESA_NOMBRE')}}">
  </a>
</nav>
