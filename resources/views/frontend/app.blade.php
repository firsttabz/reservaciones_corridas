<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    @include('frontend.partials.head')
    @yield('head')
</head>


<body class="hold-transition skin-blue sidebar-mini">

<div id="wrapper">

@include('frontend.partials.topbar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
          @if ($errors->count() > 0)
              <div class="alert alert-danger">
                  <ul class="list-unstyled">
                      @foreach($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
        @yield('content')
        </section>
    </div>
</div>

@include('frontend.partials.javascripts')
@yield('javascript')
</body>
</html>
