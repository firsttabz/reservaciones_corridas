@extends('frontend.app')
@section('content')
<br>
<div class="container">
   <div class="col-12">
     <div class="container">
       @if($reservacion->estatus == 'cancelado')
       <div class="card">
       <div class="card-header bg-warning">
            <strong>Estatus: </strong> Cancelado
             <!--<a class="nav-link disabled">Código de reservacion: {{ $clave }}</a>-->
       </div>
       <div class="card-body">
         <h5 class="card-title">Este código de reservación ha sido cancelado.</h5>
         <p>Código de reservación: <strong>"{{ $clave }}"</strong></p>
         <p>Presiona <strong>regresar</strong> para ir a la página de inicio.</p>
         <div class="text-right">
           <a class="btn btn-primary" href="{{url('/pageWeb')}}">Regresar</a>
         </div>
       </div>
     </div>
     @endif
     </div>
   </div>
  </div>
</div>
@endsection
