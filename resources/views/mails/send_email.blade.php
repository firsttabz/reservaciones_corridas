<!--<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Confirmación de pago de reservación con Vuz</title>
</head>
<body>
    <p>Reservación en vuz.mx</p>
    <p>Hola! {{ $cliente }}, te enviamos detalles de tu compra en vuz.mx:</p>
    <p>{{ $cantidad_boletos_pagados}} boletos(s) {{ $tipo_viaje }} para la fecha {{ $fecha_reservacion }}.</p>
    <p>Costo total: ${{ $suma_boletos }} MXN</p>
    <p>Código de reservación {{ $clave }}</p>
</body>
</html>-->

@extends('beautymail::templates.widgets')

@section('content')

  @include('beautymail::templates.widgets.articleStart', ['color' => '#FDC740'])

		<h4 class="secondary"><strong>Confirmación de pago de reservación con Vuz</strong></h4>
    <p>Hola! {{ $cliente }}, te enviamos detalles de tu compra en vuz.mx:</p>
    <p>{{ $cantidad_boletos_pagados}} boletos(s) {{ $tipo_viaje }} para la fecha {{ $fecha_reservacion }}.</p>
    <p>Costo total: ${{ $suma_boletos }} MXN</p>
    <p>Código de reservación {{ $clave }}</p>
    <br>
    <a href="http://vuz.mx">Visitar Sitio</a>

	@include('beautymail::templates.widgets.articleEnd')


@stop
