<!DOCTYPE html>
<!--[if lt IE 7]> <html dir="ltr" lang="en-US" class="ie6"> <![endif]-->
<!--[if IE 7]>    <html dir="ltr" lang="en-US" class="ie7"> <![endif]-->
<!--[if IE 8]>    <html dir="ltr" lang="en-US" class="ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html dir="ltr" lang="{{app()->getLocale()}}"> <!--<![endif]-->

<!-- BEGIN head -->

<!-- Mirrored from themes.quitenicestuff2.com/chauffeur/header-1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Oct 2018 21:17:16 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<!--Meta Tags-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<!-- Title -->
	<title>VUZ</title>

	<!-- JavaScript (must go in header) -->

	<script type="text/javascript" src="{{url('page/assets/js/fontawesome-markers.min.js')}}"></script>

	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{url('page/assets/css/style.css')}}" type="text/css"  media="all" />

	<link rel="stylesheet" href="{{url('page/assets/css/color-red.css')}}" type="text/css"  media="all" />
		<link rel="stylesheet" href="{{url('page/assets/css/responsive.css')}}" type="text/css"  media="all" />
	<link href="{{url('page/assets/maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{url('page/assets/rs-plugin/css/settings.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('page/assets/rs-plugin/css/layers.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('page/assets/rs-plugin/css/navigation.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('page/assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('page/assets/css/prettyPhoto.css')}}">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">




	<!-- Favicon -->
	<link rel="shortcut icon" href="{{url('page/assets/favicon.html')}}" type="image/x-icon" />
	@yield('head')
<!-- END head -->
</head>

<!-- BEGIN body -->
<body>
	<!-- Content Wrapper. Contains page content -->
	    <div class="content-wrapper">
	        <!-- Main content -->
	        <section class="content">
	          @if ($errors->count() > 0)
	              <div class="alert alert-danger">
	                  <ul class="list-unstyled">
	                      @foreach($errors->all() as $error)
	                          <li>{{ $error }}</li>
	                      @endforeach
	                  </ul>
	              </div>
	          @endif
	        </section>
	    </div>

    @yield('content')

    <!-- JavaScript -->
    <script src="{{url('page/assets/ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js')}}"></script>
    <script src="{{url('page/assets/ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{url('page/assets/rs-plugin/js/jquery.themepunch.tools.min838f.js?rev=5.0')}}"></script>
    <script type="text/javascript" src="{{url('page/assets/rs-plugin/js/jquery.themepunch.revolution.min838f.js?rev=5.0')}}"></script>
    <script type="text/javascript" src="{{url('page/assets/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{url('page/assets/js/jquery.prettyPhoto.js')}}"></script>

    <!-- Only required for local server -->
    <script type="text/javascript" src="{{url('page/assets/rs-plugin/js/extensions/revolution.extension.video.min.js')}}"></script>
    <script type="text/javascript" src="{{url('page/assets/rs-plugin/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript" src="{{url('page/assets/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{url('page/assets/rs-plugin/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <!-- Only required for local server -->

    <script type="text/javascript" src="{{url('page/assets/js/scripts.js')}}"></script>
</body>
  @yield('script')
</html>
