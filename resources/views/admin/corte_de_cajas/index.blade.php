@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.corte-de-caja.title')</h3>
    <div class="panel panel-default">
        {!! Form::open(['method' => 'get', 'route' => ['admin.corte_de_cajas.index'],'id' => 'form', 'autocomplete' => 'off']) !!}
        <div class="panel-body">

            <div class="col-xs-12 col-md-12">
                {!! Form::label('fecha_reservacion', 'Fecha inicio', ['class' => 'control-label']) !!}
                {!! Form::text('from', old('from', \Request::input('from')), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('from'))
                        <p class="help-block">
                            {{ $errors->first('from') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('tipo', trans('quickadmin.contabilidad.fields.tipo').'*', ['class' => 'control-label']) !!}
                {!! Form::select('tipo', ['paqueteria' => 'paqueteria', 'reservaciones' => 'reservaciones'], old('tipo', \Request::input('tipo')), ['class' => 'form-control select2', 'id' => 'tipo']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tipo'))
                        <p class="help-block">
                            {{ $errors->first('tipo') }}
                        </p>
                    @endif
             </div>
            @if(Auth::user()->role_id == 1)
            <div class="col-xs-12 col-md-6">
                {!! Form::label('user_id', trans('quickadmin.contabilidad.fields.user').'*', ['class' => 'control-label']) !!}
                {!! Form::select('user_id', $users, old('user_id', \Request::input('user_id')), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('user_id'))
                        <p class="help-block">
                            {{ $errors->first('user_id') }}
                        </p>
                    @endif
             </div>
             @else
              <input type="hidden" name="user_id" value="{{Crypt::encrypt(Auth::id())}}" />
             @endif
             <div class="col-xs-12" id="tipo_fecha_container">
               {!! Form::label('tipo_fecha', trans('quickadmin.corte-de-caja.fields.tipo_fecha').'*', ['class' => 'control-label']) !!}
               {!! Form::select('tipo_fecha', ['fecha_reservacion' => 'Por fecha reservación', 'ingreso_dinero' => 'Por fecha de ingreso de dinero'], old('tipo_fecha', \Request::input('tipo_fecha')), ['class' => 'form-control select2']) !!}
                   <p class="help-block"></p>
                   @if($errors->has('tipo'))
                       <p class="help-block">
                           {{ $errors->first('tipo') }}
                       </p>
                   @endif
             </div>
        </div>
        {!! Form::submit("Aceptar", ['class' => 'btn btn-primary btn-block',]) !!}
        {!! Form::close() !!}
    </div>

    @if(\Request::input('tipo') == 'paqueteria')
      <div class="panel panel-default">
        <div class="panel-body">
          <legend>
            {{\Request::input('tipo')}}
          </legend>
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>clave</th>
                    <th>Estatus</th>
                    <th>Fecha de envío</th>
                    <th>Remitente</th>
                    <th>Destinatario</th>
                    <th>Estatus</th>
                    <th>Vendedor recibe</th>
                    <th>Vendedor liquida</th>
                    <th>Costo</th>

                </tr>
                </thead>
                <tbody>
                @foreach($data['list'] as $r)
                <tr>
                    <td>{{$r->clave}}</td>
                    <td>{{$r->estatus}}</td>
                    <td>{{Carbon\Carbon::parse($r->created_at)->format('d-m-Y')}}</td>
                    <td>{{$r->remitente}}</td>
                    <td>{{$r->destinatario}}</td>
                    <td>{{$r->estatus}}</td>
                    <td>{{\App\User::find($r->vendedor_id_recibe)->name}}</td>
                    <td>{{\App\User::find($r->vendedor_id_dinero)->name}}</td>
                    <td>{{number_format($r->costo,2)}}</td>
                </tr>
                @endforeach

                <tr class="warning">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>TOTAL</td>
                    <td>{{number_format($data['sum'],2)}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    @elseif(\Request::input('tipo') == 'reservaciones' &&  \Request::input('tipo_fecha') == 'ingreso_dinero')
  <!--  /*************************************************** INGRESO DE DINERO ***************************************/ -->
        <div class="panel">
          <div class="row">
            <div class="col-lg-12 col-sm-12">
              <div class="container">
                <h3 class=""><b>Reservaciones por ingreso de dinero</b></h3>
                <table class="table table-bordered" style="width:92%;">
                  <thead>
                    <tr>
                        <th style="width:50%" >CONCEPTO</th>
                        <th style="width:50%" >SUMATORIA</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <th style="width:50%"  scope="row">Pagados del día</th>
                        <td style="background-color:#0080006b;">{{number_format($data['sum_pagado_del_dia_NV'],2)}}</td>
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row">Anticipos del día</th>
                        <td style="background-color:#0080006b;">{{number_format($data['sum_anticipo_del_dia'],2)}}</td>
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row">Liquidaciones del día</th>
                        <td style="background-color:#0080006b;">{{number_format($data['sum_liquidado_del_dia'],2)}}</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td><!--   THIS IS UP    -->
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row">Pagados de otros días</th>
                        <td style="background-color:#ffeb3bd1;">{{number_format($data['sum_pagado_de_otros_dias_NV'],2)}}</td>
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row">Anticipos de otros días</th>
                        <td style="background-color:#ffeb3bd1;">{{number_format($data['sum_anticipo_de_otros_dias'],2)}}</td>
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row">Liquidaciones de otros días</th>
                        <td style="background-color:#ffeb3bd1;">{{number_format($data['sum_liquidado_de_otros_dias_NV'],2)}}</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td><!--   THIS IS UP    -->
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row"><span>TOTAL PAGADOS</span></th>
                        <td>{{number_format($data['sum_pagado_del_dia_NV'] + $data['sum_pagado_de_otros_dias_NV'],2)}}</td>
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row"><b>TOTAL ANTICIPOS</b></th>
                        <td>{{number_format($data['sum_anticipo_de_otros_dias'] + $data['sum_anticipo_del_dia'],2)}}</td>
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row"><span>TOTAL LIQUIDACIONES</span></th>
                        <td>{{number_format($data['sum_liquidado_del_dia'] + $data['sum_liquidado_de_otros_dias_NV'],2)}}</td>
                    </tr>
                    <tr>
                        <th style="width:50%"  scope="row"><span>TOTALES</span></th>
                        <td style="background-color:#03a9f469;"><b>{{number_format($data['sum_liquidado_del_dia'] + $data['sum_liquidado_de_otros_dias_NV'] + $data['sum_anticipo_del_dia'] + $data['sum_anticipo_de_otros_dias'] + $data['sum_pagado_del_dia_NV'] + $data['sum_pagado_de_otros_dias_NV'],2)}} </b></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
          <div class="panel table-responsive">
            <div class="panel-body">
              <legend><b>Reservaciones pagadas del día</b></legend>
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>Clave</th>
                      <th>Corrida</th>
                      <th>Fecha reservación</th>
                      <th>Fecha creación</th>
                      <th>Estatus</th>
                      <th>Cliente</th>
                      <th>Vendedor</th>
                      <th>Costo boleto</th>

                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data['list_pagado_del_dia_NV'] as $r)
                  <tr>
                      <td>{{$r->clave}}</td>
                      <td>{{\App\Corrida::find($r->corrida_id)->nombre}}</td>
                      <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                      <td>{{\Carbon\Carbon::parse($r->created_at)->format('d/m/Y')}}</td>
                      <td>{{$r->estatus}}</td>
                      <td>{{$r->cliente}}</td>
                      <td>{{ \App\User::find($r->vendedor_pagado_id)->name}}</td>
                      <td>{{number_format($r->costo_boleto,2)}}</td>

                  </tr>
                  @endforeach

                  <tr class="warning">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>TOTAL</td>
                      <td>{{number_format($data['sum_pagado_del_dia_NV'],2)}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!-- LIQUIDACIONES DEL DÍA -->
            <div class="panel table-responsive">
              <div class="panel-body">
                <legend><b>Reservaciones liquidadas del día</b></legend>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Clave</th>
                        <th>Corrida</th>
                        <th>Fecha reservación</th>
                        <th>Fecha creación</th>
                        <th>Estatus</th>
                        <th>Cliente</th>
                        <th>Vendedor</th>
                        <th>Costo boleto</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data['list_liquidado_del_dia'] as $r)
                    <tr>
                        <td>{{$r->clave}}</td>
                        <td>{{\App\Corrida::find($r->corrida_id)->nombre}}</td>
                        <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                        <td>{{\Carbon\Carbon::parse($r->created_at)->format('d/m/Y')}}</td>
                        <td>{{$r->estatus}}</td>
                        <td>{{$r->cliente}}</td>
                        <td>{{\App\User::find($r->vendedor_liquida_id)['name']}}</td>
                        <td>{{number_format($r->costo_boleto,2)}}</td>

                    </tr>
                    @endforeach
                    <tr class="warning">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>TOTAL</td>
                        <td>{{number_format($data['sum_liquidado_del_dia'],2)}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          <!-- OTROS DÍAS ----->
        <div class="panel table-responsive">
          <div class="panel-body">
            <legend><b>Reservaciones pagadas de otros días</b></legend>
                  <table class="table table-striped">
                      <thead>
                      <tr>
                          <th>Clave</th>
                          <th>Corrida</th>
                          <th>Fecha reservación</th>
                          <th>Fecha creación</th>
                          <th>Estatus</th>
                          <th>Cliente</th>
                          <th>Vendedor</th>
                          <th>Costo boleto</th>

                      </tr>
                      </thead>
                      <tbody>
                      @foreach($data['list_pagado_de_otros_dias_NV'] as $r)
                      <tr>
                          <td>{{$r->clave}}</td>
                          <td>{{\App\Corrida::find($r->corrida_id)->nombre}}</td>
                          <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                          <td>{{\Carbon\Carbon::parse($r->created_at)->format('d/m/Y')}}</td>
                          <td>{{$r->estatus}}</td>
                          <td>{{$r->cliente}}</td>
                          <td>{{ \App\User::find($r->vendedor_pagado_id)->name}}</td>
                          <td>{{number_format($r->costo_boleto,2)}}</td>

                      </tr>
                      @endforeach

                      <tr class="warning">
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>TOTAL</td>
                          <td>{{number_format($data['sum_pagado_de_otros_dias_NV'],2)}}</td>
                      </tr>
                    </tbody>
                  </table>
        </div>
        </div>
        <div class="panel table-responsive">
          <div class="panel-body">
            <legend><b>Reservaciones liquidadas de otros días</b></legend>
                  <table class="table table-striped">
                      <thead>
                      <tr>
                          <th>clave</th>
                          <th>Corrida</th>
                          <th>Fecha reservación</th>
                          <th>Fecha creación</th>
                          <th>Estatus</th>
                          <th>Cliente</th>
                          <th>Vendedor</th>
                          <th>Costo boleto</th>

                      </tr>
                      </thead>
                      <tbody>
                      @foreach($data['list_liquidado_de_otros_dias_NV'] as $r)
                      <tr>
                          <td>{{$r->clave}}</td>
                          <td>{{\App\Corrida::find($r->corrida_id)->nombre}}</td>
                          <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                          <td>{{\Carbon\Carbon::parse($r->created_at)->format('d/m/Y')}}</td>
                          <td>{{$r->estatus}}</td>
                          <td>{{$r->cliente}}</td>
                          <td>{{\App\User::find($r->vendedor_liquida_id)['name']}}</td>
                          <td>{{number_format($r->costo_boleto,2)}}</td>

                      </tr>
                      @endforeach

                      <tr class="warning">
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>TOTAL</td>
                          <td>{{number_format($data['sum_liquidado_de_otros_dias_NV'],2)}}</td>
                      </tr>
                    </tbody>
                  </table>
        </div>
        </div>
<!-- /********************************** END INGRESO DE DINERO *****************************/ -->
    @elseif(\Request::input('tipo') == 'reservaciones' && \Request::input('user_id') != '')
      @foreach($fechas_pagado as $fp)
        <div class="panel table-responsive">
          <div class="panel-body">
            <legend>Boletos pagados para la fecha de reservación {{$fp['fecha']}}</legend>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>clave</th>
                    <th>Fecha reservación</th>
                    <th>Fecha creación</th>
                    <th>Estatus</th>
                    <th>Cliente</th>
                    <th>Vendedor</th>
                    <th>Costo boleto</th>

                </tr>
                </thead>
                <tbody>
                @foreach($fp['reservaciones'] as $r)
                <tr>
                    <td>{{$r->clave}}</td>
                    <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($r->created_at)->format('d/m/Y')}}</td>
                    <td>{{$r->estatus}}</td>
                    <td>{{$r->cliente}}</td>
                    <td>{{ \App\User::find($r->vendedor_pagado_id)->name}}</td>
                    <td>{{number_format($r->costo_boleto,2)}}</td>

                </tr>
                @endforeach

                <tr class="warning">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>TOTAL</td>
                    <td>{{number_format($fp['sum'],2)}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      @endforeach

      @foreach($fechas_anticipado as $fa)
        <div class="panel table-responsive">
          <div class="panel-body">
            <legend>
              {{\Request::input('tipo')}} con anticipos recibidos de la fecha {{$fa['fecha']}}
            </legend>
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>clave</th>
                      <th>Fecha reservación</th>
                      <th>Fecha creación</th>
                      <th>Estatus</th>
                      <th>Cliente</th>
                      <th>Vendedor</th>
                      <th>Costo boleto</th>
                      <th>Anticipo</th>

                  </tr>
                  </thead>
                  <tbody>
                  @foreach($fa['reservaciones'] as $r)
                  <tr>
                      <td>{{$r->clave}}</td>
                      <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                      <td>{{\Carbon\Carbon::parse($r->created_at)->format('d/m/Y')}}</td>
                      <td>{{$r->estatus}}</td>
                      <td>{{$r->cliente}}</td>
                      <td>{{\App\User::find($r->vendedor_anticipa_id)->name}}</td>
                      <td>{{number_format($r->costo_boleto,2)}}</td>
                      <td>{{number_format($r->anticipo,2)}}</td>

                  </tr>
                  @endforeach

                  <tr class="warning">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>TOTAL</td>
                      <td>{{number_format($fa['sum'],2)}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      @endforeach

      @foreach($fechas_liquidado as $fl)
        <div class="panel table-responsive">
          <div class="panel-body">
            <legend>
              {{\Request::input('tipo')}} con liquidaciones de la fecha {{$fl['fecha']}}
            </legend>
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>clave</th>
                      <th>Fecha reservación</th>
                      <th>Fecha creación</th>
                      <th>Estatus</th>
                      <th>Cliente</th>
                      <th>Vendedor</th>
                      <th>Costo boleto</th>
                      <th>Liquidación</th>

                  </tr>
                  </thead>
                  <tbody>
                  @foreach($fl['reservaciones'] as $r)
                  <tr>
                      <td>{{$r->clave}}</td>
                      <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                      <td>{{\Carbon\Carbon::parse($r->created_at)->format('d/m/Y')}}</td>
                      <td>{{$r->estatus}}</td>
                      <td>{{$r->cliente}}</td>
                      <td>{{\App\User::find($r->vendedor_liquida_id)->name}}</td>
                      <td>{{number_format($r->costo_boleto,2)}}</td>
                      <td>{{number_format($r->resta,2)}}</td>

                  </tr>
                  @endforeach

                  <tr class="warning">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>TOTAL</td>
                      <td>{{number_format($fl['sum'],2)}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      @endforeach
    @endif
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });
    </script>
    <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
    }

    $(document).ready(function() {
      //console.log('jquery ready')
      $('#tipo_fecha_container').hide();
      if($('#tipo').val() == 'reservaciones') {
        $('#tipo_fecha_container').show();
      }
      $('#tipo').change(function(e) {
        let val = (e.target.value)
        if(val === 'paqueteria') {
          $('#tipo_fecha_container').hide();
        } else {
          $('#tipo_fecha_container').show();
        }
      });
    });
    </script>
@stop
