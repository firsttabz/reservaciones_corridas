@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.corte-de-caja.title')</h3>
    <div class="panel panel-default">
        {!! Form::open(['method' => 'get', 'route' => ['admin.corte_de_cajas.index'],'id' => 'form', 'autocomplete' => 'off']) !!}
        <div class="panel-body">

            <div class="col-xs-12 col-md-6">
                {!! Form::label('fecha_reservacion', 'Fecha inicio', ['class' => 'control-label']) !!}
                {!! Form::text('from', old('from', \Request::input('from')), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('from'))
                        <p class="help-block">
                            {{ $errors->first('from') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('fecha_reservacion', 'Fecha final', ['class' => 'control-label']) !!}
                {!! Form::text('to', old('to', \Request::input('to')), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('to'))
                        <p class="help-block">
                            {{ $errors->first('to') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('tipo', trans('quickadmin.contabilidad.fields.tipo').'*', ['class' => 'control-label']) !!}
                {!! Form::select('tipo', ['paqueteria' => 'paqueteria', 'reservaciones' => 'reservaciones'], old('tipo', \Request::input('tipo')), ['class' => 'form-control select2', 'id' => 'tipo']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tipo'))
                        <p class="help-block">
                            {{ $errors->first('tipo') }}
                        </p>
                    @endif
             </div>
            @if(Auth::user()->role_id == 1)
            <div class="col-xs-12 col-md-6">
                {!! Form::label('user_id', trans('quickadmin.contabilidad.fields.user').'*', ['class' => 'control-label']) !!}
                {!! Form::select('user_id', $users, old('user_id', \Request::input('user_id')), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('user_id'))
                        <p class="help-block">
                            {{ $errors->first('user_id') }}
                        </p>
                    @endif
             </div>
             @else
              <input type="hidden" name="user_id" value="{{Crypt::encrypt(Auth::id())}}" />
             @endif
             <div class="col-xs-12" id="tipo_fecha_container">
               {!! Form::label('tipo_fecha', trans('quickadmin.corte-de-caja.fields.tipo_fecha').'*', ['class' => 'control-label']) !!}
               {!! Form::select('tipo_fecha', ['fecha_reservacion' => 'Por fecha reservación', 'ingreso_dinero' => 'Por fecha de ingreso de dinero'], old('tipo_fecha', \Request::input('tipo_fecha')), ['class' => 'form-control select2']) !!}
                   <p class="help-block"></p>
                   @if($errors->has('tipo'))
                       <p class="help-block">
                           {{ $errors->first('tipo') }}
                       </p>
                   @endif
             </div>
        </div>
        {!! Form::submit("Aceptar", ['class' => 'btn btn-primary btn-block',]) !!}
        {!! Form::close() !!}
    </div>

    @if(\Request::input('tipo') == 'paqueteria')
    <div class="panel panel-default">
        <div class="panel-body">
          <legend>
            {{\Request::input('tipo')}}
          </legend>
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>clave</th>
                    <th>Estatus</th>
                    <th>Fecha de envío</th>
                    <th>Remitente</th>
                    <th>Destinatario</th>
                    <th>Estatus</th>
                    <th>Vendedor recibe</th>
                    <th>Vendedor liquida</th>
                    <th>Costo</th>

                </tr>
                </thead>
                <tbody>
                @foreach($data['list'] as $r)
                <tr>
                    <td>{{$r->clave}}</td>
                    <td>{{$r->estatus}}</td>
                    <td>{{Carbon\Carbon::parse($r->created_at)->format('d-m-Y')}}</td>
                    <td>{{$r->remitente}}</td>
                    <td>{{$r->destinatario}}</td>
                    <td>{{$r->estatus}}</td>
                    <td>{{\App\User::find($r->vendedor_id_recibe)->name}}</td>
                    <td>{{\App\User::find($r->vendedor_id_dinero)->name}}</td>
                    <td>{{number_format($r->costo,2)}}</td>
                </tr>
                @endforeach

                <tr class="warning">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>TOTAL</td>
                    <td>{{number_format($data['sum'],2)}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      @elseif(\Request::input('tipo') == 'reservaciones')
        <div class="panel panel-default">
          <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>sumatoria</th>

                </tr>
                </thead>
                <tbody>
                  <tr>
                      <td>pagado</td>
                      <td>{{number_format($data['sum_pagado'],2)}}</td>
                  </tr>
                  <tr>
                      <td>anticipo</td>
                      <td>{{number_format($data['sum_anticipo'],2)}}</td>
                  </tr>
                  <tr>
                      <td>liquidado</td>
                      <td>{{number_format($data['sum_liquidado'],2)}}</td>
                  </tr>
                  <tr>
                      <td><b>TOTAL</b></td>
                      <td>{{number_format($data['sum_pagado'] + $data['sum_anticipo'] + $data['sum_liquidado'],2)}}</td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <legend>
              {{\Request::input('tipo')}} pagadas (100%)
            </legend>
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>clave</th>
                      <th>Fecha</th>
                      <th>Estatus</th>
                      <th>Cliente</th>
                      <th>Vendedor</th>
                      <th>Costo boleto</th>

                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data['list_pagado'] as $index => $r)
                    @if($index > 0  && \Request::input('user_id') != '' && \Request::input('tipo_fecha') == 'fecha_reservacion')
                      @if($data['list_pagado'][$index - 1]['fecha_reservacion'] != $r->fecha_reservacion)
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <th>FECHA</th>
                          <th>
                            <h4>{{Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</h4>
                          </th>
                        </tr>
                      @endif
                    @elseif(\Request::input('user_id') != '' && \Request::input('tipo_fecha') == 'fecha_reservacion')
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th>FECHA</th>
                        <th>
                          <h4>{{Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</h4>
                        </th>
                      </tr>
                    @endif
                  <tr>
                      <td>{{$r->clave}}</td>
                      <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                      <td>{{$r->estatus}}</td>
                      <td>{{$r->cliente}}</td>
                      <td>{{ \App\User::find($r->vendedor_pagado_id)->name}}</td>
                      <td>{{number_format($r->costo_boleto,2)}}</td>

                  </tr>
                  @endforeach

                  <tr class="warning">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>TOTAL</td>
                      <td>{{number_format($data['sum_pagado'],2)}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <legend>
              {{\Request::input('tipo')}} con anticipos recibidos
            </legend>
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>clave</th>
                      <th>Fecha</th>
                      <th>Estatus</th>
                      <th>Cliente</th>
                      <th>Vendedor</th>
                      <th>Costo boleto</th>
                      <th>Anticipo</th>

                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data['list_anticipo'] as $index => $r)
                    @if($index > 0  && \Request::input('user_id') != '' && \Request::input('tipo_fecha') == 'fecha_reservacion')
                      @if($data['list_anticipo'][$index - 1]['fecha_reservacion'] != $r->fecha_reservacion)
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <th>FECHA</th>
                          <th>
                            <h4>{{Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</h4>
                          </th>
                        </tr>
                      @endif
                    @elseif(\Request::input('user_id') != '' && \Request::input('tipo_fecha') == 'fecha_reservacion')
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th>FECHA</th>
                        <th>
                          <h4>{{Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</h4>
                        </th>
                      </tr>
                    @endif
                  <tr>
                      <td>{{$r->clave}}</td>
                      <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                      <td>{{$r->estatus}}</td>
                      <td>{{$r->cliente}}</td>
                      <td>{{\App\User::find($r->vendedor_anticipa_id)->name}}</td>
                      <td>{{number_format($r->costo_boleto,2)}}</td>
                      <td>{{number_format($r->anticipo,2)}}</td>

                  </tr>
                  @endforeach

                  <tr class="warning">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>TOTAL</td>
                      <td>{{number_format($data['sum_anticipo'],2)}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <legend>
              {{\Request::input('tipo')}} con liquidaciones
            </legend>
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>clave</th>
                      <th>Fecha</th>
                      <th>Estatus</th>
                      <th>Cliente</th>
                      <th>Vendedor</th>
                      <th>Costo boleto</th>
                      <th>Liquidación</th>

                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data['list_liquidado'] as $index => $r)
                  @if($index > 0  && \Request::input('user_id') != '' && \Request::input('tipo_fecha') == 'fecha_reservacion')
                    @if($data['list_liquidado'][$index - 1]['fecha_reservacion'] != $r->fecha_reservacion)
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th>FECHA</th>
                        <th>
                          <h4>{{Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</h4>
                        </th>
                      </tr>
                    @endif
                  @elseif(\Request::input('user_id') != '' && \Request::input('tipo_fecha') == 'fecha_reservacion')
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <th>FECHA</th>
                      <th>
                        <h4>{{Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</h4>
                      </th>
                    </tr>
                  @endif
                  <tr>
                      <td>{{$r->clave}}</td>
                      <td>{{\Carbon\Carbon::parse($r->fecha_reservacion)->format('d/m/Y')}}</td>
                      <td>{{$r->estatus}}</td>
                      <td>{{$r->cliente}}</td>
                      <td>{{\App\User::find($r->vendedor_liquida_id)->name}}</td>
                      <td>{{number_format($r->costo_boleto,2)}}</td>
                      <td>{{number_format($r->resta,2)}}</td>

                  </tr>
                  @endforeach

                  <tr class="warning">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>TOTAL</td>
                      <td>{{number_format($data['sum_liquidado'],2)}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      @endif
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });
    </script>
    <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
    }

    $(document).ready(function() {
      //console.log('jquery ready')
      $('#tipo_fecha_container').hide();
      if($('#tipo').val() == 'reservaciones') {
        $('#tipo_fecha_container').show();
      }
      $('#tipo').change(function(e) {
        let val = (e.target.value)
        if(val === 'paqueteria') {
          $('#tipo_fecha_container').hide();
        } else {
          $('#tipo_fecha_container').show();
        }
      });
    });
    </script>
@stop
