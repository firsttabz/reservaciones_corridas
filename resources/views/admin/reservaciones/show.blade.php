@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.reservaciones.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.cliente')</th>
                            <td field-key='cliente'>{{ $reservacione->cliente }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.anticipo')</th>
                            <td field-key='cliente'>{{ $reservacione->cliente }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.costo-boleto')</th>
                            <td field-key='costo_boleto'>{{ $reservacione->costo_boleto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.vendedor')</th>
                            <td field-key='vendedor'>{{ $reservacione->vendedor->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.asiento')</th>
                            <td field-key='asiento'>{{ $reservacione->asiento->numero or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.corrida')</th>
                            <td field-key='corrida'>{{ $reservacione->corrida->nombre or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.clave')</th>
                            <td field-key='clave'>{{ $reservacione->clave }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.descuento')</th>
                            <td field-key='descuento'>{{ Form::checkbox("descuento", 1, $reservacione->descuento == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.observaciones')</th>
                            <td field-key='observaciones'>{!! $reservacione->observaciones !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.comprobante-pago')</th>
                            <td field-key='comprobante_pago'>@if($reservacione->comprobante_pago)<a href="{{ asset(env('UPLOAD_PATH').'/' . $reservacione->comprobante_pago) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.estatus')</th>
                            <td field-key='estatus'>{{ $reservacione->estatus }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.reservaciones.fields.fecha-reservacion')</th>
                            <td field-key='fecha_reservacion'>{{ $reservacione->fecha_reservacion }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.reservaciones.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });
    </script>

@stop
