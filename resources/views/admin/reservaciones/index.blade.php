@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.reservaciones.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            Busqueda de reservaciones
        </div>

        <div class="panel-body table-responsive">
          <form class="" action="" method="get" autocomplete="off">
            <div class="row">
              <div class="col-xs-12 col-lg-8">
                  <div class="input-group">
                    <input type="text" class="form-control" name="codigo" placeholder="Ingresa el código de reservación" value="{{old('codigo',Request::get('codigo'))}}" required>
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Buscar</button>
                    </span>
                  </div><!-- /input-group -->
              </div><!-- /.col-lg-6 -->
              <div class="col-xs-12 col-lg-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="mostrar_cancelados" checked> NO mostrar boletos cancelados
                  </label>
                </div>
              </div>
            </div><!-- /.row -->
          </form>
          <br>
          <div class="row">
            @foreach($reservaciones as $r)
              <div class="col-xs-12 col-md-4">
                <div class="small-box bg-primary">
                  <div class="inner">
                    <h3>{{$r->corrida->nombre}} {{$r->corrida->extraordinaria? '(e)':''}} <br>{{$r->corrida->hora_salida}}</h3>
                    <span class="label label-default pull-right">{{$r->estatus}}</span>
                    <div>
                      {{$r->cliente}}
                    </div>
                    <div>
                      Número de asiento : {{$r->asiento->numero}}
                    </div>
                    <p class="text-center"></p>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-bandcamp"></i>
                      </div>
                      <input type="text" readonly="readonly" class="form-control" value="{{$r->clave}}">
                    </div>
                    <p></p>
                    <p class="text-center"></p>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" readonly="readonly" class="form-control" value="{{$r->fecha_reservacion}}"></div>
                      <p></p>
                      <p class="text-center"></p>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-money"></i>
                        </div>
                        <input type="text" readonly="readonly" class="form-control" value="{{$r->costo_boleto}}">
                      </div>
                      @if($r->estatus == 'apartado')
                      <div class="pull-right">
                        resta: ${{number_format($r->costo_boleto - $r->anticipo,2)}}
                      </div>
                      @endif
                    </div>
                    <div class="icon">
                      <i class="fa fa-ticket"></i>
                    </div>
                    <!-- Modal -->
                    <p class="help-block"></p>
                    <a target="_blank" href="/admin/reservaciones/imprimir/{{$r->clave}}" class="small-box-footer">
                      Imprimir boleto <i class="fa fa-arrow-circle-right"></i>
                    </a>
                    @if($r->estatus == 'apartado')
                    <a href="" class="small-box-footer" style="background-color:green;"  data-toggle="modal" data-target="#modal_liquidar_{{$r->id}}">
                      Liquidar boleto<i class="fa fa-arrow-circle-right"></i>
                    </a>
                    @endif
                    @if($r->estatus != 'cancelado')
                    <a href="#" class="small-box-footer" style="background-color:Orange;"  data-toggle="modal" data-target="#modal_cancelar_{{$r->id}}">
                      Cancela boleto <i class="fa fa-arrow-circle-right"></i>
                    </a>
                    @endif

                </div>
                <div class="modal fade" id="modal_cancelar_{{$r->id}}" tabindex="-1" role="dialog" aria-labelledby="modal_cancelar_{{$r->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel" style="color:black;">Cancela boleto</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body" style="color:black;">
                          ¿Estás seguro de cancelar el boleto {{$r->clave}}?
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                        <a href="{{url('admin/reservaciones/cancelar',$r->clave)}}" class="btn btn-secondery" style="background-color:Orange;">Confirmar</a>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="modal_liquidar_{{$r->id}}" tabindex="-1" role="dialog" aria-labelledby="modal_liquidar_{{$r->id}}" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style="color:black;">Liquidar boleto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" style="color:black;">
                            El cliente <b>{{$r->cliente}}</b> debe cubrir la cantidad de ${{number_format($r->costo_boleto - $r->anticipo,2)}}, ¿Deseas Liquidarlo?
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                          <a href="{{url('admin/reservaciones/liquidar',$r->clave)}}" class="btn btn-success">Confirmar</a>

                          </div>
                        </div>
                      </div>
                    </div>
              </div>
            @endforeach
          </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        @can('reservacione_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.reservaciones.mass_destroy') }}'; @endif
        @endcan

    </script>
    <script>
      $('#myModal').modal('show')
    </script>
@endsection
