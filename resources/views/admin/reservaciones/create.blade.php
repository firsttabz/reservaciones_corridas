@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.reservaciones.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.reservaciones.store'], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('cliente', trans('quickadmin.reservaciones.fields.cliente').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('cliente', old('cliente'), ['class' => 'form-control', 'placeholder' => 'Nombre de la persona quien reliza la reservación', 'required' => '']) !!}
                    <p class="help-block">Nombre de la persona quien reliza la reservación</p>
                    @if($errors->has('cliente'))
                        <p class="help-block">
                            {{ $errors->first('cliente') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('costo_boleto', trans('quickadmin.reservaciones.fields.costo-boleto').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('costo_boleto', old('costo_boleto'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('costo_boleto'))
                        <p class="help-block">
                            {{ $errors->first('costo_boleto') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('vendedor_id', trans('quickadmin.reservaciones.fields.vendedor').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('vendedor_id', $vendedors, old('vendedor_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('vendedor_id'))
                        <p class="help-block">
                            {{ $errors->first('vendedor_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('asiento_id', trans('quickadmin.reservaciones.fields.asiento').'', ['class' => 'control-label']) !!}
                    {!! Form::select('asiento_id', $asientos, old('asiento_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('asiento_id'))
                        <p class="help-block">
                            {{ $errors->first('asiento_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('corrida_id', trans('quickadmin.reservaciones.fields.corrida').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('corrida_id', $corridas, old('corrida_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('corrida_id'))
                        <p class="help-block">
                            {{ $errors->first('corrida_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('clave', trans('quickadmin.reservaciones.fields.clave').'', ['class' => 'control-label']) !!}
                    {!! Form::text('clave', old('clave'), ['class' => 'form-control', 'placeholder' => 'Calve de reservación']) !!}
                    <p class="help-block">Calve de reservación</p>
                    @if($errors->has('clave'))
                        <p class="help-block">
                            {{ $errors->first('clave') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('descuento', trans('quickadmin.reservaciones.fields.descuento').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('descuento', 0) !!}
                    {!! Form::checkbox('descuento', 1, old('descuento', false), []) !!}
                    <p class="help-block">Marcar si el boleto aplica descuento</p>
                    @if($errors->has('descuento'))
                        <p class="help-block">
                            {{ $errors->first('descuento') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('observaciones', trans('quickadmin.reservaciones.fields.observaciones').'', ['class' => 'control-label']) !!}
                    {!! Form::textarea('observaciones', old('observaciones'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('observaciones'))
                        <p class="help-block">
                            {{ $errors->first('observaciones') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('comprobante_pago', trans('quickadmin.reservaciones.fields.comprobante-pago').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('comprobante_pago', old('comprobante_pago')) !!}
                    {!! Form::file('comprobante_pago', ['class' => 'form-control']) !!}
                    {!! Form::hidden('comprobante_pago_max_size', 5) !!}
                    <p class="help-block"></p>
                    @if($errors->has('comprobante_pago'))
                        <p class="help-block">
                            {{ $errors->first('comprobante_pago') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('estatus', trans('quickadmin.reservaciones.fields.estatus').'*', ['class' => 'control-label']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('estatus'))
                        <p class="help-block">
                            {{ $errors->first('estatus') }}
                        </p>
                    @endif
                    <div>
                        <label>
                            {!! Form::radio('estatus', 'cancelado', false, ['required' => '']) !!}
                            cancelado
                        </label>
                    </div>
                    <div>
                        <label>
                            {!! Form::radio('estatus', 'apartado', false, ['required' => '']) !!}
                            apartado
                        </label>
                    </div>
                    <div>
                        <label>
                            {!! Form::radio('estatus', 'pagado', false, ['required' => '']) !!}
                            pagado
                        </label>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('fecha_reservacion', trans('quickadmin.reservaciones.fields.fecha-reservacion').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('fecha_reservacion', old('fecha_reservacion'), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('fecha_reservacion'))
                        <p class="help-block">
                            {{ $errors->first('fecha_reservacion') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop