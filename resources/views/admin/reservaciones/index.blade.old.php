@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.reservaciones.title')</h3>
    @can('reservacione_create')
    <p>
        <a href="{{ route('admin.reservaciones.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>

    </p>
    @endcan

    @can('reservacione_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.reservaciones.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.reservaciones.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($reservaciones) > 0 ? 'datatable' : '' }} @can('reservacione_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('reservacione_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.reservaciones.fields.cliente')</th>
                        <th>@lang('quickadmin.reservaciones.fields.costo-boleto')</th>
                        <th>@lang('quickadmin.reservaciones.fields.vendedor')</th>
                        <th>@lang('quickadmin.reservaciones.fields.asiento')</th>
                        <th>@lang('quickadmin.reservaciones.fields.corrida')</th>
                        <th>@lang('quickadmin.reservaciones.fields.clave')</th>
                        <th>@lang('quickadmin.reservaciones.fields.descuento')</th>
                        <th>@lang('quickadmin.reservaciones.fields.observaciones')</th>
                        <th>@lang('quickadmin.reservaciones.fields.comprobante-pago')</th>
                        <th>@lang('quickadmin.reservaciones.fields.estatus')</th>
                        <th>@lang('quickadmin.reservaciones.fields.fecha-reservacion')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>

                <tbody>
                    @if (count($reservaciones) > 0)
                        @foreach ($reservaciones as $reservacione)
                            <tr data-entry-id="{{ $reservacione->id }}">
                                @can('reservacione_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='cliente'>{{ $reservacione->cliente }}</td>
                                <td field-key='costo_boleto'>{{ $reservacione->costo_boleto }}</td>
                                <td field-key='vendedor'>{{ $reservacione->vendedor->name or '' }}</td>
                                <td field-key='asiento'>{{ $reservacione->asiento->numero or '' }}</td>
                                <td field-key='corrida'>{{ $reservacione->corrida->nombre or '' }}</td>
                                <td field-key='clave'>{{ $reservacione->clave }}</td>
                                <td field-key='descuento'>{{ Form::checkbox("descuento", 1, $reservacione->descuento == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='observaciones'>{!! $reservacione->observaciones !!}</td>
                                <td field-key='comprobante_pago'>@if($reservacione->comprobante_pago)<a href="{{ asset(env('UPLOAD_PATH').'/' . $reservacione->comprobante_pago) }}" target="_blank">Download file</a>@endif</td>
                                <td field-key='estatus'>{{ $reservacione->estatus }}</td>
                                <td field-key='fecha_reservacion'>{{ $reservacione->fecha_reservacion }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('reservacione_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.restore', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('reservacione_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.perma_del', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('reservacione_view')
                                    <a href="{{ route('admin.reservaciones.show',[$reservacione->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('reservacione_edit')
                                    <a href="{{ route('admin.reservaciones.edit',[$reservacione->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('reservacione_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.destroy', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="16">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        @can('reservacione_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.reservaciones.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection
