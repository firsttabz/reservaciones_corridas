@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.asiento.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.asientos.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('numero', trans('quickadmin.asiento.fields.numero').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('numero', old('numero'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('numero'))
                        <p class="help-block">
                            {{ $errors->first('numero') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('corrida_id', trans('quickadmin.asiento.fields.corrida').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('corrida_id', $corridas, old('corrida_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('corrida_id'))
                        <p class="help-block">
                            {{ $errors->first('corrida_id') }}
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop
