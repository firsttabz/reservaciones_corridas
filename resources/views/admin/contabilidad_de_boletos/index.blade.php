@extends('layouts.app')

@section('content')
<h2>Contabilidad de boletos</h2>
<div class="row">
  {!! Form::open(['method' => 'get','id' => 'form', 'autocomplete' => 'off', 'name' => 'form1']) !!}
  <div class="col-xs-12 col-md-6">
    {!! Form::label('from','Fecha', ['class' => 'control-label']) !!}

    <div class="input-group date">
        <div class="input-group-addon" id="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="text" name="from" class="form-control pull-right" id="date" value="{{old('from',Request::input('from'))}}">

    </div>
    <p class="help-block"></p>
        @if($errors->has('from'))
            <p class="help-block">
                {{ $errors->first('from') }}
            </p>
        @endif
  </div>
  <div class="col-xs-12 col-md-6 form-group">
      {!! Form::label('corrida_id', trans('quickadmin.corridas.title').'', ['class' => 'control-label']) !!}
      {!! Form::select('corrida_id', $corridas, old('corrida_id',\Request::input('corrida_id')), ['class' => 'form-control select2', 'required' => '']) !!}
      <p class="help-block"></p>
      @if($errors->has('corrida_id'))
          <p class="help-block">
              {{ $errors->first('corrida_id') }}
          </p>
      @endif
  </div>
  <div class="col-xs-12">
    <button type="submit" class="btn btn-block btn-success">Aceptar</button>

  </div>
  {!! Form::close() !!}
</div>
 @if(\Request::all())
  @include('admin.contabilidad_de_boletos.partials.tabla_desglose_tipo_reservacion', ['sumatorias' => $sumatorias, 'totales'=> $totales])
  @include('admin.contabilidad_de_boletos.partials.totales_distribucion',['count_reservaciones' => $count_reservaciones, 'count_redondos' => $count_redondos, 'totales'=> $totales])
  @include('admin.contabilidad_de_boletos.partials.listado_reservaciones_por_estado', ['venta_estados' => $venta_estados]);
 @endif
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            }).on('dp.hide', function(e){ /*$('form[name=form1]').submit() */})
        });
    </script>
    <!--<script type="text/javascript">
      function onclickCalendarInput(){
        document.getElementById('input-group-addon').click();
      }

    </script> -->
@stop
