<br>
<section class="">
  @foreach($sumatorias as $tipo_reservacion => $element)
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          {{strtoupper($tipo_reservacion)}}
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped" style="width:100%">
              <tbody>
                <tr>
                  <th>Costo boleto</th>
                  <th>Núm. de personas</th>
                  <th>total</th>
                </tr>
                @foreach($element as $variedad => $value)
                  <tr>
                    <td>{{$value['costo_boleto']}}</td>
                    <td>{{$value['numero_personas']}}</td>
                    <td>{{number_format($value['total'],2)}}</td>
                  </tr>
                @endforeach
                <tr>
                  <td></td>
                  <td>
                    {{($totales_personas[$tipo_reservacion])}}
                    <div class="pull-right">
                      <b>TOTAL</b>
                    </div>
                  </td>
                  <td> {{number_format($totales[$tipo_reservacion],2)}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</section>
