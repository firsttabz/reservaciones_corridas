<br>
<section>
  <div class="row">
    <div class="col-xs-12 col-md-3">
      <div class="box box-primary">
        <div class="box-header">
          <b>Total de reservaciones</b>
        </div>
        <div class="box-body text-center">
          <h1>{{$count_reservaciones}}</h1>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-3">
      <div class="box box-primary">
        <div class="box-header">
          <b>Total de redondos</b>
          <div class="pull-right">
            <div>(-1)</div>
          </div>
        </div>
        <div class="box-body text-center">
          <h1>{{$totales_personas['redondos-1']}}</h1>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-3">
      <div class="box box-primary">
        <div class="box-header">
          <b>Total de redondos</b>
          <div class="pull-right">
            <div>(-2)</div>
          </div>
        </div>
        <div class="box-body text-center">
          <h1>{{$totales_personas['redondos-2']}}</h1>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-3">
      <div class="box box-success">
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped" style="width:100%">
              <tbody>
                <tr>
                  <th>Total</th>
                  <td>{{number_format($totales['total'],2)}}</td>
                </tr>
                <tr>
                  <th>Pago a {{\App\Corrida::find(\Request::input('corrida_id'))->nombre_punto_b ?? '"sin punto b en corrida"'}}</th>
                  <td>{{number_format(($totales_personas['redondos-1'] * 200),2)}}</td>
                </tr>
                <tr>
                  <th>total</th>
                  <td>{{number_format($totales['total'] - ($totales_personas['redondos-1'] * 200),2)}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
