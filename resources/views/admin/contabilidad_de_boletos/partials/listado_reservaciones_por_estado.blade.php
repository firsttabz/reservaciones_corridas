<br>
<div class="row">
  @foreach($venta_estados as $estado => $data)
    <div class="col-xs-12 col-md-6">
      <div class="box">
        <div class="box-header">
          <div class="box-header">
            Ventas hechas en <b>{{strtoupper($estado)}}</b>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped" style="width:100%">
                <tbody>
                  <tr>
                    <th>Cliente</th>
                    <!--<th>Clave</th> -->
                    <th>Sucursal</th>
                    <th>Costo</th>
                  </tr>
                  @foreach($data['reservaciones'] as $r)
                    <tr>
                      <td>{{$r['cliente']}}</td>
                      <!--<td>{{$r['clave']}}</td> -->
                      <td>{{$r->sucursal_pagado->nombre}}</td>
                      <td>{{$r['costo_boleto']}}</td>

                    </tr>
                  @endforeach
                  <tr>
                    <td></td>
                    <td class="text-right"><b>TOTAL</b></td>
                    <td>{{number_format($data['total'],2)}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach
</div>
