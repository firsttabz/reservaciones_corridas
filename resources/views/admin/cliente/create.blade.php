@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.client.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.cliente.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('razon_social', trans('quickadmin.client.fields.razon_social').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('razon_social', old('razon_social'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('razon_social'))
                        <p class="help-block">
                            {{ $errors->first('razon_social') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('rfc', trans('quickadmin.client.fields.rfc').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('rfc', old('rfc'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('rfc'))
                        <p class="help-block">
                            {{ $errors->first('rfc') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('domicilio', trans('quickadmin.client.fields.domicilio').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('domicilio', old('domicilio'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('domicilio'))
                        <p class="help-block">
                            {{ $errors->first('domicilio') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('celular', trans('quickadmin.client.fields.celular').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('celular', old('celular'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('celular'))
                        <p class="help-block">
                            {{ $errors->first('celular') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('email', trans('quickadmin.client.fields.email').'*', ['class' => 'control-label']) !!}
                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('email'))
                        <p class="help-block">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('regimen_fiscal', trans('quickadmin.client.fields.regimen_fiscal').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('regimen_fiscal', config('sat.regimentes_fiscales'), old('regimen_fiscal'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('regimen_fiscal'))
                        <p class="help-block">
                            {{ $errors->first('regimen_fiscal') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('uso_cfdi', trans('quickadmin.client.fields.uso_cfdi').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('uso_cfdi', config('sat.usos_cfdi'), old('uso_cfdi'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('uso_cfdi'))
                        <p class="help-block">
                            {{ $errors->first('uso_cfdi') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop
