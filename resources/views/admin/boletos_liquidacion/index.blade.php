@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.lista_liquidaciones_access.title')</h3>
    <div class="panel panel-default">
        {!! Form::open(['method' => 'get', 'route' => ['admin.liquidaciones.index'],'id' => 'form', 'autocomplete' => 'off']) !!}
        <div class="panel-body">
            <div class="col-xs-12 col-md-6">
                {!! Form::label('fecha_reservacion', 'Fecha liquidación', ['class' => 'control-label']) !!}
                {!! Form::text('from', old('from', \Request::input('from')), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('from'))
                        <p class="help-block">
                            {{ $errors->first('from') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('tipo', trans('quickadmin.lista_liquidaciones_access.fields.tipo').'*', ['class' => 'control-label']) !!}
                {!! Form::select('tipo', ['paqueteria' => 'paqueteria', 'reservaciones' => 'reservaciones'], old('tipo', \Request::input('tipo')), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tipo'))
                        <p class="help-block">
                            {{ $errors->first('tipo') }}
                        </p>
                    @endif
             </div>
        </div>
        {!! Form::submit("Aceptar", ['class' => 'btn btn-primary btn-block',]) !!}
        {!! Form::close() !!}
    </div>

    @if(\Request::input('tipo') == 'paqueteria')
    <div class="panel panel-default">
        <div class="panel-body">
          <legend>
            {{\Request::input('tipo')}} liquidadas
          </legend>
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>clave</th>
                    <th>Estatus</th>
                    <th>Fecha de envío</th>
                    <th>Remitente</th>
                    <th>Destinatario</th>
                    <th>Estatus</th>
                    <th>Vendedor recibe</th>
                    <th>Vendedor liquida</th>
                    <th>Costo</th>

                </tr>
                </thead>
                <tbody>
                @foreach($lista as $r)
                <tr>
                    <td>{{$r->clave}}</td>
                    <td>{{$r->estatus}}</td>
                    <td>{{Carbon\Carbon::parse($r->created_at)->format('d-m-Y')}}</td>
                    <td>{{$r->remitente}}</td>
                    <td>{{$r->destinatario}}</td>
                    <td>{{$r->estatus}}</td>
                    <td>{{\App\User::find($r->vendedor_id_recibe)->name}}</td>
                    <td>{{\App\User::find($r->vendedor_id_dinero)->name}}</td>
                    <td>{{number_format($r->costo,2)}}</td>
                </tr>
                @endforeach
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      @elseif(\Request::input('tipo') == 'reservaciones')
        <div class="panel panel-default">
          <div class="panel-body">
            <legend>
              {{\Request::input('tipo')}} liquidadas
            </legend>
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>clave</th>
                      <th>Estatus</th>
                      <th>Cliente</th>
                      <th>Vendedor liquida</th>
                      <th>Costo boleto</th>
                      <th>Liqudación</th>

                  </tr>
                  </thead>
                  <tbody>
                  @foreach($lista as $r)
                  <tr>
                      <td>{{$r->clave}}</td>
                      <td>{{$r->estatus}}</td>
                      <td>{{$r->cliente}}</td>
                      <td>{{ $r->vendedor_liquida}}</td>
                      <td>{{ number_format($r->costo_boleto,2)}}</td>
                      <td>{{number_format($r->costo_boleto - $r->anticipo,2)}}</td>

                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      @endif
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                defaultDate: new Date()
            });

        });
    </script>
    <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
    }
    </script>
@stop
