@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.corte-de-caja.title')</h3>
    <div class="panel panel-default">
        {!! Form::open(['method' => 'get', 'route' => ['admin.corte_de_cajas_basico.index'],'id' => 'form', 'autocomplete' => 'off']) !!}
        <div class="panel-body">
            <div class="col-xs-12 col-md-6">
                {!! Form::label('fecha_reservacion', 'Fecha inicio', ['class' => 'control-label']) !!}
                {!! Form::text('from', old('from', \Request::input('from')), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('from'))
                        <p class="help-block">
                            {{ $errors->first('from') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('fecha_reservacion', 'Fecha final', ['class' => 'control-label']) !!}
                {!! Form::text('to', old('to', \Request::input('to')), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('to'))
                        <p class="help-block">
                            {{ $errors->first('to') }}
                        </p>
                    @endif
            </div>
            @if(Auth::user()->role_id == 1)
            <div class="col-xs-12 col-md-12">
                {!! Form::label('user_id', trans('quickadmin.contabilidad.fields.user').'*', ['class' => 'control-label']) !!}
                {!! Form::select('user_id', $users, old('user_id', \Request::input('user_id')), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('user_id'))
                        <p class="help-block">
                            {{ $errors->first('user_id') }}
                        </p>
                    @endif
             </div>
             @else
              <input type="hidden" name="user_id" value="{{Crypt::encrypt(Auth::id())}}" />
             @endif
        </div>
        {!! Form::submit("Aceptar", ['class' => 'btn btn-primary btn-block',]) !!}
        {!! Form::close() !!}
    </div>
    @if(count($reservaciones) > 0 || count($envios) > 0)
    <div class="row">
        <div class="col-xs-12">
          <div class="pull-right">
            <button type="button" class="btn btn-sm btn-info" onclick="printDiv('printable')">Imprimir</button>
            <br>
            <br>
          </div>
        </div>
    </div>
    @endif
    <div id="printable">
      @if(count($envios) > 0)
      <div class="panel panel-default">
          <div class="panel-body">
            <legend>Envios cobrados por {{$vendedor->name}}</legend>
              <div class="col-xs-12 table-responsive">
                  <table class="table table-striped">
                      <thead>
                      <tr>
                          <th>clave</th>
                          <th>Estatus</th>
                          <th>Fecha de envío</th>
                          <th>Remitente</th>
                          <th>Destinatario</th>
                          <th>Estatus</th>
                          <th>Vendedor dinero</th>
                          <th>Costo</th>

                      </tr>
                      </thead>
                      <tbody>
                      @foreach($envios as $r)
                      <tr>
                          <td>{{$r->clave}}</td>
                          <td>{{$r->estatus}}</td>
                          <td>{{Carbon\Carbon::parse($r->created_at)->format('d-m-Y')}}</td>
                          <td>{{$r->remitente}}</td>
                          <td>{{$r->destinatario}}</td>
                          <td>{{$r->estatus}}</td>
                          <td>{{$vendedor->name}}</td>

                          <td>{{$r->costo}}</td>
                      </tr>
                      @endforeach

                      <tr class="warning">
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>TOTAL</td>
                          <td>{{number_format($sum_envios,2)}}</td>
                      </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
      @endif
      @if($hay_quest == true)
      <div class="panel panel-default">
          <div class="panel-body">
              <div class="col-xs-12 table-responsive">
                  <table class="table table-striped">
                      <thead>
                          <tr>
                              <th>Dinero de</th>
                              <th>total</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>pagados</td>
                              <td>{{number_format($sumatorias['sum_pagado'],2)}}</td>

                          </tr>
                          <tr>
                              <td>anticipos</td>
                              <td>{{number_format($sumatorias['sum_anticipado'],2)}}</td>

                          </tr>
                          <tr>
                              <td>liquidaciones</td>
                              <td>{{number_format($sumatorias['sum_liquidado'],2)}}</td>

                          </tr>
                          <tr class="warning">
                              <td>TOTALES</td>
                              <td>{{number_format($sumatorias['sum_total'],2)}}</td>

                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      @endif
      @if(count($reservaciones) > 0)
      <div class="panel panel-default">
          <div class="panel-body">
            <legend>Boletos reservados por {{$vendedor->name}}</legend>
              <div class="col-xs-12 table-responsive">
                  <table class="table table-striped">
                      <thead>
                      <tr>
                          <th>clave</th>
                          <th>Estatus</th>
                          <th>Fecha reservación</th>
                          <th>Cliente</th>
                          <th>Asiento</th>
                          <th>Corrida</th>
                          <th>Vendedor</th>
                          <th>Descuento</th>
                          <th>Anticipo</th>
                          <th>Precio boleto</th>
                          <th>Fecha de venta</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($reservaciones as $r)
                      <tr>
                          <td>{{$r->clave}}</td>
                          <td>{{$r->estatus}}</td>
                          <td>{{$r->fecha_reservacion}}</td>
                          <td>{{$r->cliente}}</td>
                          <td>{{$r->asiento}}</td>
                          <td>{{$r->corrida}}</td>
                          <td>{{$vendedor->name}}</td>
                          <td>
                          {{ Form::checkbox("descuento", 1, $r->descuento == 1 ? true : false, ["disabled"]) }}
                          </td>
                          <td>{{$r->anticipo}}</td>
                          <td>{{$r->costo_boleto}}</td>
                          <td>{{$r->created_at}}</td>
                      </tr>
                      @endforeach
                      <tr class="warning">
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>TOTAL</td>
                          <td>{{-- number_format($sum_envios,2) --}}</td>
                      </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      @endif
    </div>
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });
    </script>
    <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
    }
    </script>
@stop
