@extends('layouts.app')

@section('content')
    <!-- <h3 class="page-title">@lang('quickadmin.reservar.title')</h3>

    <p>
        {{ trans('quickadmin.qa_custom_controller_index') }}
    </p>-->
    <div id="app-cambio-asiento">
        <cambio-boleto-app>Cargando...</cambio-boleto-app>
    </div>
@stop

@section('javascript')
    @parent
    <script src="{{ asset('js/vue-apps/cambio-boleto/app.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date_salida').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            $('.date_regreso').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });
    </script>

@stop
