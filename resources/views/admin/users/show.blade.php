@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.users.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.users.fields.name')</th>
                            <td field-key='name'>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.email')</th>
                            <td field-key='email'>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.role')</th>
                            <td field-key='role'>{{ $user->role->title or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.sucursal')</th>
                            <td field-key='sucursal'>{{ $user->sucursal->nombre ?? '' }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">

<li role="presentation" class="active"><a href="#user_actions" aria-controls="user_actions" role="tab" data-toggle="tab">Acciones de Usuario (Traza)</a></li>
<li role="presentation" class=""><a href="#reservaciones" aria-controls="reservaciones" role="tab" data-toggle="tab">Reservaciones</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">

<div role="tabpanel" class="tab-pane active" id="user_actions">
<table class="table table-bordered table-striped {{ count($user_actions) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.user-actions.created_at')</th>
                        <th>@lang('quickadmin.user-actions.fields.user')</th>
                        <th>@lang('quickadmin.user-actions.fields.action')</th>
                        <th>@lang('quickadmin.user-actions.fields.action-model')</th>
                        <th>@lang('quickadmin.user-actions.fields.action-id')</th>

        </tr>
    </thead>

    <tbody>
        @if (count($user_actions) > 0)
            @foreach ($user_actions as $user_action)
                <tr data-entry-id="{{ $user_action->id }}">
                    <td>{{ $user_action->created_at or '' }}</td>
                                <td field-key='user'>{{ $user_action->user->name or '' }}</td>
                                <td field-key='action'>{{ $user_action->action }}</td>
                                <td field-key='action_model'>{{ $user_action->action_model }}</td>
                                <td field-key='action_id'>{{ $user_action->action_id }}</td>

                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="reservaciones">
<table class="table table-bordered table-striped {{ count($reservaciones) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.reservaciones.fields.cliente')</th>
                        <th>@lang('quickadmin.reservaciones.fields.costo-boleto')</th>
                        <th>@lang('quickadmin.reservaciones.fields.vendedor')</th>
                        <th>@lang('quickadmin.reservaciones.fields.asiento')</th>
                        <th>@lang('quickadmin.reservaciones.fields.corrida')</th>
                        <th>@lang('quickadmin.reservaciones.fields.clave')</th>
                        <th>@lang('quickadmin.reservaciones.fields.descuento')</th>
                        <th>@lang('quickadmin.reservaciones.fields.observaciones')</th>
                        <th>@lang('quickadmin.reservaciones.fields.comprobante-pago')</th>
                        <th>@lang('quickadmin.reservaciones.fields.estatus')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($reservaciones) > 0)
            @foreach ($reservaciones as $reservacione)
                <tr data-entry-id="{{ $reservacione->id }}">
                    <td field-key='cliente'>{{ $reservacione->cliente }}</td>
                                <td field-key='costo_boleto'>{{ $reservacione->costo_boleto }}</td>
                                <td field-key='vendedor'>{{ $reservacione->vendedor->name or '' }}</td>
                                <td field-key='asiento'>{{ $reservacione->asiento->numero or '' }}</td>
                                <td field-key='corrida'>{{ $reservacione->corrida->nombre or '' }}</td>
                                <td field-key='clave'>{{ $reservacione->clave }}</td>
                                <td field-key='descuento'>{{ Form::checkbox("descuento", 1, $reservacione->descuento == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='observaciones'>{!! $reservacione->observaciones !!}</td>
                                <td field-key='comprobante_pago'>@if($reservacione->comprobante_pago)<a href="{{ asset(env('UPLOAD_PATH').'/' . $reservacione->comprobante_pago) }}" target="_blank">Download file</a>@endif</td>
                                <td field-key='estatus'>{{ $reservacione->estatus }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('reservacione_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.restore', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('reservacione_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.perma_del', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('reservacione_view')
                                    <a href="{{ route('admin.reservaciones.show',[$reservacione->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('reservacione_edit')
                                    <a href="{{ route('admin.reservaciones.edit',[$reservacione->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('reservacione_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.destroy', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="15">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.users.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
