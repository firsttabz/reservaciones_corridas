@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.corridas.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.activo')</th>
                            <td field-key='nombre'>{{ $corrida->activo? 'Sí':'No' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.extraordinaria')</th>
                            <td field-key='nombre'>{{ $corrida->extraordinaria? 'Sí':'No' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.nombre')</th>
                            <td field-key='nombre'>{{ $corrida->nombre }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.hora-salida')</th>
                            <td field-key='hora_salida'>{{ $corrida->hora_salida }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.costo-boleto-sencillo')</th>
                            <td field-key='costo_boleto_sencillo'>{{ $corrida->costo_boleto_sencillo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.costo-boleto-redondo')</th>
                            <td field-key='costo_boleto_redondo'>{{ $corrida->costo_boleto_redondo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.total-asientos')</th>
                            <td field-key='total_asientos'>{{ $corrida->total_asientos }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.coordenadas_origen')</th>
                            <td field-key='coordenadas_origen'>{{ $corrida->lat_a }} , {{ $corrida->long_a }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.coordenadas_destino')</th>
                            <td field-key='coordenadas_destino'>{{ $corrida->lat_b }} , {{ $corrida->long_b }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.observaciones')</th>
                            <td field-key='observaciones'>{!! $corrida->observaciones !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.camioneta')</th>
                            <td field-key='camioneta'>{{$corrida->camioneta == 1? 'Sprinter/Crafter':'Autobus'}}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.observaciones')</th>
                            <td field-key='observaciones'>{!! $corrida->observaciones !!}</td>
                        </tr>
                        <!-- <tr>
                            <th>@lang('quickadmin.corridas.fields.generar-lunes')</th>
                            <td field-key='generar_lunes'>{{ Form::checkbox("generar_lunes", 1, $corrida->generar_lunes == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.generar-martes')</th>
                            <td field-key='generar_martes'>{{ Form::checkbox("generar_martes", 1, $corrida->generar_martes == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.generar-miercoles')</th>
                            <td field-key='generar_miercoles'>{{ Form::checkbox("generar_miercoles", 1, $corrida->generar_miercoles == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.generar-jueves')</th>
                            <td field-key='generar_jueves'>{{ Form::checkbox("generar_jueves", 1, $corrida->generar_jueves == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.generar-viernes')</th>
                            <td field-key='generar_viernes'>{{ Form::checkbox("generar_viernes", 1, $corrida->generar_viernes == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.generar-sabado')</th>
                            <td field-key='generar_sabado'>{{ Form::checkbox("generar_sabado", 1, $corrida->generar_sabado == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.generar-domingo')</th>
                            <td field-key='generar_domingo'>{{ Form::checkbox("generar_domingo", 1, $corrida->generar_domingo == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.chofer')</th>
                            <td field-key='chofer'>{{ $corrida->chofer->nombre or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.corridas.fields.autobus')</th>
                            <td field-key='autobus'>{{ $corrida->autobus->placas or '' }}</td>
                        </tr> -->
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">

<li role="presentation" class="active"><a href="#reservaciones" aria-controls="reservaciones" role="tab" data-toggle="tab">Reservaciones</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">

<div role="tabpanel" class="tab-pane active" id="reservaciones">
<table class="table table-bordered table-striped {{ count($reservaciones) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.reservaciones.fields.cliente')</th>
                        <th>@lang('quickadmin.reservaciones.fields.costo-boleto')</th>
                        <th>@lang('quickadmin.reservaciones.fields.vendedor')</th>
                        <th>@lang('quickadmin.reservaciones.fields.asiento')</th>
                        <th>@lang('quickadmin.reservaciones.fields.corrida')</th>
                        <th>@lang('quickadmin.reservaciones.fields.clave')</th>
                        <th>@lang('quickadmin.reservaciones.fields.descuento')</th>
                        <th>@lang('quickadmin.reservaciones.fields.observaciones')</th>
                        <th>@lang('quickadmin.reservaciones.fields.comprobante-pago')</th>
                        <th>@lang('quickadmin.reservaciones.fields.estatus')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($reservaciones) > 0)
            @foreach ($reservaciones as $reservacione)
                <tr data-entry-id="{{ $reservacione->id }}">
                    <td field-key='cliente'>{{ $reservacione->cliente }}</td>
                                <td field-key='costo_boleto'>{{ $reservacione->costo_boleto }}</td>
                                <td field-key='vendedor'>{{ $reservacione->vendedor->name or '' }}</td>
                                <td field-key='asiento'>{{ $reservacione->asiento->numero or '' }}</td>
                                <td field-key='corrida'>{{ $reservacione->corrida->nombre or '' }}</td>
                                <td field-key='clave'>{{ $reservacione->clave }}</td>
                                <td field-key='descuento'>{{ Form::checkbox("descuento", 1, $reservacione->descuento == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='observaciones'>{!! $reservacione->observaciones !!}</td>
                                <td field-key='comprobante_pago'>@if($reservacione->comprobante_pago)<a href="{{ asset(env('UPLOAD_PATH').'/' . $reservacione->comprobante_pago) }}" target="_blank">Download file</a>@endif</td>
                                <td field-key='estatus'>{{ $reservacione->estatus }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('reservacione_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.restore', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('reservacione_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.perma_del', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('reservacione_view')
                                    <a href="{{ route('admin.reservaciones.show',[$reservacione->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('reservacione_edit')
                                    <a href="{{ route('admin.reservaciones.edit',[$reservacione->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('reservacione_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.reservaciones.destroy', $reservacione->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="15">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.corridas.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.timepicker').datetimepicker({
                format: "{{ config('app.time_format_moment') }}",
            });

        });
    </script>

@stop
