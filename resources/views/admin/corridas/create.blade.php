@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.corridas.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.corridas.store'],'id' => 'form']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>

        <div class="panel-body">

          <div class="row">
              <div class="col-xs-12 form-group">
                  {!! Form::label('activo', trans('quickadmin.corridas.fields.activo').'', ['class' => 'control-label']) !!}
                  {!! Form::hidden('activo', 0) !!}
                  {!! Form::checkbox('activo', 1, old('activo', true), []) !!}
                  <p class="help-block"></p>
                  @if($errors->has('activo'))
                      <p class="help-block">
                          {{ $errors->first('activo') }}
                      </p>
                  @endif
              </div>
          </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('nombre', trans('quickadmin.corridas.fields.nombre').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', old('nombre'), ['class' => 'form-control', 'placeholder' => 'Nombre de corrida', 'required' => '']) !!}
                    <p class="help-block">Nombre de corrida</p>
                    @if($errors->has('nombre'))
                        <p class="help-block">
                            {{ $errors->first('nombre') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('hora_salida', trans('quickadmin.corridas.fields.hora-salida').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('hora_salida', old('hora_salida'), ['class' => 'form-control timepicker', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('hora_salida'))
                        <p class="help-block">
                            {{ $errors->first('hora_salida') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('costo_boleto_sencillo', trans('quickadmin.corridas.fields.costo-boleto-sencillo').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('costo_boleto_sencillo', old('costo_boleto_sencillo',(int)$configuracion->costo_boleto_sencillo), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('costo_boleto_sencillo'))
                        <p class="help-block">
                            {{ $errors->first('costo_boleto_sencillo') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('costo_boleto_redondo', trans('quickadmin.corridas.fields.costo-boleto-redondo').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('costo_boleto_redondo', old('costo_boleto_redondo', (int)$configuracion->costo_boleto_redondo), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('costo_boleto_redondo'))
                        <p class="help-block">
                            {{ $errors->first('costo_boleto_redondo') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                  <label for="select_camioneta">{{trans('quickadmin.corridas.fields.camioneta').''}}</label>
                  <select class="form-control" id="select_camioneta" name="camioneta">
                    <option value="0">Autobus</option>
                    <option value="1" {{old('camioneta') == 1? 'selected':''}}>Sprinter/Crafter</option>
                  </select>

                    <p class="help-block"></p>
                    @if($errors->has('camioneta'))
                        <p class="help-block">
                            {{ $errors->first('camioneta') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                  <label for="asientos">{{trans('quickadmin.corridas.fields.total-asientos').''}}</label>
                  <select class="form-control" id="asientos" name="total_asientos">
                    <option value="">-- Selecciona</option>
                    <option value="53" {{old('total_asientos') == 53? 'selected':''}}>53</option>
                    <option value="51" {{old('total_asientos') == 51? 'selected':''}}>51</option>
                    <option value="50" {{old('total_asientos') == 50? 'selected':''}}>50</option>
                    <option value="48" {{old('total_asientos') == 48? 'selected':''}}>48</option>
                    <option value="47" {{old('total_asientos') == 47? 'selected':''}}>47</option>
                    <option value="46" {{old('total_asientos') == 46? 'selected':''}}>46</option>
                    <option value="20" {{old('total_asientos') == 20? 'selected':''}}>20</option>
                  </select>

                    <p class="help-block"></p>
                    @if($errors->has('camioneta'))
                        <p class="help-block">
                            {{ $errors->first('camioneta') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-6 form-group">
                    {!! Form::label('Latitud de origen', trans('quickadmin.corridas.fields.lat_a').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('lat_a', old('lat_a'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('lat_a'))
                        <p class="help-block">
                            {{ $errors->first('lat_a') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 col-sm-6 form-group">
                    {!! Form::label('Longitud de origen', trans('quickadmin.corridas.fields.long_a').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('lng_a', old('lng_a'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('lng_a'))
                        <p class="help-block">
                            {{ $errors->first('lng_a') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-sm-6 form-group">
                  {!! Form::label('Latitud de destino', trans('quickadmin.corridas.fields.lat_b').'*', ['class' => 'control-label']) !!}
                  {!! Form::text('lat_b', old('lat_b'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                  <p class="help-block"></p>
                  @if($errors->has('lat_b'))
                      <p class="help-block">
                          {{ $errors->first('lat_b') }}
                      </p>
                  @endif
              </div>
              <div class="col-xs-6 col-sm-6 form-group">
                  {!! Form::label('Longitud de destino', trans('quickadmin.corridas.fields.long_b').'*', ['class' => 'control-label']) !!}
                  {!! Form::text('lng_b', old('lng_b'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                  <p class="help-block"></p>
                  @if($errors->has('lng_b'))
                      <p class="help-block">
                          {{ $errors->first('lng_b') }}
                      </p>
                  @endif
              </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('observaciones', trans('quickadmin.corridas.fields.observaciones').'', ['class' => 'control-label']) !!}
                    {!! Form::textarea('observaciones', old('observaciones'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('observaciones'))
                        <p class="help-block">
                            {{ $errors->first('observaciones') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('extraordinaria', trans('quickadmin.corridas.fields.extraordinaria').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('extraordinaria', 0) !!}
                    {!! Form::checkbox('extraordinaria', 1, old('extraordinaria', false), ['id' => 'cextraordinaria']) !!}
                    <p class="help-block" id="help-extraordinaria">Solo se habilitará esta corrida el día de la fecha programada</p>
                    @if($errors->has('extraordinaria'))
                        <p class="help-block">
                            {{ $errors->first('extraordinaria') }}
                        </p>
                    @endif
                </div>

            </div>
            <div class="row">
              <div class="col-xs-12 form-group" id="programa_fecha" style="display:none;">
                {!! Form::label('extraordinaria', trans('quickadmin.corridas.fields.fecha_extraordinaria').'', ['class' => 'control-label']) !!}
                <div class="input-group date">
                    <div class="input-group-addon" id="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" onclick="onclickCalendarInput()" name="fecha_extraordinaria" class="form-control pull-right" id="date" value="{{old('fecha_extraordinaria',Request::input('fecha_extraordinaria'))}}">

                </div>
              </div>
            </div>
<!--
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('generar_lunes', trans('quickadmin.corridas.fields.generar-lunes').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('generar_lunes', 0) !!}
                    {!! Form::checkbox('generar_lunes', 1, old('generar_lunes', true), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('generar_lunes'))
                        <p class="help-block">
                            {{ $errors->first('generar_lunes') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('generar_martes', trans('quickadmin.corridas.fields.generar-martes').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('generar_martes', 0) !!}
                    {!! Form::checkbox('generar_martes', 1, old('generar_martes', true), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('generar_martes'))
                        <p class="help-block">
                            {{ $errors->first('generar_martes') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('generar_miercoles', trans('quickadmin.corridas.fields.generar-miercoles').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('generar_miercoles', 0) !!}
                    {!! Form::checkbox('generar_miercoles', 1, old('generar_miercoles', true), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('generar_miercoles'))
                        <p class="help-block">
                            {{ $errors->first('generar_miercoles') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('generar_jueves', trans('quickadmin.corridas.fields.generar-jueves').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('generar_jueves', 0) !!}
                    {!! Form::checkbox('generar_jueves', 1, old('generar_jueves', true), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('generar_jueves'))
                        <p class="help-block">
                            {{ $errors->first('generar_jueves') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('generar_viernes', trans('quickadmin.corridas.fields.generar-viernes').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('generar_viernes', 0) !!}
                    {!! Form::checkbox('generar_viernes', 1, old('generar_viernes', true), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('generar_viernes'))
                        <p class="help-block">
                            {{ $errors->first('generar_viernes') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('generar_sabado', trans('quickadmin.corridas.fields.generar-sabado').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('generar_sabado', 0) !!}
                    {!! Form::checkbox('generar_sabado', 1, old('generar_sabado', true), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('generar_sabado'))
                        <p class="help-block">
                            {{ $errors->first('generar_sabado') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('generar_domingo', trans('quickadmin.corridas.fields.generar-domingo').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('generar_domingo', 0) !!}
                    {!! Form::checkbox('generar_domingo', 1, old('generar_domingo', true), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('generar_domingo'))
                        <p class="help-block">
                            {{ $errors->first('generar_domingo') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('chofer_id', trans('quickadmin.corridas.fields.chofer').'', ['class' => 'control-label']) !!}
                    {!! Form::select('chofer_id', $chofers, old('chofer_id'), ['class' => 'form-control select2', ($configuracion->forzar_chofer? 'required':'')]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('chofer_id'))
                        <p class="help-block">
                            {{ $errors->first('chofer_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('autobus_id', trans('quickadmin.corridas.fields.autobus').'', ['class' => 'control-label']) !!}
                    {!! Form::select('autobus_id', $autobuses, old('autobus_id'), ['id'=> 'autobus','class' => 'form-control select2', ($configuracion->forzar_autobus? 'required':'')]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('autobus_id'))
                        <p class="help-block">
                            {{ $errors->first('autobus_id') }}
                        </p>
                    @endif
                </div>
            </div>
            -->
        </div>
    </div>
    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger',]) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.timepicker').datetimepicker({
                format: "{!! config('app.time_format_moment') !!}",
            });

            $('#form').submit(function( e ) {
              //e.preventDefault();
              band = confirm("Se generarán "+   $("#asientos").val() +" asientos para esta corrida ¿Confirmas?");
              return (band)
            });

            var autobuses_autocomplete = @json($autobuses_autocomplete);
            $('#autobus').change((e) => {
              console.log(e.target.value)
              autobuses_autocomplete.forEach((item,index) => {
                if(item.id == e.target.value){
                  $("#asientos").val(item.numero_asientos)
                }
              });
            });
            $('#select_camioneta').change((e) => {
              var val = (e.target.value);
              if(val == 1){
                $("#asientos").val(20)
              } else {
                $("#asientos").val({{$configuracion->total_asientos}})
              }
            });
            $('#cextraordinaria').change(function() {
              console.log('chekeado', this.checked)
                if(this.checked) {
                    $('#help-extraordinaria').css({'color':'blue','font-weight':'bold'})
                    $('#programa_fecha').show();
                }
                else {
                  $('#help-extraordinaria').css({'color':'gray','font-weight':'normal'})
                  $('#programa_fecha').hide();
                }
            });
            $('#asientos').change(function(){
              if($('#select_camioneta').val() == '1'){
                alert('No puedes cambiar el total de asientos, porque estas usando una Sprinter/Crafter');
                $('#asientos').val(20);
              }
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                "defaultDate": new Date(),
                "showClose": true
            })
        });

        function onclickCalendarInput(){
          document.getElementById('input-group-addon').click();
        }
    </script>

@stop
