@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.corridas.title')</h3>
    @can('corrida_create')
    <p>
        <a href="{{ route('admin.corridas.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>

    </p>
    @endcan

    @can('corrida_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.corridas.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.corridas.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($corridas) > 0 ? 'datatable' : '' }} @can('corrida_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('corrida_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.corridas.fields.nombre')</th>
                        <th>@lang('quickadmin.corridas.fields.hora-salida')</th>
                        <th>@lang('quickadmin.corridas.fields.costo-boleto-sencillo')</th>
                        <th>@lang('quickadmin.corridas.fields.costo-boleto-redondo')</th>
                        <th>@lang('quickadmin.corridas.fields.total-asientos')</th>
                      <!--  <th>@lang('quickadmin.corridas.fields.observaciones')</th> -->
                      <!--  <th>@lang('quickadmin.corridas.fields.generar-lunes')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-martes')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-miercoles')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-jueves')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-viernes')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-sabado')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-domingo')</th> -->
                        <th>@lang('quickadmin.corridas.fields.activo')</th>
                        <th>@lang('quickadmin.corridas.fields.extraordinaria')</th>
                        <!-- <th>@lang('quickadmin.corridas.fields.chofer')</th>
                        <th>@lang('quickadmin.corridas.fields.autobus')</th> -->
                        <th>@lang('quickadmin.corridas.fields.camioneta')</th>
                        <th>@lang('quickadmin.corridas.fields.coordenadas_origen')</th>
                        <th>@lang('quickadmin.corridas.fields.coordenadas_destino')</th>

                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>

                <tbody>
                    @if (count($corridas) > 0)
                        @foreach ($corridas as $corrida)
                            <tr data-entry-id="{{ $corrida->id }}">
                                @can('corrida_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='nombre'>{{ $corrida->nombre }}</td>
                                <td field-key='hora_salida'>{{ $corrida->hora_salida }}</td>
                                <td field-key='costo_boleto_sencillo'>{{ $corrida->costo_boleto_sencillo }}</td>
                                <td field-key='costo_boleto_redondo'>{{ $corrida->costo_boleto_redondo }}</td>
                                <td field-key='total_asientos'>{{ $corrida->total_asientos }}</td>
                                <!-- <td field-key='observaciones'>{!! $corrida->observaciones !!}</td> -->
                                <!--<td field-key='generar_lunes'>{{ Form::checkbox("generar_lunes", 1, $corrida->generar_lunes == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_martes'>{{ Form::checkbox("generar_martes", 1, $corrida->generar_martes == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_miercoles'>{{ Form::checkbox("generar_miercoles", 1, $corrida->generar_miercoles == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_jueves'>{{ Form::checkbox("generar_jueves", 1, $corrida->generar_jueves == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_viernes'>{{ Form::checkbox("generar_viernes", 1, $corrida->generar_viernes == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_sabado'>{{ Form::checkbox("generar_sabado", 1, $corrida->generar_sabado == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_domingo'>{{ Form::checkbox("generar_domingo", 1, $corrida->generar_domingo == 1 ? true : false, ["disabled"]) }}</td> -->
                                <td field-key='activo'>{{ $corrida->activo? 'Sí':'No' }}</td>
                                <td field-key='extraordinaria'>{{ $corrida->extraordinaria? 'Sí':'No' }}</td>
                                <!-- <td field-key='chofer'>{{ $corrida->chofer->nombre or '' }}</td>
                                <td field-key='autobus'>{{ $corrida->autobus->placas or '' }}</td> -->
                                <td field-key='camioneta'>{{ $corrida->camioneta == 0? 'Autobus':'Sprinter/Crafter' }}</td>
                                <td field-key='coordenadas_origen'>{{ $corrida->lat_a }} , {{ $corrida->long_a }}</td>
                                <td field-key='coordenadas_destino'>{{ $corrida->lat_b }} , {{ $corrida->long_b }}</td>

                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('corrida_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.corridas.restore', $corrida->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('corrida_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.corridas.perma_del', $corrida->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('corrida_view')
                                    <a href="{{ route('admin.corridas.show',[$corrida->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('corrida_edit')
                                    <a href="{{ route('admin.corridas.edit',[$corrida->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('corrida_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.corridas.destroy', $corrida->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="20">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        @can('corrida_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.corridas.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection
