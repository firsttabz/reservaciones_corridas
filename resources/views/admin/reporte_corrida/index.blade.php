@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        {!! Form::open(['method' => 'get', 'route' => ['admin.reporte_corrida.index'],'id' => 'form']) !!}
        <div class="panel-body">
            <div class="col-xs-12 col-md-6">
                {!! Form::label('fecha_reservacion', 'Fecha', ['class' => 'control-label']) !!}
                {!! Form::text('from', old('from',Request::input('from')), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('from'))
                        <p class="help-block">
                            {{ $errors->first('from') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('corrida_id', trans('quickadmin.reservaciones.fields.corrida').'*', ['class' => 'control-label']) !!}
                {!! Form::select('corrida_id', $corridas, old('corrida_id',Request::input('corrida_id')), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('corrida_id'))
                        <p class="help-block">
                            {{ $errors->first('corrida_id') }}
                        </p>
                    @endif
            </div>
        </div>
        {!! Form::submit("Aceptar", ['class' => 'btn btn-primary btn-block',]) !!}
        {!! Form::close() !!}
    </div>
    @if(count($asientos_estructura) > 0)
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-xs-12 table-responsive">
              <legend>Reservaciones</legend>
              <div class="pull-right">
                <a target="_blank" href="{{url('admin/excel/reporte_corrida').'?from='.Request::input('from').'&corrida_id='.Request::input('corrida_id')}}" class='btn btn-sm btn-info'>Generar archivo excel</a>
              </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Asiento</th>
                        <th>clave</th>
                        <th>Estatus</th>
                        <th>Fecha reservación</th>
                        <th>Cliente</th>
                        <th>Celular</th>
                        <th>Corrida</th>
                        <th>Descuento</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($asientos_estructura as $r)
                    <tr>
                        <td>{{$r['asiento']}}</td>
                        <td>{{$r['clave']}}</td>
                        <td>{{$r['estatus']}}</td>
                        <td>{{$r['fecha_reservacion']}}</td>
                        <td>{{$r['cliente']}}</td>
                        <td>{{$r['phone']}}</td>
                        <td>{{$r['corrida']}}</td>
                        <td>
                        {{ Form::checkbox("descuento", 1, $r['descuento'] == 1 ? true : false, ["disabled"]) }}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif

    @if(count($paqueteria) > 0)
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-xs-12 table-responsive">
              <legend>Paquetería</legend>
              <div class="pull-right">
                <a target="_blank" href="{{url('admin/excel/reporte_paqueteria').'?from='.Request::input('from').'&corrida_id='.Request::input('corrida_id')}}" class='btn btn-sm btn-info'>Generar archivo excel</a>
              </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>clave</th>
                        <th>Estatus</th>
                        <th>Fecha</th>
                        <th>Remitente</th>
                        <th>Número remitente</th>
                        <th>Destinatario</th>
                        <th>Número destinatario</th>
                        <th>Estatus</th>
                        <th>Pagado</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paqueteria as $p)
                    <tr>
                        <td>{{$p->clave}}</td>
                        <td>{{$p->estatus}}</td>
                        <td>{{$p->created_at}}</td>
                        <td>{{$p->remitente}}</td>
                        <td>{{$p->telefono_remitente}}</td>
                        <td>{{$p->destinatario}}</td>
                        <td>{{$p->telefono_destinatario}}</td>
                        <td>{{$p->estatus}}</td>
                        <td>{{$p->pagado ? 'Sí':'No'}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif

@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });
    </script>

@stop
