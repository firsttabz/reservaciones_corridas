@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.chofer.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.chofer.fields.nombre')</th>
                            <td field-key='nombre'>{{ $chofer->nombre }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.chofer.fields.edad')</th>
                            <td field-key='edad'>{{ $chofer->edad }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.chofer.fields.domicilio')</th>
                            <td field-key='domicilio'>{{ $chofer->domicilio }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.chofer.fields.domicili-gmaps')</th>
                            <td field-key='domicili_gmaps'>{{ $chofer->domicili_gmaps_address }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.chofer.fields.telefono')</th>
                            <td field-key='telefono'>{{ $chofer->telefono }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.chofer.fields.documentos')</th>
                            <td field-key='documentos's> @foreach($chofer->getMedia('documentos') as $media)
                                <p class="form-group">
                                    <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }} ({{ $media->size }} KB)</a>
                                </p>
                            @endforeach</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.chofer.fields.nota')</th>
                            <td field-key='nota'>{!! $chofer->nota !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.chofer.fields.correo')</th>
                            <td field-key='correo'>{{ $chofer->correo }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#corridas" aria-controls="corridas" role="tab" data-toggle="tab">Corridas</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="corridas">
<table class="table table-bordered table-striped {{ count($corridas) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.corridas.fields.nombre')</th>
                        <th>@lang('quickadmin.corridas.fields.hora-salida')</th>
                        <th>@lang('quickadmin.corridas.fields.costo-boleto-sencillo')</th>
                        <th>@lang('quickadmin.corridas.fields.costo-boleto-redondo')</th>
                        <th>@lang('quickadmin.corridas.fields.total-asientos')</th>
                        <th>@lang('quickadmin.corridas.fields.observaciones')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-lunes')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-martes')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-miercoles')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-jueves')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-viernes')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-sabado')</th>
                        <th>@lang('quickadmin.corridas.fields.generar-domingo')</th>
                        <th>@lang('quickadmin.corridas.fields.chofer')</th>
                        <th>@lang('quickadmin.corridas.fields.autobus')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($corridas) > 0)
            @foreach ($corridas as $corrida)
                <tr data-entry-id="{{ $corrida->id }}">
                    <td field-key='nombre'>{{ $corrida->nombre }}</td>
                                <td field-key='hora_salida'>{{ $corrida->hora_salida }}</td>
                                <td field-key='costo_boleto_sencillo'>{{ $corrida->costo_boleto_sencillo }}</td>
                                <td field-key='costo_boleto_redondo'>{{ $corrida->costo_boleto_redondo }}</td>
                                <td field-key='total_asientos'>{{ $corrida->total_asientos }}</td>
                                <td field-key='observaciones'>{!! $corrida->observaciones !!}</td>
                                <td field-key='generar_lunes'>{{ Form::checkbox("generar_lunes", 1, $corrida->generar_lunes == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_martes'>{{ Form::checkbox("generar_martes", 1, $corrida->generar_martes == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_miercoles'>{{ Form::checkbox("generar_miercoles", 1, $corrida->generar_miercoles == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_jueves'>{{ Form::checkbox("generar_jueves", 1, $corrida->generar_jueves == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_viernes'>{{ Form::checkbox("generar_viernes", 1, $corrida->generar_viernes == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_sabado'>{{ Form::checkbox("generar_sabado", 1, $corrida->generar_sabado == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='generar_domingo'>{{ Form::checkbox("generar_domingo", 1, $corrida->generar_domingo == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='chofer'>{{ $corrida->chofer->nombre or '' }}</td>
                                <td field-key='autobus'>{{ $corrida->autobus->placas or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('corrida_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.corridas.restore', $corrida->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('corrida_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.corridas.perma_del', $corrida->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('corrida_view')
                                    <a href="{{ route('admin.corridas.show',[$corrida->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('corrida_edit')
                                    <a href="{{ route('admin.corridas.edit',[$corrida->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('corrida_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.corridas.destroy', $corrida->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="20">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.chofers.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
