@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.chofer.title')</h3>
    @can('chofer_create')
    <p>
        <a href="{{ route('admin.chofers.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('chofer_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.chofers.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.chofers.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($chofers) > 0 ? 'datatable' : '' }} @can('chofer_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('chofer_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.chofer.fields.nombre')</th>
                        <th>@lang('quickadmin.chofer.fields.edad')</th>
                        <th>@lang('quickadmin.chofer.fields.domicilio')</th>
                        <th>@lang('quickadmin.chofer.fields.domicili-gmaps')</th>
                        <th>@lang('quickadmin.chofer.fields.telefono')</th>
                        <th>@lang('quickadmin.chofer.fields.celular')</th>
                        <th>@lang('quickadmin.chofer.fields.documentos')</th>
                        <th>@lang('quickadmin.chofer.fields.nota')</th>
                        <th>@lang('quickadmin.chofer.fields.correo')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($chofers) > 0)
                        @foreach ($chofers as $chofer)
                            <tr data-entry-id="{{ $chofer->id }}">
                                @can('chofer_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='nombre'>{{ $chofer->nombre }}</td>
                                <td field-key='edad'>{{ $chofer->edad }}</td>
                                <td field-key='domicilio'>{{ $chofer->domicilio }}</td>
                                <td field-key='domicili_gmaps'>{{ $chofer->domicili_gmaps_address }}</td>
                                <td field-key='telefono'>{{ $chofer->telefono }}</td>
                                <td field-key='celular'>{{ $chofer->celular }}</td>
                                <td field-key='documentos'> @foreach($chofer->getMedia('documentos') as $media)
                                <p class="form-group">
                                    <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }} ({{ $media->size }} KB)</a>
                                </p>
                            @endforeach</td>
                                <td field-key='nota'>{!! $chofer->nota !!}</td>
                                <td field-key='correo'>{{ $chofer->correo }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('chofer_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.chofers.restore', $chofer->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('chofer_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.chofers.perma_del', $chofer->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('chofer_view')
                                    <a href="{{ route('admin.chofers.show',[$chofer->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('chofer_edit')
                                    <a href="{{ route('admin.chofers.edit',[$chofer->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('chofer_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.chofers.destroy', $chofer->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="14">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('chofer_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.chofers.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection