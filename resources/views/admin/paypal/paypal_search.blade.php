@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.buscar_envio.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            Busqueda por código de reservaciones
        </div>

        <div class="panel-body table-responsive">
          <div class="row">
            <div class="col-lg-6">
              <form class="" action="" method="get" autocomplete="off">
                <div class="input-group">
                  <input type="text" class="form-control" name="clave" placeholder="Ingresa el código de seguimiento" value="{{old('clave',Request::get('clave'))}}" required>
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Buscar</button>
                  </span>
                </div><!-- /input-group -->
              </form>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <br>
          <div class="row">
            @if($paypal_reservacion)
              <div class="col-xs-12 col-md-6">
                <div class="small-box bg-primary">
                  <div class="inner">
                    <h3>{{ $paypal_reservacion->corrida_id }}</h3>
                    @if($paypal_reservacion->estatus == 'pagado')
                    <span class="label label-default pull-right">Pagado</span>
                    @elseif($paypal_reservacion->estatus == 'cancelado')
                    <span class="label label-default pull-right">Cancelado</span>
                    @endif
                    <div>
                      Nombre: {{ $paypal_reservacion->cliente }}
                      <br>
                      Correo: {{ $paypal_reservacion->email }}
                    </div>
                    <p class="text-center"></p>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-bandcamp"></i>
                      </div>
                      <input type="text" readonly="readonly" class="form-control" value="{{ $paypal_reservacion->clave_global}}">
                    </div>
                    <p></p>
                    <p class="text-center"></p>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" readonly="readonly" class="form-control" value="{{ $paypal_reservacion->fecha_reservacion }}"></div>
                      <p></p>
                      <p class="text-center"></p>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-money"></i>
                        </div>
                        <input type="text" readonly="readonly" class="form-control" value="${{ $suma_boletos }} MXN">
                      </div>
                      <p></p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-archive"></i>
                    </div>
                    @if($paypal_reservacion->estatus == "pagado")
                    <a href="" class="small-box-footer" style="background-color:Red;"  data-toggle="modal" data-target="#exampleModal">
                      Reembolsar Pago <i class="fa fa-arrow-circle-right"></i>
                    </a>
                    @endif
                    <!-- Modal -->
                    <p class="help-block"></p>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel" style="color:black;">Reembolsar reservación en plataforma</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:black;">
                              ¿Seguro que desae reembolsar reservación pagada con paypal en plataforma?
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            <a href="{{url('admin/paypal/paypal_execute_refund', $paypal_reservacion->clave_global)}}" class="btn btn-secondery" style="background-color:Orange;">Confirmar</a>

                            </div>
                          </div>
                        </div>
                    </div>
              </div>
          </div>
          @endif
        </div>
    </div>
@stop

@section('javascript')
    <script>
      $('#myModal').modal('show')
    </script>
@endsection
