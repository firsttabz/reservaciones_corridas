@extends('layouts.app')

@section('content')

  @if(env('BAND_REDIRECCION') == TRUE)
    <script>window.location.href = "https://autobuseslineadorada.com/";</script>
  @else
    <h3 class="page-title">@lang('quickadmin.envios.title') - {{$vendedor->name}}</h3>
    <div class="panel panel-default">
        {!! Form::open(['method' => 'POST', 'route' => ['admin.envios.store'],'id' => 'form','autocomplete'=> 'off' ,'onsubmit'=>'return onClickAceptar()']) !!}
        <div class="panel-body">
            <div class="col-xs-12 col-md-6">
                {!! Form::label('remitente', 'Remitente*', ['class' => 'control-label']) !!}
                {!! Form::text('remitente', old('remitente'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('remitente'))
                        <p class="help-block">
                            {{ $errors->first('remitente') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('telefono_remitente', 'Número telefónico* (R)', ['class' => 'control-label']) !!}
                {!! Form::text('telefono_remitente', old('telefono_remitente'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('telefono_remitente'))
                        <p class="help-block">
                            {{ $errors->first('telefono_remitente') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('destinatario', 'Destinatario*', ['class' => 'control-label']) !!}
                {!! Form::text('destinatario', old('destinatario'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('destinatario'))
                        <p class="help-block">
                            {{ $errors->first('destinatario') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('telefono_destinatario', 'Número telefónico* (D)', ['class' => 'control-label']) !!}
                {!! Form::text('telefono_destinatario', old('telefono_destinatario'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('telefono_destinatario'))
                        <p class="help-block">
                            {{ $errors->first('telefono_destinatario') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('costo', 'Costo', ['class' => 'control-label']) !!}
                {!! Form::text('costo', old('costo'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('costo'))
                        <p class="help-block">
                            {{ $errors->first('costo') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('pagado', 'Estatus', ['class' => 'control-label', 'required']) !!}
                {!! Form::select('pagado',[2 => 'Por favor seleccione', 1 => 'pagado', 0 => 'por pagar'], old('pagado',0), ['class' => 'form-control select2', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('pagado'))
                        <p class="help-block">
                            {{ $errors->first('pagado') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('id_corrida', 'Corrida', ['class' => 'control-label']) !!}
                {!! Form::select('id_corrida', $corridas, old('id_corrida'), ['class' => 'form-control select2', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('id_corrida'))
                        <p class="help-block">
                            {{ $errors->first('id_corrida') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-6">
                {!! Form::label('categoria', 'Categoría', ['class' => 'control-label']) !!}
                {!! Form::text('categoria', old('categoria'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('categoria'))
                        <p class="help-block">
                            {{ $errors->first('categoria') }}
                        </p>
                    @endif
            </div>
            <div class="col-xs-12 col-md-12">
                {!! Form::label('descripcion', 'Descripción', ['class' => 'control-label']) !!}
                {!! Form::text('descripcion', old('descripcion'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                    @if($errors->has('descripcion'))
                        <p class="help-block">
                            {{ $errors->first('descripcion') }}
                        </p>
                    @endif
            </div>
        </div>
        {!! Form::submit("Aceptar", ['class' => 'btn btn-primary btn-block','id' => 'botonAceptar']) !!}
        {!! Form::close() !!}
    </div>
  @endif

@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });

        function onClickAceptar() {
          $('#botonAceptar').hide();
          //console.log($(obj))
          /*$(obj).attr('disabled','disabled')
          setTimeout(() => {
            $(obj).removeAttr('disabled')
          },10000)*/
        }
    </script>

@stop
