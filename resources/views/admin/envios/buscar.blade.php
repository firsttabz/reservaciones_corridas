@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.buscar_envio.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            Busqueda de envío
        </div>

        <div class="panel-body table-responsive">
          <div class="row">
            <div class="col-lg-6">
              <form class="" action="" method="get" autocomplete="off">
                <div class="input-group">
                  <input type="text" class="form-control" name="clave" placeholder="Ingresa el código de seguimiento" value="{{old('clave',Request::get('clave'))}}" required>
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Buscar</button>
                  </span>
                </div><!-- /input-group -->
              </form>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <br>
          <div class="row">
            @foreach($envios as $e)
              <div class="col-xs-12 col-md-6">
                <div class="small-box bg-primary">
                  <div class="inner">
                    <h3>{{$e->corrida->nombre}} {{$e->corrida->extraordinaria? '(e)':''}} {{$e->corrida->hora_salida}}</h3>
                    @if($e->estatus != 'cancelado')
                    <span class="label label-default pull-right">{{$e->estatus == 'recibido'? 'por entregar': 'entregado'}}</span>
                    @else
                    <span class="label label-default pull-right">cancelado</span>
                    @endif
                    @if($e->pagado)
                    <span class="label label-default pull-right">Pagado</span>
                    @else
                    <span class="label label-default pull-right">Por pagar</span>
                    @endif
                    <div>
                      Remitente: {{$e->remitente}} - {{$e->telefono_remitente}}
                      <br>
                      Destinatario: {{$e->destinatario}} - {{$e->telefono_destinatario}}
                    </div>
                    <div>
                      Número de seguimiento : {{$e->clave}}
                    </div>
                    <p class="text-center"></p>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-bandcamp"></i>
                      </div>
                      <input type="text" readonly="readonly" class="form-control" value="{{$e->clave}}">
                    </div>
                    <p></p>
                    <p class="text-center"></p>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" readonly="readonly" class="form-control" value="{{$e->corrida->hora_salida}}"></div>
                      <p></p>
                      <p class="text-center"></p>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-money"></i>
                        </div>
                        <input type="text" readonly="readonly" class="form-control" value="{{$e->costo}}">
                      </div>
                      <p></p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-archive"></i>
                    </div>
                    @if($e->estatus == 'recibido')
                    <a href="" class="small-box-footer" style="background-color:Orange;"  data-toggle="modal" data-target="#entregaModal">
                      Entregar <i class="fa fa-arrow-circle-right"></i>
                    </a>
                    <a href="" class="small-box-footer" style="background-color:Red;"  data-toggle="modal" data-target="#exampleModal">
                      Cancelar <i class="fa fa-arrow-circle-right"></i>
                    </a>
                    @endif
                    <a target="_blank" href="{{url('/admin/envios/imprimir').'/'.$e->clave}}" class="small-box-footer">
                      Imprimir ticket <i class="fa fa-arrow-circle-right"></i>
                    </a>
                    <!-- Modal -->
                    <p class="help-block"></p>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel" style="color:black;">Cancela envio</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:black;">
                              Seguro que desae cancelar el envío?
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            <a href="{{url('admin/envios/cancelar',$e->clave)}}" class="btn btn-secondery" style="background-color:Orange;">Confirmar</a>

                            </div>
                          </div>
                        </div>
                    </div>
                    <!-- Entreg Modal -->
                    <p class="help-block"></p>
                    <div class="modal fade" id="entregaModal" tabindex="-1" role="dialog" aria-labelledby="entregaModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            @if($e->pagado == 1)
                            <div class="modal-header">
                              <h5 class="modal-title" id="entregaModalLabel" style="color:black;">Entrega envío</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:black;">
                              Entregar envío?
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            <a href="{{url('admin/envios/entrega',$e->clave)}}" class="btn btn-secondery" style="background-color:Orange;color:aliceblue">Confirmar</a>
                            @else
                            <div class="modal-header">
                              <h5 class="modal-title" id="entregaModalLabel" style="color:black;">Pago pendiente</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:black;">
                              Pago pendiente! Desea realizar la confirmación del pago?
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            <a href="{{url('admin/envios/entrega',$e->clave)}}" class="btn btn-secondery" style="background-color:Green;color:aliceblue">Confirmar</a>
                            @endif

                            </div>
                          </div>
                        </div>
                    </div>
                    <!-- Paga Modal -->
                    <p class="help-block"></p>
                    <div class="modal fade" id="pagaModal" tabindex="-1" role="dialog" aria-labelledby="pagaModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="pagaModalLabel" style="color:black;">Paga envío</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body" style="color:black;">
                              Pagar envío?
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            <a href="{{url('admin/envios/pagar',$e->clave)}}" class="btn btn-secondery" style="background-color:Orange;">Confirmar</a>

                            </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
      $('#myModal').modal('show')
    </script>
@endsection
