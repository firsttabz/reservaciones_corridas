@extends('layouts.app')

@section('content')
    <!-- <h3 class="page-title">@lang('quickadmin.reservar.title')</h3>

    <p>
        {{ trans('quickadmin.qa_custom_controller_index') }}
    </p>-->
    @if(env('BAND_REDIRECCION') == TRUE)
      <script>window.location.href = "https://autobuseslineadorada.com/";</script>
    @else
    <div id="app-encuentra-asiento">
        <encuentra-asiento-app>Cargando...</encuentra-asiento-app>
    </div>
    @endif
@stop

@section('javascript')
    @parent
    <script src="{{ asset('js/vue-apps/encuentra-asiento/app.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date_salida').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            $('.date_regreso').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });
    </script>

@stop
