@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.configuracion.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.costo-boleto-sencillo')</th>
                            <td field-key='costo_boleto_sencillo'>{{ $configuracion->costo_boleto_sencillo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.costo-boleto-redondo')</th>
                            <td field-key='costo_boleto_redondo'>{{ $configuracion->costo_boleto_redondo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.total-asientos')</th>
                            <td field-key='total_asientos'>{{ $configuracion->total_asientos }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.tiempo-espera-reservacion')</th>
                            <td field-key='tiempo_espera_reservacion'>{{ $configuracion->tiempo_espera_reservacion }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.tiempo-espera-reservacion-linea')</th>
                            <td field-key='tiempo_espera_reservacion'>{{ $configuracion->tiempo_espera_reservacion_plataforma}}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.editar-precios')</th>
                            <td field-key='editar_precios'>{{ Form::checkbox("editar_precios", 1, $configuracion->editar_precios == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.aplicar-descuentos')</th>
                            <td field-key='aplicar_descuentos'>{{ Form::checkbox("aplicar_descuentos", 1, $configuracion->aplicar_descuentos == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.forzar-chofer')</th>
                            <td field-key='forzar_chofer'>{{ Form::checkbox("forzar_chofer", 1, $configuracion->forzar_chofer == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.configuracion.fields.forzar-autobus')</th>
                            <td field-key='forzar_autobus'>{{ Form::checkbox("forzar_autobus", 1, $configuracion->forzar_autobus == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.configuracions.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
