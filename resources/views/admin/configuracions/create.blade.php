@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.configuracion.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.configuracions.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('costo_boleto_sencillo', trans('quickadmin.configuracion.fields.costo-boleto-sencillo').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('costo_boleto_sencillo', old('costo_boleto_sencillo'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('costo_boleto_sencillo'))
                        <p class="help-block">
                            {{ $errors->first('costo_boleto_sencillo') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('costo_boleto_redondo', trans('quickadmin.configuracion.fields.costo-boleto-redondo').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('costo_boleto_redondo', old('costo_boleto_redondo'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('costo_boleto_redondo'))
                        <p class="help-block">
                            {{ $errors->first('costo_boleto_redondo') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('tiempo_espera_reservacion', trans('quickadmin.configuracion.fields.tiempo-espera-reservacion').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('tiempo_espera_reservacion', old('tiempo_espera_reservacion'), ['class' => 'form-control', 'placeholder' => 'Tiempo de reservación en minutos', 'required' => '']) !!}
                    <p class="help-block">Tiempo de reservación en minutos</p>
                    @if($errors->has('tiempo_espera_reservacion'))
                        <p class="help-block">
                            {{ $errors->first('tiempo_espera_reservacion') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('tiempo_espera_reservacion_plataforma', trans('quickadmin.configuracion.fields.tiempo-espera-reservacion-linea').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('tiempo_espera_reservacion_plataforma', old('tiempo_espera_reservacion_plataforma'), ['class' => 'form-control', 'placeholder' => 'Tiempo de reservación en minutos', 'required' => '']) !!}
                    <p class="help-block">Tiempo de reservación en minutos</p>
                    @if($errors->has('tiempo_espera_reservacion_plataforma'))
                        <p class="help-block">
                            {{ $errors->first('tiempo_espera_reservacion_plataforma') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('total_asientos', trans('quickadmin.configuracion.fields.total-asientos').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('total_asientos', old('total_asientos'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('total_asientos'))
                        <p class="help-block">
                            {{ $errors->first('total_asientos') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('editar_precios', trans('quickadmin.configuracion.fields.editar-precios').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('editar_precios', 0) !!}
                    {!! Form::checkbox('editar_precios', 1, old('editar_precios', false), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('editar_precios'))
                        <p class="help-block">
                            {{ $errors->first('editar_precios') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('aplicar_descuentos', trans('quickadmin.configuracion.fields.aplicar-descuentos').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('aplicar_descuentos', 0) !!}
                    {!! Form::checkbox('aplicar_descuentos', 1, old('aplicar_descuentos', false), []) !!}
                    <p class="help-block">Permiso para aplicar descuentos</p>
                    @if($errors->has('aplicar_descuentos'))
                        <p class="help-block">
                            {{ $errors->first('aplicar_descuentos') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('forzar_chofer', trans('quickadmin.configuracion.fields.forzar-chofer').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('forzar_chofer', 0) !!}
                    {!! Form::checkbox('forzar_chofer', 1, old('forzar_chofer', false), []) !!}
                    <p class="help-block">Cada vez que creen una corrida forzar la selección de un chofer</p>
                    @if($errors->has('forzar_chofer'))
                        <p class="help-block">
                            {{ $errors->first('forzar_chofer') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('forzar_autobus', trans('quickadmin.configuracion.fields.forzar-autobus').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('forzar_autobus', 0) !!}
                    {!! Form::checkbox('forzar_autobus', 1, old('forzar_autobus', false), []) !!}
                    <p class="help-block">Cada vez que creen una corrida forzar que se seleccione un autobús</p>
                    @if($errors->has('forzar_autobus'))
                        <p class="help-block">
                            {{ $errors->first('forzar_autobus') }}
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop
