@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.configuracion.title')</h3>
    @can('configuracion_create')
    <p>
        <a href="{{ route('admin.configuracions.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>

    </p>
    @endcan

    @can('configuracion_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.configuracions.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.configuracions.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($configuracions) > 0 ? 'datatable' : '' }} @can('configuracion_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('configuracion_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.configuracion.fields.costo-boleto-sencillo')</th>
                        <th>@lang('quickadmin.configuracion.fields.costo-boleto-redondo')</th>
                        <th>@lang('quickadmin.configuracion.fields.total-asientos')</th>
                        <th>@lang('quickadmin.configuracion.fields.tiempo-espera-reservacion')</th>
                        <th>@lang('quickadmin.configuracion.fields.tiempo-espera-reservacion-linea')</th>
                        <th>@lang('quickadmin.configuracion.fields.editar-precios')</th>
                        <th>@lang('quickadmin.configuracion.fields.aplicar-descuentos')</th>
                        <th>@lang('quickadmin.configuracion.fields.forzar-chofer')</th>
                        <th>@lang('quickadmin.configuracion.fields.forzar-autobus')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>

                <tbody>
                    @if (count($configuracions) > 0)
                        @foreach ($configuracions as $configuracion)
                            <tr data-entry-id="{{ $configuracion->id }}">
                                @can('configuracion_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='costo_boleto_sencillo'>{{ $configuracion->costo_boleto_sencillo }}</td>
                                <td field-key='costo_boleto_redondo'>{{ $configuracion->costo_boleto_redondo }}</td>
                                <td field-key='total_asientos'>{{ $configuracion->total_asientos }}</td>
                                <td field-key='tiempo_espera_reservacion'>{{ $configuracion->tiempo_espera_reservacion }}</td>
                                <td field-key='tiempo_espera_reservacion_linea'>{{ $configuracion->tiempo_espera_reservacion_plataforma }}</td>
                                <td field-key='editar_precios'>{{ Form::checkbox("editar_precios", 1, $configuracion->editar_precios == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='aplicar_descuentos'>{{ Form::checkbox("aplicar_descuentos", 1, $configuracion->aplicar_descuentos == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='forzar_chofer'>{{ Form::checkbox("forzar_chofer", 1, $configuracion->forzar_chofer == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='forzar_autobus'>{{ Form::checkbox("forzar_autobus", 1, $configuracion->forzar_autobus == 1 ? true : false, ["disabled"]) }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('configuracion_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.configuracions.restore', $configuracion->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('configuracion_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.configuracions.perma_del', $configuracion->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('configuracion_view')
                                    <a href="{{ route('admin.configuracions.show',[$configuracion->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('configuracion_edit')
                                    <a href="{{ route('admin.configuracions.edit',[$configuracion->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('configuracion_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.configuracions.destroy', $configuracion->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="12">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        @can('configuracion_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.configuracions.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection
