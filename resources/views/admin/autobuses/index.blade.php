@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.autobus.title')</h3>
    @can('autobus_create')
    <p>
        <a href="{{ route('admin.autobuses.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('autobus_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.autobuses.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.autobuses.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($autobuses) > 0 ? 'datatable' : '' }} @can('autobus_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('autobus_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.autobus.fields.marca')</th>
                        <th>@lang('quickadmin.autobus.fields.modelo')</th>
                        <th>@lang('quickadmin.autobus.fields.placas')</th>
                        <th>@lang('quickadmin.autobus.fields.propietario')</th>
                        <th>@lang('quickadmin.autobus.fields.numero-asientos')</th>
                        <th>@lang('quickadmin.autobus.fields.descripcion')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($autobuses) > 0)
                        @foreach ($autobuses as $autobus)
                            <tr data-entry-id="{{ $autobus->id }}">
                                @can('autobus_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='marca'>{{ $autobus->marca }}</td>
                                <td field-key='modelo'>{{ $autobus->modelo }}</td>
                                <td field-key='placas'>{{ $autobus->placas }}</td>
                                <td field-key='propietario'>{{ $autobus->propietario }}</td>
                                <td field-key='numero_asientos'>{{ $autobus->numero_asientos }}</td>
                                <td field-key='descripcion'>{!! $autobus->descripcion !!}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('autobus_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.autobuses.restore', $autobus->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('autobus_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.autobuses.perma_del', $autobus->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('autobus_view')
                                    <a href="{{ route('admin.autobuses.show',[$autobus->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('autobus_edit')
                                    <a href="{{ route('admin.autobuses.edit',[$autobus->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('autobus_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.autobuses.destroy', $autobus->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="11">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('autobus_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.autobuses.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection