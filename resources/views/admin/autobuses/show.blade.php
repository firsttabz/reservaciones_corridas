@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.autobus.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.marca')</th>
                            <td field-key='marca'>{{ $autobus->marca }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.modelo')</th>
                            <td field-key='modelo'>{{ $autobus->modelo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.placas')</th>
                            <td field-key='placas'>{{ $autobus->placas }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.propietario')</th>
                            <td field-key='propietario'>{{ $autobus->propietario }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.numero-asientos')</th>
                            <td field-key='numero_asientos'>{{ $autobus->numero_asientos }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.descripcion')</th>
                            <td field-key='descripcion'>{!! $autobus->descripcion !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.costo_renta')</th>
                            <td field-key='costo_renta'>{!! number_format($autobus->costo_renta,2) !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.costo_boleto_redondo')</th>
                            <td field-key='costo_boleto_redondo'>{!! number_format($autobus->costo_boleto_redondo,2) !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.autobus.fields.costo_boleto_sencillo')</th>
                            <td field-key='costo_boleto_sencillo'>{!! number_format($autobus->costo_boleto_sencillo,2) !!}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->

            <p>&nbsp;</p>

            <a href="{{ route('admin.autobuses.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
