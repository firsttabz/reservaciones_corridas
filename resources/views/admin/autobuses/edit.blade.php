@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.autobus.title')</h3>

    {!! Form::model($autobus, ['method' => 'PUT', 'route' => ['admin.autobuses.update', $autobus->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('marca', trans('quickadmin.autobus.fields.marca').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('marca', old('marca'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('marca'))
                        <p class="help-block">
                            {{ $errors->first('marca') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('modelo', trans('quickadmin.autobus.fields.modelo').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('modelo', old('modelo'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('modelo'))
                        <p class="help-block">
                            {{ $errors->first('modelo') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('placas', trans('quickadmin.autobus.fields.placas').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('placas', old('placas'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('placas'))
                        <p class="help-block">
                            {{ $errors->first('placas') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('propietario', trans('quickadmin.autobus.fields.propietario').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('propietario', old('propietario'), ['class' => 'form-control', 'placeholder' => 'Escribe el nombre del propietario', 'required' => '']) !!}
                    <p class="help-block">Escribe el nombre del propietario</p>
                    @if($errors->has('propietario'))
                        <p class="help-block">
                            {{ $errors->first('propietario') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('costo_renta', trans('quickadmin.autobus.fields.costo_renta').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('costo_renta', old('costo_renta'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('costo_renta'))
                        <p class="help-block">
                            {{ $errors->first('costo_renta') }}
                        </p>
                    @endif
                </div>
            </div>
            <!--
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('costo_renta', trans('quickadmin.autobus.fields.costo_boleto_sencillo').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('costo_boleto_sencillo', old('costo_boleto_sencillo'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('costo_boleto_sencillo'))
                        <p class="help-block">
                            {{ $errors->first('costo_boleto_sencillo') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('costo_boleto_redondo', trans('quickadmin.autobus.fields.costo_boleto_redondo').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('costo_boleto_redondo', old('costo_boleto_redondo'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('costo_boleto_redondo'))
                        <p class="help-block">
                            {{ $errors->first('costo_boleto_redondo') }}
                        </p>
                    @endif
                </div>
            </div>
          -->
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('descripcion', trans('quickadmin.autobus.fields.descripcion').'', ['class' => 'control-label']) !!}
                    {!! Form::textarea('descripcion', old('descripcion'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descripcion'))
                        <p class="help-block">
                            {{ $errors->first('descripcion') }}
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop
