@if(App\FechaCorte::dias() <= 5)
<!--
  <div class="col-xs-12">
    <div class="info-box bg-yellow">
      <span class="info-box-icon"><i class="fa fa-exclamation"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Fecha de corte</span>
              <span class="info-box-number">El servicio está por caducar {{\Carbon\Carbon::parse(\App\FechaCorte::first()->fecha_corte)->format('d-m-Y')}}</span>

                <div class="progress">
                  <div class="progress-bar" style="width: {{abs(((App\FechaCorte::dias())*100)/env('DIAS_PARA_CORTE',30)-100)}}%"></div>
                </div>
                    <span class="progress-description">
                      Quédan <b>{{App\FechaCorte::dias()}}</b> día{{App\FechaCorte::dias()>1? 's':''}} de servicio
                    </span>
          </div>

      </div>
    </div> -->
    <div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>Alerta!</strong> Quédan <b>{{App\FechaCorte::dias()}}</b> día{{App\FechaCorte::dias()>1? 's':''}} de servicio.
    </div>
@endif
