@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">



            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('quickadmin.qa_dashboard')</span>
                </a>
            </li>

            @can('user_management_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>@lang('quickadmin.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('role_access')
                    <li>
                        <a href="{{ route('admin.roles.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.roles.title')</span>
                        </a>
                    </li>
                    @endcan

                    @can('user_access')
                    <li>
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-user"></i>
                            <span>@lang('quickadmin.users.title')</span>
                        </a>
                    </li>@endcan

                    @can('user_action_access')
                    <li>
                        <a href="{{ route('admin.user_actions.index') }}">
                            <i class="fa fa-th-list"></i>
                            <span>@lang('quickadmin.user-actions.title')</span>
                        </a>
                    </li>@endcan

                    @can('sucursale_access')
                    <li>
                        <a href="{{ route('admin.sucursales.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.sucursales.title')</span>
                        </a>
                    </li>@endcan
                </ul>
            </li>@endcan

            @can('contact_management_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-phone-square"></i>
                    <span>@lang('quickadmin.contact-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('contact_company_access')
                    <li>
                        <a href="{{ route('admin.contact_companies.index') }}">
                            <i class="fa fa-building-o"></i>
                            <span>@lang('quickadmin.contact-companies.title')</span>
                        </a>
                    </li>@endcan

                    @can('contact_access')
                    <li>
                        <a href="{{ route('admin.contacts.index') }}">
                            <i class="fa fa-user-plus"></i>
                            <span>@lang('quickadmin.contacts.title')</span>
                        </a>
                    </li>@endcan

                </ul>
            </li>@endcan


            @can('planeación_de_corrida_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-train"></i>
                    <span>@lang('quickadmin.planeacion-de-corridas.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('autobus_access')
                    <li>
                        <a href="{{ route('admin.autobuses.index') }}">
                            <i class="fa fa-bus"></i>
                            <span>@lang('quickadmin.autobus.title')</span>
                        </a>
                    </li>@endcan

                    @can('chofer_access')
                    <li>
                        <a href="{{ route('admin.chofers.index') }}">
                            <i class="fa fa-user-o"></i>
                            <span>@lang('quickadmin.chofer.title')</span>
                        </a>
                    </li>@endcan

                    @can('corrida_access')
                    <li>
                        <a href="{{ route('admin.corridas.index') }}">
                            <i class="fa fa-road"></i>
                            <span>@lang('quickadmin.corridas.title')</span>
                        </a>
                    </li>@endcan

                    @can('configuracion_access')
                    <li>
                        <a href="{{ route('admin.configuracions.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.configuracion.title')</span>
                        </a>
                    </li>@endcan

                    @can('asiento_access')
                    <li>
                        <a href="{{ route('admin.asientos.index') }}">
                            <i class="fa fa-caret-down"></i>
                            <span>@lang('quickadmin.asiento.title')</span>
                        </a>
                    </li>@endcan

                    @can('reporte_corrida_access')
                    <li>
                        <a href="{{ route('admin.reporte_corrida.index') }}">
                            <i class="fa fa-table"></i>
                            <span>Reporte de corrida</span>
                        </a>
                    </li>@endcan
                </ul>
            </li>
            @endcan

            @can('contabilidad_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-balance-scale"></i>
                    <span>@lang('quickadmin.contabilidad.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    @can('contabilidad_de_boletos')
                    <li>
                        <a href="{{ route('admin.contabiliad.boletos') }}">
                            <i class="fa fa-table"></i>
                            <span>Contabilidad de boletos</span>
                        </a>
                    </li>
                    @endcan

                    @can('corte_de_caja_access')
                    <li>
                        <a href="{{ route('admin.corte_de_cajas.index') }}">
                            <i class="fa fa-table"></i>
                            <span>@lang('quickadmin.corte-de-caja.title')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.corte_de_cajas_basico.index') }}">
                            <i class="fa fa-table"></i>
                            <span>@lang('quickadmin.corte-de-caja.title') básico</span>
                        </a>
                    </li>
                    @endcan
                    @can('lista_liquidaciones_access')
                    <li>
                        <a href="{{ route('admin.liquidaciones.index') }}">
                            <i class="fa fa-table"></i>
                            <span>@lang('quickadmin.lista_liquidaciones_access.title')</span>
                        </a>
                    </li>@endcan
                </ul>
            </li>
            @endcan

            @can('venta_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money"></i>
                    <span>@lang('quickadmin.ventas.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('reservacione_access')
                    <li>
                        <a href="{{ route('admin.reservaciones.index') }}">
                            <i class="fa fa-list"></i>
                            <span>@lang('quickadmin.reservaciones.title')</span>
                        </a>
                    </li>@endcan

                    @can('reservar_access')
                    <li>
                        <a href="{{ route('admin.reservars.index') }}">
                            <i class="fa fa-location-arrow"></i>
                            <span>@lang('quickadmin.reservar.title')</span>
                        </a>
                    </li>@endcan

                    @can('reservar_cambios')
                    <li>
                        <a href="{{ route('admin.reservaciones.cambios') }}">
                            <i class="fa fa-edit"></i>
                            <span>@lang('quickadmin.reservar_cambios.title')</span>
                        </a>
                    </li>@endcan

                </ul>
            </li>@endcan

            @can('envios_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-archive"></i>
                    <span>@lang('quickadmin.envios.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('envio_access')
                    <li>
                        <a href="{{ route('admin.envios.create') }}">
                            <i class="fa fa-archive"></i>
                            <span>@lang('quickadmin.envios.title')</span>
                        </a>
                    </li>@endcan

                    @can('buscar_envio_access')
                    <li>
                        <a href="{{ route('admin.envios.index') }}">
                            <i class="fa fa-search"></i>
                            <span>@lang('quickadmin.buscar_envio.title')</span>
                        </a>
                    </li>@endcan
                </ul>
            </li>@endcan

            @can('client_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>@lang('quickadmin.client.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('client_access')
                    <li>
                        <a href="{{ route('admin.cliente.index') }}">
                            <i class="fa fa-user"></i>
                            <span>Cliente</span>
                        </a>
                    </li>@endcan
                </ul>
            </li>
            @endcan

            @can('paypal_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-paypal"></i>
                    <span>Cancelaciones Paypal</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('paypal_access')
                    <li>
                        <a href="{{ url('admin/paypal/code_search') }}">
                            <i class="fa fa-paypal"></i>
                            <span>Buscar reservación</span>
                        </a>
                    </li>@endcan
                </ul>
            </li>
            @endcan

            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">@lang('quickadmin.qa_change_password')</span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.qa_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
