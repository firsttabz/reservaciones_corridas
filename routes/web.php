<?php
Route::get('/', function () {
  if(env('PLATAFORMA_RESERVACIONES') == true) {
    return redirect(('/pageWeb'));
  }
  else {
    return redirect('/admin/home');
  }
});
Route::get('/pageWeb', 'Plataforma\FrontendController@index');
Route::any('/reservar/renderBus', 'Plataforma\ReservacionController@renderBus');
Route::resource('/reservar', 'Plataforma\ReservacionController');
/*Route::get('/pagoconpaypal', function(){

    return view('frontend.paypal');
});*/

Route::get('/pagoconpaypal/{clave}', 'Plataforma\PaymentController@create')->name('pagoconpaypal');
Route::post('/paypal', 'Plataforma\PaymentController@payWithpaypal');
Route::get('/status', 'Plataforma\PaymentController@getPaymentStatus')->name('status');


// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');

    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('user_actions', 'Admin\UserActionsController');
    Route::resource('contact_companies', 'Admin\ContactCompaniesController');
    Route::post('contact_companies_mass_destroy', ['uses' => 'Admin\ContactCompaniesController@massDestroy', 'as' => 'contact_companies.mass_destroy']);
    Route::resource('contacts', 'Admin\ContactsController');
    Route::post('contacts_mass_destroy', ['uses' => 'Admin\ContactsController@massDestroy', 'as' => 'contacts.mass_destroy']);
    Route::resource('autobuses', 'Admin\AutobusesController');
    Route::post('autobuses_mass_destroy', ['uses' => 'Admin\AutobusesController@massDestroy', 'as' => 'autobuses.mass_destroy']);
    Route::post('autobuses_restore/{id}', ['uses' => 'Admin\AutobusesController@restore', 'as' => 'autobuses.restore']);
    Route::delete('autobuses_perma_del/{id}', ['uses' => 'Admin\AutobusesController@perma_del', 'as' => 'autobuses.perma_del']);
    Route::resource('chofers', 'Admin\ChofersController');
    Route::post('chofers_mass_destroy', ['uses' => 'Admin\ChofersController@massDestroy', 'as' => 'chofers.mass_destroy']);
    Route::post('chofers_restore/{id}', ['uses' => 'Admin\ChofersController@restore', 'as' => 'chofers.restore']);
    Route::delete('chofers_perma_del/{id}', ['uses' => 'Admin\ChofersController@perma_del', 'as' => 'chofers.perma_del']);
    Route::resource('corridas', 'Admin\CorridasController');
    Route::post('corridas_mass_destroy', ['uses' => 'Admin\CorridasController@massDestroy', 'as' => 'corridas.mass_destroy']);
    Route::post('corridas_restore/{id}', ['uses' => 'Admin\CorridasController@restore', 'as' => 'corridas.restore']);
    Route::delete('corridas_perma_del/{id}', ['uses' => 'Admin\CorridasController@perma_del', 'as' => 'corridas.perma_del']);
    Route::resource('configuracions', 'Admin\ConfiguracionsController');
    Route::post('configuracions_mass_destroy', ['uses' => 'Admin\ConfiguracionsController@massDestroy', 'as' => 'configuracions.mass_destroy']);
    Route::post('configuracions_restore/{id}', ['uses' => 'Admin\ConfiguracionsController@restore', 'as' => 'configuracions.restore']);
    Route::delete('configuracions_perma_del/{id}', ['uses' => 'Admin\ConfiguracionsController@perma_del', 'as' => 'configuracions.perma_del']);
    Route::resource('asientos', 'Admin\AsientosController');
    Route::post('asientos_mass_destroy', ['uses' => 'Admin\AsientosController@massDestroy', 'as' => 'asientos.mass_destroy']);
    Route::post('asientos_restore/{id}', ['uses' => 'Admin\AsientosController@restore', 'as' => 'asientos.restore']);
    Route::delete('asientos_perma_del/{id}', ['uses' => 'Admin\AsientosController@perma_del', 'as' => 'asientos.perma_del']);
    Route::get('reservaciones/imprimir/{clave}','Admin\ImpresionControlleler@imprimirBoleto');
    Route::get('/reservaciones/cambios','Admin\ReservacionesController@boletoCambio')->name('reservaciones.cambios');
    Route::resource('reservaciones', 'Admin\ReservacionesController');
    Route::post('reservaciones_mass_destroy', ['uses' => 'Admin\ReservacionesController@massDestroy', 'as' => 'reservaciones.mass_destroy']);
    Route::post('reservaciones_restore/{id}', ['uses' => 'Admin\ReservacionesController@restore', 'as' => 'reservaciones.restore']);
    Route::delete('reservaciones_perma_del/{id}', ['uses' => 'Admin\ReservacionesController@perma_del', 'as' => 'reservaciones.perma_del']);
    Route::resource('reservars', 'Admin\ReservarsController');
    Route::resource('corte_de_cajas', 'Admin\CorteDeCajasController');
    Route::resource('corte_de_cajas_basico', 'Admin\CorteCajaBasicoController');
    Route::resource('liquidaciones', 'Admin\ListaLiquidacionesController');
    Route::post('/spatie/media/upload', 'Admin\SpatieMediaController@create')->name('media.upload');
    Route::post('/spatie/media/remove', 'Admin\SpatieMediaController@destroy')->name('media.remove');

    Route::resource('reporte_corrida','Admin\ReporteCorridaController');
    Route::get('excel/reporte_corrida','Admin\ExcelController@reporteCorrida');
    Route::get('excel/reporte_paqueteria','Admin\ExcelController@reportePaqueteria');

    Route::get('/reservaciones/liquidar/{clave}', 'Admin\ReservacionesController@boletoLiquidacion');
    Route::get('/reservaciones/cancelar/{clave}','Admin\ReservacionesController@boletoCancelacion');
    Route::get('/contabilidad_de_boletos',['uses' => 'Admin\ContabilidadBoletosController@index', 'as' => 'contabiliad.boletos']);

    Route::get('/envios/imprimir/{clave}', 'Admin\ImpresionControlleler@imprimirEnvio');
    Route::get('/envios/pago-entrega/{clave}', 'Admin\EnviosController@envioPagoEntrega');
    Route::get('/envios/pagar/{clave}', 'Admin\EnviosController@envioPagar');
    Route::get('/envios/entrega/{clave}', 'Admin\EnviosController@envioEntrega');
    Route::get('/envios/cancelar/{clave}', 'Admin\EnviosController@envioCancelacion');
    Route::resource('envios', 'Admin\EnviosController');
    //Route::get('buscar_envio', 'Admin\EnviosController@buscarEnvio');
    Route::resource('cliente', 'Admin\ClientController');
    Route::resource('sucursales', 'Admin\SucursalesController');
    Route::post('sucursales_mass_destroy', ['uses' => 'Admin\SucursalesController@massDestroy', 'as' => 'sucursales.mass_destroy']);
    Route::post('sucursales_restore/{id}', ['uses' => 'Admin\SucursalesController@restore', 'as' => 'sucursales.restore']);
    Route::delete('sucursales_perma_del/{id}', ['uses' => 'Admin\SucursalesController@perma_del', 'as' => 'sucursales.perma_del']);

    //Paypalrefund Controler
    Route::get('/paypal/code_search', 'Admin\RefundController@index')->name('code_search');
    //Route::post('/paypal/paypal_set_refund', 'Admin\RefundController@setPaypalRefund');
    Route::get('/paypal/paypal_execute_refund/{clave_global}', 'Admin\RefundController@executePayPalRefund');

});
