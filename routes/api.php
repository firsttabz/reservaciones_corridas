<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {

        Route::resource('corridas', 'CorridasController', ['except' => ['create', 'edit']]);
        Route::get('reservaciones/ajustar','ReservacionesController@ajustar');

        Route::post('reservaciones/encuentra', 'ReservacionesController@encuentra');
        Route::post('reservaciones/reservar', 'ReservacionesController@reservar');
        Route::resource('reservaciones', 'ReservacionesController', ['except' => ['create', 'edit']]);

        Route::get('mensajes/send','MensajesController@service');
        Route::any('sistema/desactivacion','ActivacionSistemaController@desactivacion');
        Route::any('sistema/activacion','ActivacionSistemaController@activacion');
        Route::resource('sucursales', 'SucursalesController', ['except' => ['create', 'edit']]);

        Route::post('reservaciones/cambios/encuentra', 'ReservacionesController@cambiosEncuentra');
        Route::post('reservaciones/cambios/reserva', 'ReservacionesController@cambiosReserva');
        Route::post('reservaciones/reservacion','ReservacionesController@reservacion');
        Route::post('reservacion/disponibilidad', 'ReservacionesController@verificarDisponibilidad');
        Route::post('reservacion/cancelar/{clave}', 'ReservacionesController@cancelarReservacion');
        Route::post('reservacion/plataforma', 'ReservacionesController@reservacionPlataforma');
        // Route::any('sistema/whatsapp/incomes', 'ActivacionSistemaController@incomes');
});

Route::get('consultarPhone',function(){
  set_time_limit ( 3600*10 ); 
    $reserv2 = \App\Reservacione::select('phone')
    ->whereNotNull('phone')
    ->distinct('phone')
    ->whereRaw('LENGTH(phone) = 10')
    ->get();
    $array_phone = [];
    foreach ($reserv2 as $r) {
      $response = \Curl::to('https://www.waboxapp.com/api/send/link')
        ->withData([
          'token' => 'f94e5347306f081fc58bebca5ec540425b9fd98ebe3ba',
          'uid' => '5219513126088',
          'to' => '52'.$r->phone,
          //'custom_uid' => env('WABOX_CUSTOM_ID_PREFIX').str_random(10),
          'caption' => 'Descarga la nueva app de línea dorada, ¡es gratis!',
          'description' => 'Consulta nuestras corridas, sucursales y más.',
          'url_thumb' => 'https://autobuseslineadorada.com/images/LD/logo_linea_app.png',
          'url' => 'https://play.google.com/store/apps/details?id=io.jasbit.doradacliente'
        ])
        ->asJson()
        ->post();
        $response->phone ='52'.$r->phone;
        $array_phone = $response;
    }

  return dd($array_phone);

});
