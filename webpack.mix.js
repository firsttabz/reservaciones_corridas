let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .js('resources/assets/js/apps/encuentra-asiento/app.js', 'public/js/vue-apps/encuentra-asiento')
   .js('resources/assets/js/apps/cambio-boleto/app.js', 'public/js/vue-apps/cambio-boleto')

   /** asientos***/
   .js([
     'resources/assets/reservaciones_plataforma/javascript.js',
     //'resources/assets/reservaciones_plataforma/jQuery3.js'
   ],'public/js/bus.js')
   .styles([
     'resources/assets/reservaciones_plataforma/style.css',
     'resources/assets/reservaciones_plataforma/custom_tabs.css',
   ], 'public/css/bus.css');
   /**************/
