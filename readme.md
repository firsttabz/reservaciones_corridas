<p align="center"><img src="https://cbus.com.mx/central-items/logos/logo-app-distort.png" width="150px"></p>

## Para desarrolladores
Liberamos la version 1.1.12 de Central Bus, si eres desarrallodor este repositorio es para ti, eres libre de usar este código, modificarlo, adaptarlo o venderlo.

¿Qué verás en este código?
Verás los inicios de la solución, te invitamos a que te diviertas aprendiendo del modelo de negocios, Somos concientes que cada empresa tiene sus propios procesos, pero este te servirá de base para empezar a dar una solución.


## Sistema basado en el framework Laravel

Laravel es un marco de aplicación web con sintaxis expresiva y elegante. Laravel intenta eliminar el dolor del desarrollo al facilitar las tareas comunes utilizadas en la mayoría de los proyectos web


## Central bus 1.1.12

Es un sistema de transporte de pasajeros y paquetería con alcance a nivel nacional que se enfoca en la administración y coordinación de las operaciones de empresas de transportación. El sistema abarca reserva de pasajes y paquetería. 

## Características del sistema - ¿Qué hace?

Con Central bus puedes optimizar tiempos porque realizas diferentes actividades con un solo sistema

-	Imprimir tickets de abordaje
-	Controlar las reservaciones
-	Emitir tickets
-	Realizar reporte de ventas
-	Controlar las reservaciones 
-	Elaborar cortes
-	Cortes de taquilla


## Requisitos

Para poder hacer un **deploy** de este software requieres caractrísticas mínimas de hardware, pero debes considerar que las espeficiaciones del hardware dependeran de la empresa la cual requiera esta solución.

-   Procesador de 2GHz
-   Sistema operativo Linux basado en la distribución Debian
-   Apache 2.1
-   PHP 7.2 o mayor

Además te recomendamos leer la sección de instalación de Laravel para continuar con el **deploy** del sistema.

No pierdas vista de tus unidades, con nuestro software de rastreo háchales un ojo.